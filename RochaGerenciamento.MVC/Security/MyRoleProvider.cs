﻿using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Web;
using System.Web.Security;
//using RochaGerenciamento.Domain.Entities;
//using RochaGerenciamento.Infra.Contexto;
using System.Security.Principal;
using System.Configuration.Provider;
using System.Data.Odbc;
using System.Collections.Specialized;
using System;
using System.Configuration;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Domain.ValueObject;
//using RochaGerenciamento.Domain.Enums;
//using RochaGerenciamento.Domain.ValueObject;

namespace RochaGerenciamento.MVC.Security
{
    public class MyRoleProvider : RoleProvider
    {
        
        private Context db;

        public MyRoleProvider()
        {
            db = new Context();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new System.NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new System.NotImplementedException();
        }
        
        
        public override string[] GetRolesForUser(string username)
        {
            User user = new User();
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];//.ASPXAUTH
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            username = (username == null || username.Length <= 0) ? authTicket.Name : username;
            user = db.Users.FirstOrDefault(x => x.Login == username && x.IdcActive == "1");
            SessionUser.Object = user;
            if (user == null)
                return new string[] { };
            
            return new string[] { user.Profile.Name};

        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] rolenames)
        {
            throw new System.NotImplementedException();
        }


        public override bool RoleExists(string roleName)
        {
            throw new System.NotImplementedException();
        }
    }
}