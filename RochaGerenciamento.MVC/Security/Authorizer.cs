﻿using System.Web;

namespace RochaGerenciamento.MVC.Security
{
    public class Authorizer
    {
        
        public static bool AuthAdmin
        {
            get { return HttpContext.Current.User.IsInRole("Administrador"); }
        
        }

        public static bool AuthFinanceiro
        {
            get { return HttpContext.Current.User.IsInRole("Financeiro"); }
        }

        public static bool AuthCompras
        {
            get { return HttpContext.Current.User.IsInRole("Compras"); }
        }

        public static bool AuthAlmoxarifado
        {
            get { return HttpContext.Current.User.IsInRole("Almoxarifado"); }
        }

        public static bool AuthVendedor
        {
            get { return HttpContext.Current.User.IsInRole("Vendedor"); }
        }

        public static bool AuthEncarregado
        {
            get { return HttpContext.Current.User.IsInRole("Encarregado"); }
        }
    }
}