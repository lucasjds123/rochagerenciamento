﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Utils;
using RochaGerenciamento.MVC.Security;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,almoxarifado,encarregado")]
    public class RequestController : BaseController<Request>
    {
        IUserAppService _userApp;
        IProductAppService _productApp;
        public RequestController(IRequestAppService entityApp, IUserAppService userApp, IProductAppService productApp)
            : base(entityApp, userApp)
        {
            _userApp = userApp;
            _productApp = productApp;
            ViewBag.CodeProduct = new SelectList(_productApp.DropDownList(), GeneralField.Code, GeneralField.Name);
        }

        [HttpPost]
        public override ActionResult Create(Request entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _entityApp.Add(entity);
                    var from = _userApp.GetEmailUserByProfile(FlagProfile.Almoxarifado.ToString());
                    var to = _userApp.GetEmailUserByProfile(FlagProfile.Compras.ToString());
                    Email.Send(entity.Message + entity.Observation, Email.SubjectAlmoxarifado + entity.Tittle, from, to);
                    TempData["Success"] = SuccessMessage.AddAndEmailSent;
                    return RedirectToAction("Index");
                }
                TempData["Error"] = ErrorMessage.FieldRequired;
                return View();
            }
            catch 
            {
                TempData["Error"] = ErrorMessage.AdminError;
                return View();
            }

        }
        public ActionResult SendEmail(int id = 0)
        {
            try
            {
                var obj = _entityApp.GetById(id);
                var from = _userApp.GetEmailUserByProfile(FlagProfile.Almoxarifado.ToString());
                var to = _userApp.GetEmailUserByProfile(FlagProfile.Compras.ToString());
                Email.Send(obj.Message, Email.SubjectAlmoxarifado + obj.Tittle, from, to);
                TempData["Success"] = SuccessMessage.Email;
                return RedirectToAction("Index");
            }
            catch 
            {
                TempData["Error"] = ErrorMessage.Email;
                return RedirectToAction("Index");
            }
        }
        
    }
}