﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;
using RochaEmpreendimento.Utils;
using System.Net;
using System.IO;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,compras")]
    public class ProductController : BaseController<Product>
    {
        IUnitTypeAppService _unitTypeApp;
        IProviderAppService _providerApp;
        IProductAppService _productApp;
        IManufacturerAppService _manufacturerApp;
        public ProductController(IProductAppService entityApp, IAppServiceBase<User> userApp, IUnitTypeAppService unitTypeApp,
            IProviderAppService providerApp, IManufacturerAppService manufacturerApp)
            : base(entityApp, userApp)
        {
            _unitTypeApp = unitTypeApp;
            _providerApp = providerApp;
            _productApp = entityApp;
            _manufacturerApp = manufacturerApp;
            ViewBag.CodeUnitType = new SelectList(_unitTypeApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeProvider = new SelectList(_providerApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeManufacturer = new SelectList(_manufacturerApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeProduct = new SelectList(_productApp.DropDownList(), GeneralField.Name, GeneralField.Name);
            ViewBag.Recurrent = DropDownList.ListYesNo();
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeUnitType = new SelectList(_unitTypeApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeUnitType);
            ViewBag.CodeProvider = new SelectList(_providerApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeProvider);
            ViewBag.CodeManufacturer = new SelectList(_manufacturerApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeManufacturer);
            ViewBag.Recurrent = DropDownList.ListYesNo(entity.Recurrent);
            return View(entity);
        }

        [HttpPost]
        [ActionName("EditAux")]
        public ActionResult Edit(Product entity, HttpPostedFileBase uploadFile)
        {
            try
            {
                var product = _productApp.GetById(entity.Code);
                if (uploadFile != null &&  _productApp.FindBySpecs(uploadFile.FileName, entity.Code))
                {
                    TempData["Error"] = ErrorMessage.FileExists;
                    return RedirectToAction("Edit",  new { id = entity.Code});
                }
                if (product.Specs != null)
                    RochaGerenciamento.Utils.File.Delete(product.Specs);
                if (ModelState.IsValid)
                {
                    RochaGerenciamento.Utils.File.Save(uploadFile);
                    _entityApp.Update(entity);
                    TempData["Success"] = SuccessMessage.Alter;
                    return RedirectToAction("Index");
                }
                TempData["Error"] = ErrorMessage.FieldRequired;
                return RedirectToAction("Edit", new { id = entity.Code });
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return RedirectToAction("Edit", new { id = entity.Code });
            }

        }
        
        [HttpPost]
        [ActionName("CreateAux")]
        public ActionResult Create(Product entity, HttpPostedFileBase uploadFile)
        {
            try
            {
                if (uploadFile != null && _productApp.FindBySpecs(uploadFile.FileName))
                {
                    TempData["Error"] = ErrorMessage.FileExists;
                    return RedirectToAction("Create");
                }
                if (ModelState.IsValid  )
                {
                    RochaGerenciamento.Utils.File.Save(uploadFile);
                    _entityApp.Add(entity);
                    TempData["Success"] = SuccessMessage.Add;
                    return RedirectToAction("Index");
                }
                TempData["Error"] = ErrorMessage.FieldRequired;
                return RedirectToAction("Create");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return RedirectToAction("Create");
            }

        }

        public ActionResult OpenDocument(int id = 0)
        {
            Product product = _productApp.GetById(id);
            if (product.Specs == null)
            {
                TempData["Error"] = ErrorMessage.FileNotExists; 
                return RedirectToAction("Index");
                
            }
            RochaGerenciamento.Utils.File.Open(RochaGerenciamento.Utils.File.GetPath(product.Specs), product.Specs);
            return View("Close");
        }
    }
}