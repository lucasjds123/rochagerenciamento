﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,encarregado")]
    public class SubServicePlanController : BaseController<SubServicePlan>
    {
        private ISubServicePlanAppService _subServicePlanApp;
        private IPlanAppService _planApp;
        private ISubServiceAppService _subServiceApp;
        public SubServicePlanController(ISubServicePlanAppService entityApp, IAppServiceBase<User> userApp, IPlanAppService planApp , ISubServiceAppService subServiceApp)
            : base(entityApp, userApp)
        {
            _subServicePlanApp = entityApp;
            _planApp = planApp;
            _subServiceApp = subServiceApp;
            ViewBag.CodePlan = new SelectList(_planApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeSubServicePlanDepen = new SelectList(_subServicePlanApp.DependencyList(), GeneralField.Code, GeneralField.Dependency);
            ViewBag.CodeSubService = new SelectList(_subServiceApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodePlan = new SelectList(_planApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodePlan);
            ViewBag.CodeSubServicePlanDepen = new SelectList(_subServicePlanApp.DependencyList(), GeneralField.Code, GeneralField.Dependency, entity.CodeSubServicePlanDepen);
            ViewBag.CodeSubService = new SelectList(_subServiceApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name , entity.CodeSubService);
            return View(entity);
        }

        [System.Web.Mvc.HttpPost]
        public JsonResult ListPlan(int current, int status, int rowCount, Dictionary<string, string> sort, string searchPhrase, string nameConstruction, string namePlan, string nameService, string nameSubService,
                                        string measureUnit, string amountRepetition,  string percent, string estimation, string value)
        {
            int total = 0;
            var list = _subServicePlanApp.GetDynamicList(searchPhrase, rowCount, current, status, ref total, nameConstruction, namePlan, nameService, nameSubService,
                                         measureUnit, amountRepetition,  percent, estimation, value);

            return Json(new
            {
                current = current,
                rowCount = rowCount,
                rows = list,
                total = total
            });
        }

        
        public JsonResult Start(int id = 0)
        {
            try
            {
                if (_subServicePlanApp.CheckDependency(id))
                {
                    _subServicePlanApp.Start(id);
                    return Json(new { Message = "StartConfirmed", Success = SuccessMessage.StartConfirmed, Error = String.Empty });
                }
                return Json(new { Message = "Dependency", Success = String.Empty, Error = ErrorMessage.Dependency });
            }
            catch
            {
                return Json(new { Message = "False", Error = ErrorMessage.NotCompleted, Success = String.Empty });
            }
        }

        public JsonResult UpdatePercent(int cod = 0, double amount = 0)
        {
            try
            {
                
                _subServicePlanApp.UpdatePercent(cod, amount);
                return Json(new { Message = "Update", Success = SuccessMessage.Percent, Error = String.Empty });
            }
            catch
            {
                return Json(new { Message = "False", Error = ErrorMessage.NotCompleted, Success = String.Empty });
            }
        }

        public JsonResult Stop(int id = 0)
        {
            try
            {
                _subServicePlanApp.Stop(id);
                return Json(new { Message = "Stop", Success = SuccessMessage.Stop, Error = String.Empty });
            }
            catch
            {
                return Json(new { Message = "False", Error = ErrorMessage.NotCompleted, Success = String.Empty });
            }
        }

        public JsonResult Finish(int id = 0)
        {
            try
            {
                _subServicePlanApp.Finish(id);
                return Json(new { Message = "Finish", Success = SuccessMessage.Stop, Error = String.Empty });
            }
            catch
            {
                return Json(new { Message = "False", Error = ErrorMessage.NotCompleted, Success = String.Empty });
            }
        }
        public JsonResult Refresh(int id = 0)
        {
            try
            {
                _subServicePlanApp.Refresh(id);
                return Json(new { Message = "Refresh", Success = SuccessMessage.Stop, Error = String.Empty });
            }
            catch
            {
                return Json(new { Message = "False", Error = ErrorMessage.NotCompleted, Success = String.Empty });
            }
        }

    }
}