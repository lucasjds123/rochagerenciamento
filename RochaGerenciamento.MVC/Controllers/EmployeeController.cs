﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;
using RochaEmpreendimento.Utils;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador")]
    public class EmployeeController : BaseController<Employee>
    {
        IAppServiceBase<Position> _positionApp;
        IAppServiceBase<EmployeeStatus> _statusApp;
        public EmployeeController(IEmployeeAppService entityApp, IAppServiceBase<User> userApp, IAppServiceBase<Position> positionApp, IAppServiceBase<EmployeeStatus> statusApp)
            : base(entityApp, userApp)
        {
            _positionApp = positionApp;
            _statusApp = statusApp;
            ViewBag.CodePosition = new SelectList(_positionApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeEmployeeStatus = new SelectList(_statusApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.ThirdParty = DropDownList.ListYesNo();
     
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodePosition = new SelectList(_positionApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodePosition);
            ViewBag.CodeEmployeeStatus = new SelectList(_statusApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeEmployeeStatus);
            ViewBag.ThirdParty = DropDownList.ListYesNo(entity.ThirdParty);
            return View(entity);
        }

    }
}