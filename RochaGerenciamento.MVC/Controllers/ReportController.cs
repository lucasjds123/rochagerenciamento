﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CsvHelper;
using System.Collections;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.StoredProcedures;
using System.Net;
using System.ComponentModel;
using RochaEmpreendimento.Utils;
using RochaGerenciamento.Utils;

namespace RochaGerenciamento.MVC.Controllers
{
    public class ReportController : Controller
    {

        private IConstructionAppService _constructionAppService;
        private IProductAppService _productAppService;
        private IProviderAppService _providerAppService;
        private IUnitTypeAppService _unitTypeAppService;
        private IOrderAppService _orderAppService;
        private IAppServiceBase<Situation> _situationAppService;
        private IUserAppService _userApp;
        private IReportAppService _reportAppService;
        private IEmployeeAppService _employeeAppService;


        public ReportController(IConstructionAppService constructionAppService, IProductAppService productAppService, IProviderAppService providerAppService,
                                IUnitTypeAppService unitTypeAppService, IAppServiceBase<Situation> situationAppService, IProductConstructionAppService productConstructionAppService,
                                IOrderAppService orderAppService, IReportAppService reportAppService, IUserAppService userApp , IEmployeeAppService employeeAppService)
        {
            _constructionAppService = constructionAppService;
            _productAppService = productAppService;
            _providerAppService = providerAppService;
            _unitTypeAppService = unitTypeAppService;
            _situationAppService = situationAppService;
            _orderAppService = orderAppService;
            _userApp = userApp;
            _employeeAppService = employeeAppService;
           
            _reportAppService = reportAppService;
            ViewBag.CodeConstruction = new SelectList(_constructionAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeProduct = new SelectList(_productAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeProvider = new SelectList(_providerAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeUnitType = new SelectList(_unitTypeAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeSituation = new SelectList(_situationAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeOrder = new SelectList(_orderAppService.OrderList().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.TypeReport = DropDownList.ListTypeReport();
            ViewBag.Approval = DropDownList.ListYesNo();
        }

        [Authorize(Roles = "administrador,almoxarifado,encarregado,compras")]   
        public ActionResult Stock()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "administrador,almoxarifado,encarregado,compras")]   
        public JsonResult Stock(int codeSituation = 0, int codeConstruction = 0, int codeProduct = 0, int codeProvider= 0 , int codeUnitType = 0, string dateStart = "", string dateFinish = "")
        {
            try
            {
                var list = _reportAppService.Stock(codeSituation, codeConstruction, codeProduct, codeProvider, codeUnitType, dateStart, dateFinish);
           

                return Json(new { Message = "Success", Success = SuccessMessage.Report, Error = String.Empty, List = list.ToList() });
            }
            catch
            {
                return Json(new { Message = "Error", Error = ErrorMessage.Report, Success = String.Empty });
            }
            
        
        }

        [Authorize(Roles = "administrador,compras,financeiro")]   
        public ActionResult Order()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "administrador,compras,financeiro")]
        public JsonResult Order(int approval = -1, int codeConstruction = 0, string dateStart = "", string dateFinish = "")
        {
            try
            {
                var list = _reportAppService.Order(approval, codeConstruction, dateStart, dateFinish);

                return Json(new { Message = "Success", Success = SuccessMessage.Report, Error = String.Empty, List = list.ToList() });
            }
            catch
            {
                return Json(new { Message = "Error", Error = ErrorMessage.Report, Success = String.Empty });
            }


        }
        [Authorize(Roles = "administrador,compras,financeiro,vendedor")]
        public ActionResult ExpenditureVsIncoming()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "administrador,compras,financeiro,vendedor")]
        public JsonResult ExpenditureVsIncoming( int codeConstruction =0 , string dateStart = "", string dateFinish = "")
        {
            try
            {
                var list = _reportAppService.ExpenditureVsIncoming(codeConstruction, dateStart, dateFinish);

                return Json(new { Message = "Success", Success = SuccessMessage.Report, Error = String.Empty, List = list.ToList() });
            }
            catch
            {
                return Json(new { Message = "Error", Error = ErrorMessage.Report, Success = String.Empty });
            }


        }
        [Authorize(Roles = "administrador,financeiro,vendedor")]
        public ActionResult Incoming()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "administrador,financeiro,vendedor")]
        public JsonResult Incoming(int codeConstruction = 0, string dateStart = "", string dateFinish = "")
        {
            try
            {
                var list = _reportAppService.Incoming(codeConstruction, dateStart, dateFinish);

                return Json(new { Message = "Success", Success = SuccessMessage.Report, Error = String.Empty, List = list.ToList() });
            }
            catch
            {
                return Json(new { Message = "Error", Error = ErrorMessage.Report, Success = String.Empty });
            }


        }

        [Authorize(Roles = "administrador,compras")]
        public ActionResult FilterOrder()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "administrador,compras")]
        public JsonResult FilterOrder(int codeOrder = 0)
        {
            try
            {
                var list = _reportAppService.FilterOrder(codeOrder);

                return Json(new { Message = "Success", Success = SuccessMessage.Report, Error = String.Empty, List = list.ToList() });
            }
            catch
            {
                return Json(new { Message = "Error", Error = ErrorMessage.Report, Success = String.Empty });
            }


        }

        [HttpPost]
        public ActionResult SendOrderToProvider(HttpPostedFileBase uploadFile, string to)
        {
            try
            {
                if (uploadFile == null || to == string.Empty)
                {
                    TempData["Error"] = ErrorMessage.Email;
                    return RedirectToAction("FilterOrder");
                }
                var from = _userApp.GetEmailUserByProfile(FlagProfile.Compras.ToString());
                Email.Send(Email.SubjectOrder, Email.BodyOrder, from, to, uploadFile);
                TempData["Success"] = SuccessMessage.Email;
                return RedirectToAction("FilterOrder");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return RedirectToAction("FilterOrder");
            }
            
        }

        [Authorize(Roles = "administrador,almoxarifado")]
        public ActionResult EmployeeVsProduct()
        {
            ViewBag.CodeEmployee = new SelectList(_employeeAppService.GetAll(), GeneralField.Code, GeneralField.Name);
            return View();
        }

        [Authorize(Roles = "administrador,almoxarifado")]
        [HttpPost]
        public ActionResult EmployeeVsProduct(int codEmployee,  string dateStart = "", string dateFinish = "")
        {
            try
            {
                var list = _reportAppService.EmployeeVsProduct(codEmployee, dateStart, dateFinish);

                return Json(new { Message = "Success", Success = SuccessMessage.Report, Error = String.Empty, List = list.ToList() });
            }
            catch (Exception ex)
            {
                return Json(new { Message = "Error", Error = ErrorMessage.Report, Success = String.Empty });
            }

        }

        [Authorize(Roles = "administrador,compras,vendedor")]
        public ActionResult AttendanceDate()
        {
            
            return View();
        }

        [Authorize(Roles = "administrador,compras,vendedor")]
        [HttpPost]
        public ActionResult AttendanceDate(string year)
        {

            try
            {
                var list = _reportAppService.AttendanceDate(year);

                return Json(new { Message = "Success", Success = SuccessMessage.Report, Error = String.Empty, List = list.ToList() });
            }
            catch (Exception ex)
            {
                return Json(new { Message = "Error", Error = ErrorMessage.Report, Success = String.Empty });
            }
        }

        [Authorize(Roles = "administrador,compras,vendedor")]
        public ActionResult ContactOrigin()
        {
            ViewBag.CodeMonths = DropDownList.Months();
            return View();
        }

        [Authorize(Roles = "administrador,compras,vendedor")]
        [HttpPost]
        public ActionResult ContactOrigin(string year, string month)
        {

            try
            {
                var list = _reportAppService.ContactOrigin(year, month);

                return Json(new { Message = "Success", Success = SuccessMessage.Report, Error = String.Empty, List = list.ToList() });
            }
            catch (Exception ex)
            {
                return Json(new { Message = "Error", Error = ErrorMessage.Report, Success = String.Empty });
            }
        }

        [Authorize(Roles = "administrador,compras,vendedor")]
        public ActionResult OwnerPurchases()
        {
            ViewBag.CodeMonths = DropDownList.Months();
            return View();
        }

        [Authorize(Roles = "administrador,compras,vendedor")]
        [HttpPost]
        public ActionResult OwnerPurchases(string year, string month)
        {

            try
            {
                var list = _reportAppService.OwnerPurchases(year, month);

                return Json(new { Message = "Success", Success = SuccessMessage.Report, Error = String.Empty, List = list.ToList() });
            }
            catch (Exception ex)
            {
                return Json(new { Message = "Error", Error = ErrorMessage.Report, Success = String.Empty });
            }
        }

        [Authorize(Roles = "administrador,compras,vendedor")]
        [HttpGet]
        public ActionResult Clients()
        {
            
            try
            {
                var list =  _reportAppService.Clients().ToList();
                return View(list);
            }
            catch (Exception ex)
            {
                return View();
            }
        }



    }
}

