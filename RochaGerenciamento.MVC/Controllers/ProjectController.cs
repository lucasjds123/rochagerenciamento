﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador")]
    public class ProjectController : BaseController<Project>
    {
        ISubServiceAppService _subServiceApp;
        public ProjectController(IProjectAppService entityApp, IAppServiceBase<User> userApp, ISubServiceAppService subServiceApp)
            : base(entityApp, userApp)
        {
            _subServiceApp = subServiceApp;
            ViewBag.CodeSubService = new SelectList(_subServiceApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeSubService = new SelectList(_subServiceApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeSubService);
            return View(entity);
        }
    }
}