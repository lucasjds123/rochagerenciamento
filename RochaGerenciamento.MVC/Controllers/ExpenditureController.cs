﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,compras")]
    public class ExpenditureController : BaseController<Expenditure>
    {
        IConstructionAppService _constructionAppService;
        IAppServiceBase<ExpenditureType> _expenditureTypeAppService;
  

        public ExpenditureController(IExpenditureAppService entityApp, IAppServiceBase<User> userApp, IConstructionAppService constructionAppService,
                                    IAppServiceBase<ExpenditureType> expenditureTypeAppService)
            : base(entityApp, userApp)
        {
            _constructionAppService = constructionAppService;
            _expenditureTypeAppService = expenditureTypeAppService;
            ViewBag.CodeConstruction = new SelectList(_constructionAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeExpenditureType = new SelectList(_expenditureTypeAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeConstruction = new SelectList(_constructionAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeConstruction);
            ViewBag.CodeExpenditureType = new SelectList(_expenditureTypeAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeExpenditureType);
            return View(entity);
        }
    }
}