﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Application;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using System.Reflection;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    public abstract class BaseController<TEntity> : Controller where TEntity : class
    {
        protected IAppServiceBase<User> _usuariosApp;
        protected IAppServiceBase<TEntity> _entityApp;

        public BaseController(IAppServiceBase<TEntity> entityApp, IAppServiceBase<User> usuariosApp)
        {
            _entityApp = entityApp;
            _usuariosApp = usuariosApp;
        }

        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            return View(entity);
        }

        public bool CheckIfExists(TEntity entity)
        {
            if (entity == null)
            {
                TempData["Error"] = ErrorMessage.RegisterNotFound;
                return true;
            }
            return false;
        }

        [HttpPost]
        public virtual ActionResult Edit(TEntity entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _entityApp.Update(entity);
                    TempData["Success"] = SuccessMessage.Alter;
                    return RedirectToAction("Index");
                }
                TempData["Error"] = ErrorMessage.FieldRequired;
                return View(entity);
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return View();
            }

        }

        public virtual ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult Create(TEntity entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _entityApp.Add(entity);
                    TempData["Success"] = SuccessMessage.Add;
                    return RedirectToAction("Index");
                }
                TempData["Error"] = ErrorMessage.FieldRequired;
                return View();
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return View();
            }
        }

        public virtual ActionResult Delete(int id = 0)
        {
            try
            {
                var obj = _entityApp.GetById(id);
                _entityApp.Remove(obj);
                TempData["Success"] = SuccessMessage.Delete;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return RedirectToAction("Index");
            }
        }


        [System.Web.Mvc.HttpPost]
        public JsonResult List(int current, int rowCount, Dictionary<string, string> sort, string searchPhrase, int code = 0)
        {
            int total = 0;
            var list = _entityApp.GetDynamicList(searchPhrase, rowCount, current, ref total, code);

            return Json(new
            {
                current = current,
                rowCount = rowCount,
                rows = list,
                total = total
            });
        }

        public virtual ActionResult Details(int id)
        {
            TEntity entity = _entityApp.GetById(id);
            if (entity == null)
            {
                TempData["Error"] = ErrorMessage.RegisterNotFound;
                return RedirectToAction("Index");
            }
            return View(entity);
        }
    }
}

public class RequireRequestValueAttribute : ActionMethodSelectorAttribute
{
    public RequireRequestValueAttribute(string valueName)
    {
        ValueName = valueName;
    }
    public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
    {
        return (controllerContext.HttpContext.Request[ValueName] != null);
    }
    public string ValueName { get; private set; }
}