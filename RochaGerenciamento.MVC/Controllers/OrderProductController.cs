﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RochaGerenciamento.MVC.Controllers
{
    public class OrderProductController : BaseController<OrderProduct>
    {
        
        public OrderProductController(IOrderProductAppService entityApp, IAppServiceBase<User> userApp)
            : base(entityApp, userApp)
        {
            
        }

    }
}
