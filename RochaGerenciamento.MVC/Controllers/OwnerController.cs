﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,vendas,compras")]
    public class OwnerController : BaseController<Owner>
    {
        IOwnerAppService _ownerApp;
        IClientAppService _clientApp;
        IUnitAppService _unitApp;
        IAppServiceBase<StatusPurchase> _statusApp;
        public OwnerController(IOwnerAppService entityApp, IAppServiceBase<User> userApp, IClientAppService clientApp, IUnitAppService unitApp, IAppServiceBase<StatusPurchase> statusApp)
            : base(entityApp, userApp)
        {
            _clientApp = clientApp;
            _unitApp = unitApp;
            _statusApp = statusApp;
            ViewBag.CodeClient = new SelectList(_clientApp.DropDownList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeUnit = new SelectList(_unitApp.DropDownList(), GeneralField.Code, GeneralField.UnitValue);
            ViewBag.CodeStatusPurchase = new SelectList(_statusApp.GetAll(), GeneralField.Code, GeneralField.Name);
           
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeClient = new SelectList(_clientApp.DropDownList(), GeneralField.Code, GeneralField.Name, entity.CodeClient);
            ViewBag.CodeUnit = new SelectList(_unitApp.DropDownList(), GeneralField.Code, GeneralField.UnitValue, entity.CodeUnit);
            ViewBag.CodeStatusPurchase = new SelectList(_statusApp.GetAll(), GeneralField.Code, GeneralField.Name, entity.CodeStatusPurchase); ;
            return View(entity);
        }
    }
}