﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Utils;

namespace RochaGerenciamento.MVC.Controllers
{
   
    public class ProductConstructionController : BaseController<ProductConstruction>
    {
        IProductConstructionAppService _productConstructionAppService;
        IConstructionAppService _constructionAppService;
        IEmployeeAppService _employeeAppService;

        public ProductConstructionController(IProductConstructionAppService entityApp, IAppServiceBase<User> userApp,
                    IProductConstructionAppService productConstructionAppService, IConstructionAppService constructionAppService, 
                    IEmployeeAppService employeeAppService)
            : base(entityApp, userApp)
        {
            _constructionAppService = constructionAppService;
            _employeeAppService = employeeAppService;
            _productConstructionAppService = productConstructionAppService;
            RochaGerenciamento.Domain.ValueObject.Session.AmountNotVerified = _productConstructionAppService.AmountInStock((int) FlagSituation.Verified).ToString();
 
        }

        [Authorize(Roles = "administrador,almoxarifado")]
        public override ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "administrador,almoxarifado")]
        public ActionResult Confirm(string id )
        {
            var entities = _productConstructionAppService.GetListByIds(id);
            try
            {
                if (_productConstructionAppService.CheckSituation(entities))
                {
                    TempData["Error"] = ErrorMessage.AlreadyConfirmed;
                    return RedirectToAction("Index");
                }
                if (_productConstructionAppService.CheckVerified(entities))
                {
                    TempData["Error"] = ErrorMessage.NeedsToBeConfirmed;
                    return RedirectToAction("Index");
                }
                ViewBag.CodeConstruction = new SelectList(_constructionAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
                ViewBag.CodeEmployee = new SelectList(_employeeAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
                return View(entities);
            }
            catch
            {

                TempData["Error"] = ErrorMessage.NotExist;
                return RedirectToAction("Index");
            }
            
        
        }

        [Authorize(Roles = "administrador,almoxarifado")]
        [HttpPost]
        public ActionResult Confirm(int codeConstruction, double[] amounts, int[] codeProducts, int[] codeProductConstructions, int codeEmployee, string password)
        {
            try
            {
                if (_employeeAppService.CheckPassword(codeEmployee, password))
                {
                    TempData["Error"] = ErrorMessage.EmployeePassword;
                    return RedirectToAction("Confirm", codeProductConstructions);
                }
                if (_productConstructionAppService.AmountInStock(codeProductConstructions, codeProducts, amounts, (int)FlagSituation.Available))
                {
                    _productConstructionAppService.UpdateSituation(amounts, (int)FlagSituation.NotAvailable, codeProducts, codeConstruction, codeEmployee, null, codeProductConstructions);
                    TempData["Success"] = SuccessMessage.Confirm;
                    return RedirectToAction("Index");
                }
                TempData["Error"] = ErrorMessage.AmountStock;
                return RedirectToAction("Confirm", codeProductConstructions);
            }
            catch(Exception e)
            {
                TempData["Error"] = e.Message;
                return RedirectToAction("Confirm", codeProductConstructions);
            }
        }

        public JsonResult Back(double amount, int codProduct, int codConstruction, int codEmployee, DateTime date, int codProductConstruction)
        {
            try
            {
                _productConstructionAppService.UpdateSituation(new double[1] { amount }, (int)FlagSituation.Available, new int[1] { codProduct }, codConstruction, codEmployee, date, new int[1] { codProductConstruction });
                return Json(new { Message = "Back", Success = SuccessMessage.Back, Error = String.Empty });
            }
            catch
            {
                return Json(new { Message = "False", Error = ErrorMessage.NotCompleted, Success = String.Empty });
            }
        }

        public JsonResult Recycle(double amount, int codProduct, int codConstruction = 0)
        {
            try
            {
                _productConstructionAppService.UpdateSituation(new double[1] { amount }, (int)FlagSituation.NoBond, new int[1] { codProduct }, codConstruction);
                return Json(new { Message = "Recycle", Success = SuccessMessage.Recycle, Error = String.Empty });
            }
            catch
            {
                return Json(new { Message = "False", Error = ErrorMessage.NotCompleted, Success = String.Empty });
            }
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeConstruction = new SelectList(_constructionAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            return View(entity);
        }

        [Authorize(Roles = "compras,encarregado")]
        public ActionResult ListProductConstruction()
        {
            return View();
        }

        
        public ActionResult Receipt(string employee , string amount, string product, string construction, string date)
        {
            ViewBag.Employee =  employee;
            ViewBag.Amount = amount;
            ViewBag.Product =  product;
            ViewBag.Construction = construction;
            ViewBag.Date = date;
            return View();
        }

        [ActionName("ReceiptAux")]
        public ActionResult Receipt(string employee, string amount, string productConstruction)
        {
            ViewBag.Employee = _employeeAppService.GetById(Convert.ToInt32(employee)).Name;
            ViewBag.Amount = amount;
            ViewBag.Product = _productConstructionAppService.GetById(Convert.ToInt32(productConstruction)).Product.Name ;
            ViewBag.Construction = _productConstructionAppService.GetById(Convert.ToInt32(productConstruction)).Construction.Name;
            return View("Receipt");
        }




        

    }
}