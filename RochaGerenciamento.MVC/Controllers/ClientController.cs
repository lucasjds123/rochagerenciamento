﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,vendedor")]
    public class ClientController : BaseController<Client>
    {
        IAppServiceBase<CivilStatus> _civilStatusApp;
        IAppServiceBase<ContactOrigin> _contactOriginApp;
        IAddressAppService _addressApp;
        public ClientController(IClientAppService entityApp, IAppServiceBase<User> userApp, IAppServiceBase<CivilStatus> civilStatusApp,
                                IAppServiceBase<ContactOrigin> contactOriginApp, IAddressAppService addressApp)
            : base(entityApp, userApp)
        {
            _contactOriginApp = contactOriginApp;
            _civilStatusApp = civilStatusApp;
            _addressApp = addressApp;
            ViewBag.CodeContactOrigin = new SelectList(_contactOriginApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeCivilStatus = new SelectList(_civilStatusApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeAddress = new SelectList(_addressApp.AddressList(), GeneralField.Code, GeneralField.Name);
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeContactOrigin = new SelectList(_contactOriginApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeContactOrigin);
            ViewBag.CodeCivilStatus = new SelectList(_civilStatusApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeCivilStatus);
            ViewBag.CodeAddress = new SelectList(_addressApp.AddressList(), GeneralField.Code, GeneralField.Name, entity.CodeAddress);
            return View(entity);
        }

        [HttpPost]
        [ActionName("CreateAux")]
        public ActionResult Create(Client entity, string submit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _entityApp.Add(entity);
                    TempData["Success"] = SuccessMessage.Add;
                    if (string.IsNullOrEmpty(submit))
                        return RedirectToAction("Create", "Owner");
                    
                    return RedirectToAction("Index");
                }
                TempData["Error"] = ErrorMessage.FieldRequired;
                return View();
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return View();
            }

        }

    }
}