﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador")]
    public class UserController : BaseController<User>
    {
        IAppServiceBase<Profile> _profileApp;

        public UserController(IUserAppService entityApp, IAppServiceBase<Profile> profileApp)
            : base(entityApp, entityApp)
        {
            _profileApp = profileApp;
            ViewBag.CodeProfile = new SelectList(_profileApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeProfile = new SelectList(_profileApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeProfile);
            return View(entity);
        }
        
    }
}