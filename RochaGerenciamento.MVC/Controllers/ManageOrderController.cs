﻿using RochaEmpreendimento.Utils;
using RochaGerenciamento.Application;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RochaGerenciamento.MVC.Controllers
{
    public class ManageOrderController : BaseController<Order>
    {
        private IManageOrderAppService _manageOrderAppService;
        private IOrderAppService _orderApp;
        private IUserAppService _userApp;
        private IConstructionAppService _constructionApp;
        private IProductAppService _productApp;

        public ManageOrderController(IOrderAppService orderApp, IUserAppService userApp, IManageOrderAppService managerOrderAppService, 
                                    IConstructionAppService constructionApp, IProductAppService productApp)
            :base (orderApp,userApp)
        {
            _manageOrderAppService = managerOrderAppService;
            _orderApp = orderApp;
            _userApp = userApp;
            _productApp = productApp;
            _constructionApp = constructionApp;
            ViewBag.CodeConstructionDestination = new SelectList(_constructionApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeProduct = new SelectList(_productApp.ProductList(), GeneralField.Code, GeneralField.ProductValue);
        }

        public ActionResult Approve(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (entity.Approval.Equals(RegisterSituation.Active))
            {
                TempData["Error"] = ErrorMessage.Approved;
                return RedirectToAction("ListVerifyOrder");
            }
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            return View(entity);
        }

        
        public ActionResult Verify(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (entity.Approval.Equals(RegisterSituation.NonActive))
            {
                TempData["Error"] = ErrorMessage.NotApproved;
                return RedirectToAction("ListVerifyOrder");
            }
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.VerifiedResult = DropDownList.ListYesNo( entity.VerifiedResult);
            return View(entity);
        }

        [HttpPost]
        public ActionResult Approve(Order entity, string message)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    _manageOrderAppService.ApprovalOrder(entity);
                    var from = _userApp.GetEmailUserByProfile(FlagProfile.Financeiro.ToString());
                    var to = _userApp.GetEmailUserByProfile(FlagProfile.Almoxarifado.ToString());
                    Email.Send(message, Email.SubjectFinanceiro + entity.NumberOrder, from, to);

                    TempData["Success"] = SuccessMessage.Approval;
                    return RedirectToAction(RochaGerenciamento.Domain.ValueObject.Session.UrlOrder);
                }
                TempData["Error"] = ErrorMessage.FieldRequired;
                return RedirectToAction(RochaGerenciamento.Domain.ValueObject.Session.UrlOrder);
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return RedirectToAction(RochaGerenciamento.Domain.ValueObject.Session.UrlOrder);
            }

        }

        [HttpPost]
        public ActionResult Verify(Order entity, string message)
        {
            try
            {
               
                _manageOrderAppService.VerifyOrder(entity);
                var from = _userApp.GetEmailUserByProfile(FlagProfile.Almoxarifado.ToString());
                var to = _userApp.GetEmailUserByProfile(FlagProfile.Compras.ToString());
                Email.Send(message, Email.SubjectVerified + entity.NumberOrder, from, to);
                TempData["Success"] = SuccessMessage.Verified;
                return RedirectToAction("ListVerifyOrder");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return RedirectToAction("Verify", entity.Code);
            }
            
        }

        [Authorize(Roles = "administrador,compras")]
        public ActionResult ListOrder()
        {
            return View();
        }

        [Authorize(Roles = "administrador,almoxarifado")]
        public ActionResult ListVerifyOrder()
        {
            return View();
        }

        public override ActionResult Create()
        {
            ViewBag.NumberOrder = _orderApp.GetNextNumberOrder();
            return View();
        }
        
        [HttpPost]
        [ActionName("CreateAux")]
        public ActionResult Create(Order entity, string message, bool[] permanentPrice)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _manageOrderAppService.AddOrder(entity, permanentPrice);
                    var from = _userApp.GetEmailUserByProfile(FlagProfile.Compras.ToString());
                    var to = _userApp.GetEmailUserByProfile(FlagProfile.Almoxarifado.ToString()) + _userApp.GetEmailUserByProfile(FlagProfile.Financeiro.ToString());
                    Email.Send(message, Email.SubjectAlmoxarifado + entity.NumberOrder, from, to) ;
            
                    TempData["Success"] = SuccessMessage.AddAndEmailSent;
                    return RedirectToAction("Index");
                }
                TempData["Error"] = ErrorMessage.FieldRequired;
                return View("Index");
            }
            catch
            {
                TempData["Error"] = ErrorMessage.AdminError;
                return View("Index");
            }

        }

        [Authorize(Roles = "administrador,compras,financeiro,almoxarifado")]
        [System.Web.Mvc.HttpPost]
        public JsonResult ListOrder(int current, int rowCount, Dictionary<string, string> sort, string searchPhrase
                                    , string nameConstruction, string numberOrder, string nameProvider)
        {
            int total = 0;
            var list = _orderApp.GetDynamicList(searchPhrase, rowCount, current, ref total, nameConstruction, numberOrder, nameProvider);

            return Json(new
            {
                current = current,
                rowCount = rowCount,
                rows = list,
                total = total
            });
        }

        public override ActionResult Delete(int id = 0)
        {
            try
            {
                var obj = _entityApp.GetById(id);
                _entityApp.Remove(obj);
                TempData["Success"] = SuccessMessage.Delete;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return RedirectToAction("Index");
            }
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeConstructionDestination = new SelectList(_constructionApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeConstructionDestination);    
            ViewBag.CodeProduct = new MultiSelectList(_productApp.ProductList(), GeneralField.Code, GeneralField.ProductValue, entity.OrderProducts.Select( x => x.CodeProduct).ToList());
            return View(entity);
        }

    }
}
