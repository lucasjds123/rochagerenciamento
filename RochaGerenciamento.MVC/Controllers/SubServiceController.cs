﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador")]
    public class SubServiceController : BaseController<SubService>
    {
        IAppServiceBase<Service> _serviceApp;
        IProjectAppService _projectApp;
        public SubServiceController(ISubServiceAppService entityApp, IAppServiceBase<User> userApp, IAppServiceBase<Service> serviceApp , IProjectAppService projectApp)
            : base(entityApp, userApp)
        {
            _serviceApp = serviceApp;
            _projectApp = projectApp;
            ViewBag.CodeService = new SelectList(_serviceApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeService = new SelectList(_serviceApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeService);
            return View(entity);
        }
    }
}