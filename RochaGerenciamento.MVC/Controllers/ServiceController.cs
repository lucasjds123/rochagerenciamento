﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador")]
    public class ServiceController : BaseController<Service>
    {
        public ServiceController(IAppServiceBase<Service> entityApp, IAppServiceBase<User> userApp)
            : base(entityApp, userApp)
        {
        }
    }
}