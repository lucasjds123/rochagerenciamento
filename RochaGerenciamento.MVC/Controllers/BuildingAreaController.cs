﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,encarregado")]
    public class BuildingAreaController : BaseController<BuildingArea>
    {
        IBuildingAreaAppService _buildingApp;
        IConstructionAppService _constructionApp;
        public BuildingAreaController(IBuildingAreaAppService entityApp, IConstructionAppService constructionApp, IAppServiceBase<User> userApp)
            : base(entityApp, userApp)
        {
            _buildingApp = entityApp;
            _constructionApp = constructionApp;
            ViewBag.CodeConstruction = new SelectList(_constructionApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeConstruction = new SelectList(_constructionApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeConstruction);
            
            return View(entity);
        }
    }
}