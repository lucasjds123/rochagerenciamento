﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,vendedor")]
    public class UnitController : BaseController<Unit>
    {
        IAppServiceBase<Localization> _localizationAppService;
        IConstructionAppService _constructionAppService;

        public UnitController(IUnitAppService entityApp, IAppServiceBase<User> userApp, IConstructionAppService constructionAppService,
                            IAppServiceBase<Localization> localizationAppService)
            : base(entityApp, userApp)
        {
            _constructionAppService = constructionAppService;
            _localizationAppService = localizationAppService;
            ViewBag.CodeConstruction = new SelectList(_constructionAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeLocalization = new SelectList(_localizationAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeConstruction = new SelectList(_constructionAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeConstruction);
            ViewBag.CodeLocalization = new SelectList(_localizationAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeLocalization);
            return View(entity);
        }
    }
}