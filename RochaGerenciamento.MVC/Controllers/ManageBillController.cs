﻿using RochaEmpreendimento.Utils;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,financeiro,compras")]
    public class ManageBillController : BaseController<Bill>
    {
        private IManageBillAppService _manageBillAppService;
        private IOrderAppService _orderAppService;
        private IBillAppService _billAppService;

        public ManageBillController(IBillAppService billApp, IUserAppService userApp, IManageBillAppService managerBillAppService, IOrderAppService orderAppService)
            : base(billApp, userApp)
        {
            _orderAppService = orderAppService;
            _manageBillAppService = managerBillAppService;
            _billAppService = billApp;
            
            ViewBag.CodeOrder = new SelectList(_orderAppService.OrderList(), GeneralField.Code, GeneralField.Name);
            ViewBag.Checked = DropDownList.ListYesNo();
        }

        public override ActionResult Index()
        {
            _manageBillAppService.AmountAboutToExpire();
            return View();
        }

        [HttpPost]
        [ActionName("CreateAux")]
        public ActionResult Create(Bill entity, int amountInstallments, DateTime dueDate)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var orders = _manageBillAppService.CreateOrders(entity.Orders);
                    var installments = _manageBillAppService.CreateInstallments(entity.Checked, entity.TotalValue, amountInstallments, dueDate);
                    _manageBillAppService.AddBill(entity, orders, installments);
                    TempData["Success"] = SuccessMessage.Add;
                    return RedirectToAction("Index");
                }
                TempData["Error"] = ErrorMessage.FieldRequired;
                return View("Create");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return View("Create");
            }

        }

        public override ActionResult Delete(int id = 0)
        {
            try
            {
                var bill = _entityApp.GetById(id);
                _manageBillAppService.UpdateOrders(bill);
                _entityApp.Remove(bill);
                TempData["Success"] = SuccessMessage.Delete;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return RedirectToAction("Index");
            }
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.Checked = DropDownList.ListYesNo(entity.Checked);
            return View(entity);
        }

        public JsonResult Confirm(int id = 0)
        {
            try
            {
                _manageBillAppService.ConfirmPayment(id);
                return Json(new { Success = SuccessMessage.Paid, Error = String.Empty });
            }
            catch
            {
                return Json(new { Error = ErrorMessage.NotCompleted, Success = String.Empty });
            }
        }
        public JsonResult ConfirmPaymentAll(int id = 0)
        {
            try
            {
                _manageBillAppService.ConfirmPaymentAll(id);
                return Json(new { Success = SuccessMessage.PaidAll, Error = String.Empty });
            }
            catch
            {
                return Json(new { Error = ErrorMessage.NotCompleted, Success = String.Empty });
            }
        }

    }
}
