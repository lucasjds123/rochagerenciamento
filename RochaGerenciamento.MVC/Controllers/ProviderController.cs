﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;
using RochaEmpreendimento.Utils;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,compras")]
    public class ProviderController : BaseController<Provider>
    {
        IAppServiceBase<MaterialType> _materialTypeApp;
        IAddressAppService _addressApp;
        public ProviderController(IProviderAppService entityApp, IAppServiceBase<User> userApp, IAppServiceBase<MaterialType> materialTypeApp, IAddressAppService addressApp)
            : base(entityApp, userApp)
        {
            _materialTypeApp = materialTypeApp;
            _addressApp = addressApp;
            ViewBag.CodeMaterialType = new SelectList(_materialTypeApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeAddress = new SelectList(_addressApp.AddressList(), GeneralField.Code, GeneralField.Name);
            ViewBag.PSQ = DropDownList.ListYesNo();
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeMaterialType = new SelectList(_materialTypeApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeMaterialType);

            ViewBag.PSQ = DropDownList.ListYesNo(entity.PSQ);
            return View(entity);
        }

        
    }
}