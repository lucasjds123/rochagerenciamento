﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaEmpreendimento.Utils;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,vendedor,financeiro,compras")]
    public class ContractIncomingController : BaseController<ContractIncoming>
    {
        IContractAppService _contractAppService;
        IAppServiceBase<IncomingType> _incomingTypeAppService;
        IManageBillAppService _manageBillAppService;
        
        public ContractIncomingController(IContractIncomingAppService billApp, IUserAppService userApp, IContractAppService contractAppService, 
                                        IAppServiceBase<IncomingType> incomingTypeAppService, IManageBillAppService manageBillAppService)
            : base(billApp, userApp)
        {
            _contractAppService = contractAppService;
            _incomingTypeAppService = incomingTypeAppService;
            _manageBillAppService = manageBillAppService;
            ViewBag.CodeContract = new SelectList(_contractAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeIncomingType = new SelectList(_incomingTypeAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.Checked = DropDownList.ListYesNo();
        }

        [Authorize(Roles = "administrador,financeiro,compras")]
        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeIncomingType = new SelectList(_incomingTypeAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeIncomingType);
            return View(entity);
        }

        [Authorize(Roles = "administrador,financeiro,compras")]
        [HttpPost]
        [ActionName("CreateAux")]
        public ActionResult Create(ContractIncoming entity, int amountInstallments, DateTime dueDate)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var installments = _manageBillAppService.CreateInstallments(entity.Bill.Checked, entity.Bill.TotalValue, amountInstallments, dueDate);
                    _manageBillAppService.AddBill(entity.Bill, entity, installments);
                    TempData["Success"] = SuccessMessage.Add;
                    return RedirectToAction("Index");
                }
                TempData["Error"] = ErrorMessage.FieldRequired;
                return View("Create");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return View("Create");
            }

        }

        [Authorize(Roles = "administrador,financeiro,compras")]
        public ActionResult ListIncoming()
        {
            return View();
        }
    }
}