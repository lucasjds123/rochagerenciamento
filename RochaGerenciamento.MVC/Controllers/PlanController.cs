﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador")]
    public class PlanController : BaseController<Plan>
    {
        IConstructionAppService _constructionApp;
        public PlanController(IPlanAppService entityApp, IAppServiceBase<User> userApp, IConstructionAppService constructionApp)
            : base(entityApp, userApp)
        {
            _constructionApp = constructionApp;
            ViewBag.CodeConstruction = new SelectList(_constructionApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeConstruction = new SelectList(_constructionApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeConstruction);
            return View(entity);
        }
    }
}