﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,vendedor")]
    public class ContactOriginController : BaseController<ContactOrigin>
    {
        public ContactOriginController(IAppServiceBase<ContactOrigin> entityApp, IAppServiceBase<User> userApp)
            : base(entityApp, userApp)
        {
        }
    }
}