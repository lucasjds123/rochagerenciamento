﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,vendedor,compras")]
    public class ContractController : BaseController<Contract>
    {
        IClientAppService _clientApp;
        IAppServiceBase<ContractType> _contractTypeApp;
        IOwnerAppService _ownerApp;
        IContractAppService _contractApp;

        public ContractController(IContractAppService entityApp, IAppServiceBase<User> userApp, IUnitAppService unitApp, IClientAppService clientApp,
                                IAppServiceBase<ContractType> contractTypeApp, IOwnerAppService ownerApp)
            : base(entityApp, userApp)
        {
            _clientApp = clientApp;
            _contractTypeApp = contractTypeApp;
            _ownerApp = ownerApp;
            _contractApp = entityApp;
            ViewBag.CodeClient = new SelectList(_clientApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeContractType = new SelectList(_contractTypeApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeOwner = new SelectList(_ownerApp.DropDownList(), GeneralField.Code, GeneralField.OwnerValue);
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeContractType = new SelectList(_contractTypeApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeContractType);
            ViewBag.CodeOwner = new SelectList(_ownerApp.DropDownList(), GeneralField.Code, GeneralField.OwnerValue, entity.CodeOwner);
           
            return View(entity);
        }

        [HttpPost]
        public override ActionResult Create(Contract entity)
        {
            try
            {
                entity.Owner = _ownerApp.GetById(entity.CodeOwner);
                if (_contractApp.VerifyOwners(entity))
                {
                    TempData["Error"] = ErrorMessage.SameClient;
                    return View();
                }
                if (ModelState.IsValid)
                {
                    entity.Owner = null;
                    _entityApp.Add(entity);
                    TempData["Success"] = SuccessMessage.Add;
                    return RedirectToAction("Index");
                }
                TempData["Error"] = ErrorMessage.FieldRequired;
                return View();
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return View();
            }
        }
    }
}