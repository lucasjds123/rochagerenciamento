﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.MVC.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RochaGerenciamento.MVC.Controllers
{
    public class PrincipalController : Controller
    {
        IConstructionAppService _constructionAppService;
        public PrincipalController(IConstructionAppService constructionAppService)
        {
            _constructionAppService = constructionAppService;
            ViewBag.CodeConstruction = new SelectList(_constructionAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
        }

        public JsonResult SetSessionConstruction(int codeConstruction, string nameConstruction)
        {
            
            try
            {
                RochaGerenciamento.Domain.ValueObject.Session.CodeConstruction = codeConstruction;
                RochaGerenciamento.Domain.ValueObject.Session.NameConstruction = nameConstruction;
                return Json(new { Message = "ConstructionChosen", Success = SuccessMessage.ConstructionChosen, Error = String.Empty });
            }
            catch
            {
                return Json(new { Message = "False", Error = ErrorMessage.NotCompleted, Success = String.Empty });
            }
        } 

        public ActionResult Index()
        {
            //if (Authorizer.AutorizaEstoque)
            //    return RedirectToAction("Estoque", "Movimentacao");
            //if (Authorizer.AutorizaPlanejamento)
            //    return RedirectToAction("ItensBaixa", "Movimentacao");
            //if (Authorizer.AutorizaVendedor)
            //    return RedirectToAction("ItensVendidos", "Movimentacao");
            ViewBag.CodeConstruction = new SelectList(_constructionAppService.GetAll().ToList(), GeneralField.Code, GeneralField.Name, RochaGerenciamento.Domain.ValueObject.Session.CodeConstruction);
            return View();
        }

    }
}
