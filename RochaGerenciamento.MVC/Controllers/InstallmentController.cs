﻿using RochaEmpreendimento.Utils;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,financeiro")]
    public class InstallmentController : BaseController<Installment>
    {
        IInstallmentAppService _installmentApp;
        IBillAppService _billApp;
        public InstallmentController(IInstallmentAppService entityApp, IUserAppService userApp, IBillAppService billApp)
            : base(entityApp, userApp)
        {
            _installmentApp = entityApp;
            _billApp = billApp;
            
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.Checked = DropDownList.ListYesNo(entity.Checked);
            return View(entity);
        }

        [HttpPost]
        public override ActionResult Edit(Installment entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _entityApp.Update(entity);
                    TempData["Success"] = SuccessMessage.Alter;
                    return RedirectToAction("Details", RochaGerenciamento.Domain.ValueObject.Session.UrlBill,
                        new { id = RochaGerenciamento.Domain.ValueObject.Session.UrlBill == ReturnBillType.Incoming ?
                                                            _billApp.GetById(entity.CodeBill).ContractIncomings.FirstOrDefault().Code 
                                                            : entity.CodeBill });
                }
                TempData["Error"] = ErrorMessage.FieldRequired;
                return View(entity);
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return View();
            }

        }



    }
}
