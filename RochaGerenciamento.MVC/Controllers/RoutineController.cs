﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.StoredProcedures;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RochaGerenciamento.MVC.Controllers
{
    [AllowAnonymous]
    public class RoutineController : Controller
    {
        
        IProductConstructionAppService _productconstructionAppService;
        IUserAppService _userAppService;
        IInstallmentAppService _installmentAppService;
        public RoutineController(IProductConstructionAppService productConstructionAppService, IUserAppService userAppService, IInstallmentAppService installmentAppService)
        {
            _productconstructionAppService = productConstructionAppService;
            _userAppService = userAppService;
            _installmentAppService = installmentAppService;
        }

        [AllowAnonymous]
        public ActionResult AlertProduct()
        {
            string productMessage = _productconstructionAppService.ProductsAboutToExpire();
            if (productMessage == string.Empty)
                return View();
            var from = _userAppService.GetEmailUserByProfile(FlagProfile.Almoxarifado.ToString());
            var to = _userAppService.GetEmailUserByProfile(FlagProfile.Almoxarifado.ToString());
            Email.Send(productMessage, Email.SubjectLimit, from, to);
            return View();
        }

        [AllowAnonymous]
        public ActionResult AlertInstallment()
        {
            string installmentMessage = _installmentAppService.InstallmentsAboutToExpire();
            if (installmentMessage == string.Empty)
                return View();
            var from = _userAppService.GetEmailUserByProfile(FlagProfile.Financeiro.ToString());
            var to = _userAppService.GetEmailUserByProfile(FlagProfile.Financeiro.ToString());
            Email.Send(installmentMessage, Email.SubjectExpirationDate, from, to);
            return View();
        }

    }
}
