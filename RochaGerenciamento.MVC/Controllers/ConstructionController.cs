﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador")]
    public class ConstructionController : BaseController<Construction>
    {
        IAddressAppService _addressApp;
        public ConstructionController(IConstructionAppService entityApp, IAppServiceBase<User> userApp,IAddressAppService addressApp)
            : base(entityApp, userApp)
        {
            _addressApp = addressApp;
            ViewBag.CodeAddress = new SelectList(_addressApp.AddressList(), GeneralField.Code, GeneralField.Name);
        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeAddress = new SelectList(_addressApp.AddressList(), GeneralField.Code, GeneralField.Name, entity.CodeAddress);
            return View(entity);
        }
    }
}