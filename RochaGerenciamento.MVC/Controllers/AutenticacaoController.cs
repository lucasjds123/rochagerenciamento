﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using RochaGerenciamento.Utils;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [AllowAnonymous]
    public class AutenticacaoController : Controller
    {
        public readonly IUserAppService _user;

        public AutenticacaoController(IUserAppService user)
        {
            _user = user;
        }

        public ActionResult Index()
        {
            User user = SessionUser.Object;
            if (user != null && _user.CheckUserExistence(user.Login))
            {
                FormsAuthentication.SetAuthCookie(user.Login, false);
                SessionUser.Object = user;
                return RedirectToAction("Index", "Principal");
            }
            return View();
        }

        public ActionResult Timeout()
        {
            TempData["Error"] = ErrorMessage.EndSession;
            return RedirectToAction("Index", "Autenticacao");
        }

        [HttpPost]
        public ActionResult Index(User user)
        {
            try
            {
                user = _user.LookForLoginPassword(user.Login, Criptografia.Encrypt(user.Password));
                if (user != null)
                {
                    FormsAuthentication.SetAuthCookie(user.Login, false);
                    SessionUser.Object = user;
                    return RedirectToAction("Index", "Principal");
                }
                CleanSession();
                TempData["Error"] = ErrorMessage.ErrorLogin;
                return View(user);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.Message;
                return View(user);
            }
        }

        public ActionResult Logout()
        {
            CleanSession();
            return RedirectToAction("Index","Autenticacao");
        }

        public ActionResult KeepAlive()
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];//.ASPXAUTH
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            SessionUser.Object = _user.GetAll().FirstOrDefault(x => x.Login == authTicket.Name && x.IdcActive == "1");
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        private void CleanSession()
        {
            FormsAuthentication.SignOut();
            Roles.DeleteCookie();
            SessionUser.Object = null;
         
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            
        }

    }
}
