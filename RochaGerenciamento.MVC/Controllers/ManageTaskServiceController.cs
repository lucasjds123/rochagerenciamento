﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Enums;

namespace RochaGerenciamento.MVC.Controllers
{
    [Authorize(Roles = "administrador,encarregado")]
    public class ManageTaskServiceController : BaseController<TaskService>
    {
        ITaskServiceAppService _taskServiceApp;
        IAppServiceBase<Task> _taskApp;
        IConstructionAppService _constructionApp;
        IBuildingAreaAppService _areaApp;
        IUnitAppService _unitApp;
        IEmployeeAppService _employeeApp;
        IManageTaskServiceAppService _manageTaskServiceApp;
        public ManageTaskServiceController(ITaskServiceAppService entityApp, IAppServiceBase<User> userApp, IAppServiceBase<Task> taskApp ,
                                           IConstructionAppService constructionApp, IBuildingAreaAppService areaApp, IEmployeeAppService employeeApp,
                                            IManageTaskServiceAppService manageTaskServiceApp, IUnitAppService unitApp)
            : base(entityApp, userApp)
        {
            _taskServiceApp = entityApp;
            _taskApp = taskApp;
            _constructionApp = constructionApp;
            _areaApp = areaApp;
            _employeeApp = employeeApp;
            _manageTaskServiceApp = manageTaskServiceApp;
            _unitApp = unitApp;
            ViewBag.CodeTask = new SelectList(_taskApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeConstruction = new SelectList(_constructionApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeBuildingArea = new SelectList(_areaApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeUnit = new SelectList(_unitApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            ViewBag.CodeEmployeeChecker = new SelectList(_employeeApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            
        }

        public ActionResult ListTasks()
        {
            return View();
        }

        public ActionResult Start(int id = 0)
        {
            var entity = _taskServiceApp.GetById(id);
            if (entity.DateTimeStart != null || entity.CodeTaskSituation != (int)FlagTaskSituation.Open)
            {
                 TempData["Error"] = ErrorMessage.TaskStartedAlready;
                 return RedirectToAction("Index");
            }
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeEmployee = new SelectList(_employeeApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            return View(entity);
        }

        [HttpPost]
        public ActionResult Start(TaskService taskService)
        {
            try
            {
                _manageTaskServiceApp.AddTaskService(taskService);
                _taskServiceApp.GetNextNumberTask(taskService.Code, taskService.DateTimeStart.Value.ToString("yyyy-MM-dd"));
                TempData["Success"] = SuccessMessage.TaskStarted;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return RedirectToAction("Start", taskService.Code);
            }
            
        }

        public ActionResult Close(int id = 0)
        {
            
            var entity = _taskServiceApp.GetById(id);
            if (entity.CodeTaskSituation > (int)FlagTaskSituation.Open)
            {
                TempData["Error"] = ErrorMessage.TaskClosedAlready;
                return RedirectToAction("Index");
            }
            if (entity.DateTimeStart == null)
            {
                TempData["Error"] = ErrorMessage.TaskCannotBeClosed;
                return RedirectToAction("Index");
            }
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeEmployeeFinisher = new SelectList(_employeeApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name);
            return View(entity);
        }

        [HttpPost]
        public ActionResult Close(TaskService taskService)
        {
            try
            {
                _taskServiceApp.Update(taskService);
                TempData["Success"] = SuccessMessage.TaskClosed;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return RedirectToAction("Start", taskService.Code);
            }

        }

        public ActionResult Confirm(int id = 0)
        {
            var entity = _taskServiceApp.GetById(id);
            if (entity.CodeTaskSituation == (int)FlagTaskSituation.Confirmed)
            {
                TempData["Error"] = ErrorMessage.TaskConfirmedAlready;
                return RedirectToAction("Index");
            }
            if (entity.CodeTaskSituation == (int)FlagTaskSituation.Open)
            {
                TempData["Error"] = ErrorMessage.TaskNotClosed;
                return RedirectToAction("Index");
            }
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            
            return View(entity);
        }

        [HttpPost]
        public ActionResult Confirm(TaskService taskService)
        {
            try
            {
                _taskServiceApp.Update(taskService);
                TempData["Success"] = SuccessMessage.TaskConfirmed;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return RedirectToAction("Start", taskService.Code);
            }

        }

        public ActionResult Unconfirm(int id = 0)
        {
            var entity = _taskServiceApp.GetById(id);
            if (entity.CodeTaskSituation == (int)FlagTaskSituation.Confirmed)
            {
                TempData["Error"] = ErrorMessage.TaskConfirmedAlready;
                return RedirectToAction("Index");
            }
            if (entity.CodeTaskSituation == (int)FlagTaskSituation.Open)
            {
                TempData["Error"] = ErrorMessage.TaskNotClosed;
                return RedirectToAction("Index");
            }
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            
            return View(entity);
        }

        [HttpPost]
        public ActionResult Unconfirm(TaskService taskService)
        {
            try
            {
                _taskServiceApp.Update(taskService);
                TempData["Success"] = SuccessMessage.TaskUnconfirmed;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Erro: " + ex.Message;
                return RedirectToAction("Start", taskService.Code);
            }

        }

        public override ActionResult Edit(int id = 0)
        {
            var entity = _entityApp.GetById(id);
            if (CheckIfExists(entity)) return RedirectToAction("Index");
            ViewBag.CodeTask = new SelectList(_taskApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeTask);
            ViewBag.CodeConstruction = new SelectList(_constructionApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeConstruction);
            ViewBag.CodeBuildingArea = new SelectList(_areaApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeBuildingArea);
            ViewBag.CodeUnit = new SelectList(_unitApp.GetAll().ToList(), GeneralField.Code, GeneralField.Name, entity.CodeUnit);
            return View(entity);
        }

    }
}