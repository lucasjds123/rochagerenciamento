﻿
namespace RochaGerenciamento.MVC.App_Start
{
    using System;
    using System.Web;
    using Microsoft.Practices.Unity;
    using RochaGerenciamento.Domain.Entities;
    using System.Web.Mvc;
    using Unity.Mvc4;
    using RochaGerenciamento.Application.Interfaces;
    using RochaGerenciamento.Application;
    using RochaGerenciamento.Domain.Interfaces.Services;
    using RochaGerenciamento.Domain.Services;
    using RochaGerenciamento.Domain.Interfaces.Repositories;
    using RochaGerenciamento.Infra.Repositories;
    using RochaGerenciamento.Domain.Interfaces.UnitOfWork;
    using RochaGerenciamento.Infra.UnitOfWork;

    public static class Bootstraper
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            container.RegisterType(typeof(IAppServiceBase<>), typeof(AppServiceBase<>));
            container.RegisterType(typeof(IUserAppService), typeof(UserAppService));
            container.RegisterType(typeof(IProfileAppService), typeof(ProfileAppService));
            container.RegisterType(typeof(IConstructionAppService), typeof(ConstructionAppService));
            container.RegisterType(typeof(IPlanAppService), typeof(PlanAppService));
            container.RegisterType(typeof(IUnitTypeAppService), typeof(UnitTypeAppService));
            container.RegisterType(typeof(IProviderAppService), typeof(ProviderAppService));
            container.RegisterType(typeof(IProductAppService), typeof(ProductAppService));
            container.RegisterType(typeof(IRequestAppService), typeof(RequestAppService));
            container.RegisterType(typeof(ISubServiceAppService), typeof(SubServiceAppService));
            container.RegisterType(typeof(IProjectAppService), typeof(ProjectAppService));
            container.RegisterType(typeof(ISubServicePlanAppService), typeof(SubServicePlanAppService));
            container.RegisterType(typeof(IOrderAppService), typeof(OrderAppService));
            container.RegisterType(typeof(IManageOrderAppService), typeof(ManageOrderAppService));
            container.RegisterType(typeof(IManageBillAppService), typeof(ManageBillAppService));
            container.RegisterType(typeof(IBillAppService), typeof(BillAppService));
            container.RegisterType(typeof(IInstallmentAppService), typeof(InstallmentAppService));
            container.RegisterType(typeof(IOrderProductAppService), typeof(OrderProductAppService));
            container.RegisterType(typeof(IProductConstructionAppService), typeof(ProductConstructionAppService));
            container.RegisterType(typeof(IExpenditureAppService), typeof(ExpenditureAppService));
            container.RegisterType(typeof(IUnitAppService), typeof(UnitAppService));
            container.RegisterType(typeof(IClientAppService), typeof(ClientAppService));
            container.RegisterType(typeof(IContractAppService), typeof(ContractAppService));
            container.RegisterType(typeof(IContractIncomingAppService), typeof(ContractIncomingAppService));
            container.RegisterType(typeof(IReportAppService), typeof(ReportAppService));
            container.RegisterType(typeof(IRequestProductAppService), typeof(RequestProductAppService));
            container.RegisterType(typeof(IEmployeeAppService), typeof(EmployeeAppService));
            container.RegisterType(typeof(IOwnerAppService), typeof(OwnerAppService));
            container.RegisterType(typeof(IAddressAppService), typeof(AddressAppService));
            container.RegisterType(typeof(IManufacturerAppService), typeof(ManufacturerAppService));
            container.RegisterType(typeof(IBuildingAreaAppService), typeof(BuildingAreaAppService));
            container.RegisterType(typeof(ITaskServiceAppService), typeof(TaskServiceAppService));
            container.RegisterType(typeof(IManageTaskServiceAppService), typeof(ManageTaskServiceAppService));

            container.RegisterType(typeof(IServiceBase<>), typeof(ServiceBase<>));
            container.RegisterType(typeof(IUserService), typeof(UserService));
            container.RegisterType(typeof(IProfileService), typeof(ProfileService));
            container.RegisterType(typeof(IConstructionService), typeof(ConstructionService));
            container.RegisterType(typeof(IPlanService), typeof(PlanService));
            container.RegisterType(typeof(IUnitTypeService), typeof(UnitTypeService));
            container.RegisterType(typeof(IProviderService), typeof(ProviderService));
            container.RegisterType(typeof(IProductService), typeof(ProductService));
            container.RegisterType(typeof(IRequestService), typeof(RequestService));
            container.RegisterType(typeof(ISubServiceService), typeof(SubServiceService));
            container.RegisterType(typeof(IProjectService), typeof(ProjectService));
            container.RegisterType(typeof(ISubServicePlanService), typeof(SubServicePlanService));
            container.RegisterType(typeof(IOrderService), typeof(OrderService));
            container.RegisterType(typeof(IManageOrderService), typeof(ManageOrderService));
            container.RegisterType(typeof(IManageBillService), typeof(ManageBillService));
            container.RegisterType(typeof(IBillService), typeof(BillService));
            container.RegisterType(typeof(IInstallmentService), typeof(InstallmentService));
            container.RegisterType(typeof(IOrderProductService), typeof(OrderProductService));
            container.RegisterType(typeof(IProductConstructionService), typeof(ProductConstructionService));
            container.RegisterType(typeof(IExpenditureService), typeof(ExpenditureService));
            container.RegisterType(typeof(IUnitService), typeof(UnitService));
            container.RegisterType(typeof(IClientService), typeof(ClientService));
            container.RegisterType(typeof(IContractService), typeof(ContractService));
            container.RegisterType(typeof(IContractIncomingService), typeof(ContractIncomingService));
            container.RegisterType(typeof(IReportService), typeof(ReportService));
            container.RegisterType(typeof(IRequestProductService), typeof(RequestProductService));
            container.RegisterType(typeof(IEmployeeService), typeof(EmployeeService));
            container.RegisterType(typeof(IOwnerService), typeof(OwnerService));
            container.RegisterType(typeof(IAddressService), typeof(AddressService));
            container.RegisterType(typeof(IManufacturerService), typeof(ManufacturerService));
            container.RegisterType(typeof(IBuildingAreaService), typeof(BuildingAreaService));
            container.RegisterType(typeof(ITaskServiceService), typeof(TaskServiceService));
            container.RegisterType(typeof(IManageTaskServiceService), typeof(ManageTaskServiceService));

            container.RegisterType(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));
            container.RegisterType(typeof(IUserRepository), typeof(UserRepository));
            container.RegisterType(typeof(IProfileRepository), typeof(ProfileRepository));
            container.RegisterType(typeof(IConstructionRepository), typeof(ConstructionRepository));
            container.RegisterType(typeof(IPlanRepository), typeof(PlanRepository));
            container.RegisterType(typeof(IUnitTypeRepository), typeof(UnitTypeRepository));
            container.RegisterType(typeof(IProviderRepository), typeof(ProviderRepository));
            container.RegisterType(typeof(IProductRepository), typeof(ProductRepository));
            container.RegisterType(typeof(IRequestRepository), typeof(RequestRepository));
            container.RegisterType(typeof(ISubServiceRepository), typeof(SubServiceRepository));
            container.RegisterType(typeof(IProjectRepository), typeof(ProjectRepository));
            container.RegisterType(typeof(ISubServicePlanRepository), typeof(SubServicePlanRepository));
            container.RegisterType(typeof(IOrderRepository), typeof(OrderRepository));
            container.RegisterType(typeof(IBillRepository), typeof(BillRepository));
            container.RegisterType(typeof(IInstallmentRepository), typeof(InstallmentRepository));
            container.RegisterType(typeof(IOrderProductRepository), typeof(OrderProductRepository));
            container.RegisterType(typeof(IProductConstructionRepository), typeof(ProductConstructionRepository));
            container.RegisterType(typeof(IExpenditureRepository), typeof(ExpenditureRepository));
            container.RegisterType(typeof(IUnitRepository), typeof(UnitRepository));
            container.RegisterType(typeof(IClientRepository), typeof(ClientRepository));
            container.RegisterType(typeof(IContractRepository), typeof(ContractRepository));
            container.RegisterType(typeof(IContractIncomingRepository), typeof(ContractIncomingRepository));
            container.RegisterType(typeof(IReportRepository), typeof(ReportRepository));
            container.RegisterType(typeof(IRequestProductRepository), typeof(RequestProductRepository));
            container.RegisterType(typeof(IEmployeeRepository), typeof(EmployeeRepository));
            container.RegisterType(typeof(IOwnerRepository), typeof(OwnerRepository));
            container.RegisterType(typeof(IAddressRepository), typeof(AddressRepository));
            container.RegisterType(typeof(IManufacturerRepository), typeof(ManufacturerRepository));
            container.RegisterType(typeof(IBuildingAreaRepository), typeof(BuildingAreaRepository));
            container.RegisterType(typeof(ITaskServiceRepository), typeof(TaskServiceRepository));

            container.RegisterType(typeof(IUnitOfWorkBase), typeof(UnitOfWorkBase));
            container.RegisterType(typeof(IManageOrderUnitOfWork), typeof(ManageOrderUnitOfWork));
            container.RegisterType(typeof(IManageBillUnitOfWork), typeof(ManageBillUnitOfWork));
            container.RegisterType(typeof(IManageTaskServiceUnitOfWork), typeof(ManageTaskServiceUnitOfWork));

            return container;
        }
    }
}