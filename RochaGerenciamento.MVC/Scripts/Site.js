﻿$(function () {
    $("#block_address").hide();
    $(".ad_inputs").attr("disabled", true);

    $("#ad_button").click(function () {
        $("#block_address").show();
        $(".block_address_select").hide()
        $("#CodeAddress").attr("disabled", true);
        $(".ad_inputs").attr("disabled", false);
    });

    $(".product_text_box").hide();
    $(".product_text_box input").attr("disabled", true);
    $("#change_combobox").click(function () {
        $(".product_text_box").show();
        $(".product_text_box input").attr("disabled", false);
        $(".product_combobox").hide();
        $("#CodeProduct").attr("disabled", true);
        
    })

    $(".calculate_area").blur(function () {
        if ($("#CommonSize").val() == "" ){
            $("#CommonSize").val("0");

        }
        if($("#PrivaceSize").val() == ""){
            $("#PrivaceSize").val("0");
        }
        if ($("#OtherAreasSize").val() == "") {
            $("#OtherAreasSize").val("0");
        }
        var total = parseFloat($("#CommonSize").val().replace(",", ".")) + parseFloat($("#PrivaceSize").val().replace(",", ".")) + parseFloat($("#OtherAreasSize").val().replace(",", "."));
        $("#TotalSize").val(total.toFixed(3).replace(".", ","));
        
    });


    $('form').submit(function () {
        $(this).find("button[type='submit']").prop('disabled', true);
    });

    $("#Discount").blur(function () {
        orderFormulaTotal();
    });

    $("#ShippingCost").blur(function () {
        orderFormulaTotal();
    });

    $("#LaborCost").blur(function () {
        orderFormulaTotal();
    });

    function orderFormulaTotal() {
        
        var result = parseFloat($("#TotalValue").val().replace(",", ".")) - parseFloat($("#Discount").val().replace(",", "."));
        result = result + parseFloat($("#LaborCost").val().replace(",", ".")) + parseFloat($("#ShippingCost").val().replace(",", ".")) ;
        $("#TotalValueSale").val(parseFloat(result).toFixed(2).replace(".", ","));

        //$("#TotalValueSale").val(((parseFloat($("#TotalValue").val().replace(",", ".")) + parseFloat($("#LaborCost").val().replace(",", ".")) + parseFloat($("#ShippingCost").val().replace(",", ".")) ) - parseFloat($("#Discount").val().replace(",", "."))).toFixed(2));
    }

     //$('#datetimepicker2,#datetimepicker3,#datetimepicker4').datetimepicker();
    $('#datetimepicker2,#datetimepicker3,#datetimepicker4').datetimepicker({
        format: 'DD/MM/YYYY HH:mm'
    });

     $('#only-date,#only-date-atendimento').datetimepicker({
        //format: 'MM/DD/YYYY'
        format: 'DD/MM/YYYY'
    });

    $(".alert-success,.alert-danger").fadeTo(4000, 500).slideUp(4000, function () {
    });

    jQuery("#Phone").mask("(99) 9999-9999?9");
    jQuery("#NumberOrder").mask("999/99");

    $("#LaborCost,#ShippingCost,#Discount,#TotalValueSale,#Bill_TotalValue,#Income,#Value,#TotalValue,#FrequencyInConstruction,#FrequencyInService,#EstimationValue,#RealValue,#ValueUnit,#Value").maskMoney({ thousands: '', decimal: ',', allowZero: true });
    $(".ValorTotalNoZero").maskMoney({ thousands: '', decimal: ',', allowZero: false });
    $("#TotalSize,#OtherAreasSize,#PrivaceSize,#CommonSize").maskMoney({ thousands: '', decimal: ',', precision: 3 });
    $(".amount_decimal_point2").maskMoney({ thousands: '', decimal: ',', precision: 1, allowZero: true });
    $(".amount_decimal_point").maskMoney({ thousands: '', decimal: ',', precision: 1 });

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return !re.test(email);
    }

    $("input.email").change(function () {
        if (validateEmail($(this).val())) {
            alert("Email Inválido");
            $(this).val('').focus();
        }
    });

    $('.selectpicker').attr("data-live-search", "true");
    $('.selectpicker').attr("data-width", "100%");


    $('.selectpicker').selectpicker({
        dropupAuto: false,
        size: 100
    });


    $("#tabs").tabs();


    $("#exporta-csv").click(function () {

        $('#grid-data,#grid-data1,#grid-data2,#grid-data3').tableExport({ type: 'csv' });
    });

    $("#exporta-xls").click(function () {
        $('#grid-data,#grid-data1,#grid-data2,#grid-data3').tableExport({ type: 'excel' });
    });

    $("#exporta-pdf").click(function () {
        $('#grid-data,#grid-data1,#grid-data2,#grid-data3,#receipt').tableExport({
            type: 'pdf',
            jspdf: { orientation: 'p',
                format: 'a4',
                margins: { left: 10, right: 10, top: 20, bottom: 20 },
                autotable: { styles: { fillColor: 'inherit',
                    textColor: 'inherit'
                },
                    tableWidth: 'auto'
                }
            }
        });
    });

    var grid = $("#grid-data").bootgrid({
        ajax: true,
        post: function () {
            return {
                id: "b0df282a-0d67-40e5-8558-c9e93b7befed"
            };
        },
        formatters: {
            "commands": function (column, row) {
                email = "<button  type=\"button\" title=\"Enviar Email\" class=\"btn btn-xs btn-default command-email\" data-row-id=\"" + row.Code + "\"><span class=\"glyphicon glyphicon-envelope\"></span></button>";
                detalhe = "<button  type=\"button\" title=\"Detalhes\" class=\"btn btn-xs btn-default command-detalhe\" data-row-id=\"" + row.Code + "\"><span class=\"glyphicon glyphicon-th-list\"></span></button>";
                edicao = "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\"" + row.Code + "\"><span class=\"glyphicon glyphicon-edit\"></span></button> ";
                deletar = "<button  type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\"" + row.Code + "\"><span class=\"glyphicon glyphicon-trash\"></span></button>";
                documento = "<button  type=\"button\" class=\"btn btn-xs btn-default command-open-document\" data-row-id=\"" + row.Code + "\"><span class=\"glyphicon glyphicon-download\"></span></button>";
              
                if ($("#Controller").val() == "Request")
                    return email + detalhe + deletar;
                if ( $("#Controller").val() == "ContractIncoming" )
                    return "";
                if ($("#Controller").val() == "Construction" || $("#Controller").val() == "Client" || $("#Controller").val() == "Contract" || $("#Controller").val() == "ContractIncoming")
                    return detalhe + edicao + deletar;
                if ($("#Controller").val() == "Order")
                    return detalhe + deletar;
                if ($("#Controller").val() == "Product" && row.Specs != null)
                    return documento + edicao + deletar
                return edicao + deletar;
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        var url = $("#Controller").val();
        grid.find(".command-edit").on("click", function (e) {
            location.href = url + "/Edit/" + $(this).data("row-id");
        }).end().find(".command-delete").on("click", function (e) {
            if (confirm('Deseja excluir este registro?'))
                location.href = url + "/Delete/" + $(this).data("row-id");
        }).end().find(".command-detalhe").unbind().on("click", function (e) {
            location.href = url + "/Details/" + $(this).data("row-id");
        }).end().find(".command-email").unbind().on("click", function (e) {
            location.href = url + "/SendEmail/" + $(this).data("row-id");
        }).end().find(".command-open-document").unbind().on("click", function (e) {
            window.open("../../Product/OpenDocument/" + $(this).data("row-id"), "_blank");
        });
    });


    $.fn.visible = function (partial, hidden, direction, container) {

        if (this.length < 1)
            return;

        var $t = this.length > 1 ? this.eq(0) : this,
                        isContained = typeof container !== 'undefined' && container !== null,
                        $w = isContained ? $(container) : $(window),
                        wPosition = isContained ? $w.position() : 0,
            t = $t.get(0),
            vpWidth = $w.outerWidth(),
            vpHeight = $w.outerHeight(),
            direction = (direction) ? direction : 'both',
            clientSize = hidden === true ? t.offsetWidth * t.offsetHeight : true;

        if (typeof t.getBoundingClientRect === 'function') {

            // Use this native browser method, if available.
            var rec = t.getBoundingClientRect(),
                tViz = isContained ?
                                                rec.top - wPosition.top >= 0 && rec.top < vpHeight + wPosition.top :
                                                rec.top >= 0 && rec.top < vpHeight,
                bViz = isContained ?
                                                rec.bottom - wPosition.top > 0 && rec.bottom <= vpHeight + wPosition.top :
                                                rec.bottom > 0 && rec.bottom <= vpHeight,
                lViz = isContained ?
                                                rec.left - wPosition.left >= 0 && rec.left < vpWidth + wPosition.left :
                                                rec.left >= 0 && rec.left < vpWidth,
                rViz = isContained ?
                                                rec.right - wPosition.left > 0 && rec.right < vpWidth + wPosition.left :
                                                rec.right > 0 && rec.right <= vpWidth,
                vVisible = partial ? tViz || bViz : tViz && bViz,
                hVisible = partial ? lViz || rViz : lViz && rViz;

            if (direction === 'both')
                return clientSize && vVisible && hVisible;
            else if (direction === 'vertical')
                return clientSize && vVisible;
            else if (direction === 'horizontal')
                return clientSize && hVisible;
        } else {

            var viewTop = isContained ? 0 : wPosition,
                viewBottom = viewTop + vpHeight,
                viewLeft = $w.scrollLeft(),
                viewRight = viewLeft + vpWidth,
                position = $t.position(),
                _top = position.top,
                _bottom = _top + $t.height(),
                _left = position.left,
                _right = _left + $t.width(),
                compareTop = partial === true ? _bottom : _top,
                compareBottom = partial === true ? _top : _bottom,
                compareLeft = partial === true ? _right : _left,
                compareRight = partial === true ? _left : _right;

            if (direction === 'both')
                return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop)) && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
            else if (direction === 'vertical')
                return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop));
            else if (direction === 'horizontal')
                return !!clientSize && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
        }
    };

    //steps
    var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn'),
                allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);
        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allPrevBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
        curStepBtn = curStep.attr("id"),
        prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        prevStepWizard.removeAttr('disabled').trigger('click');
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url'],select,input[type='number']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
})



