-- --------------------------------------------------------
-- Host:                         rocha-gerenciam.mysql.uhserver.com
-- Server version:               5.6.26-log - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for rocha_gerenciam
CREATE DATABASE IF NOT EXISTS `rocha_gerenciam` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rocha_gerenciam`;


-- Dumping structure for table rocha_gerenciam.bill
CREATE TABLE IF NOT EXISTS `bill` (
  `cod_bill` int(11) NOT NULL AUTO_INCREMENT,
  `total_value` double NOT NULL,
  `checked` char(1) DEFAULT '0',
  `barcode` varchar(100) DEFAULT NULL,
  `bank` varchar(45) DEFAULT NULL,
  `emission_date` date NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_bill`),
  KEY `fk_alteration_bill` (`cod_alteration_user`),
  KEY `fk_creation_id_bill` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_bill` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_id_bill` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.civil_status
CREATE TABLE IF NOT EXISTS `civil_status` (
  `cod_civil_status` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_civil_status`),
  KEY `fk_alteration_cs` (`cod_alteration_user`),
  KEY `fk_creation_cs` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_cs` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_cs` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.client
CREATE TABLE IF NOT EXISTS `client` (
  `cod_client` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `cpf` varchar(45) DEFAULT NULL,
  `rg` varchar(45) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `observation` varchar(500) DEFAULT NULL,
  `income` double DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `attendance_date` date DEFAULT NULL,
  `cod_contact_origin` int(11) DEFAULT NULL,
  `cod_civil_status` int(11) DEFAULT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_client`),
  KEY `fk_contac_client_idx` (`cod_contact_origin`),
  KEY `fk_civil_idx` (`cod_civil_status`),
  KEY `fk_alteration_client` (`cod_alteration_user`),
  KEY `fk_creation_client` (`cod_creation_user`),
  CONSTRAINT `FK_client_civil_status` FOREIGN KEY (`cod_civil_status`) REFERENCES `civil_status` (`cod_civil_status`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_client_contact_origin` FOREIGN KEY (`cod_contact_origin`) REFERENCES `contact_origin` (`cod_contact_origin`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_client` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_client` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.construction
CREATE TABLE IF NOT EXISTS `construction` (
  `cod_construction` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `number` varchar(45) NOT NULL,
  `borough` varchar(100) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_construction`),
  KEY `fk_alteration_id` (`cod_alteration_user`),
  KEY `fk_creation_id` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_id` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creation_id` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.contact_origin
CREATE TABLE IF NOT EXISTS `contact_origin` (
  `cod_contact_origin` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_contact_origin`),
  KEY `fk_alteration_co` (`cod_alteration_user`),
  KEY `fk_creation_co` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_co` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_co` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.contract
CREATE TABLE IF NOT EXISTS `contract` (
  `cod_contract` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `agreement_date` date NOT NULL,
  `witness_name` varchar(100) DEFAULT NULL,
  `witness_cpf` varchar(45) DEFAULT NULL,
  `cod_contract_type` int(11) NOT NULL,
  `cod_unit` int(11) NOT NULL,
  `cod_client` int(11) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_contract`),
  KEY `fk_consstr_client_idx` (`cod_client`),
  KEY `fk_unit_contrac_idx` (`cod_unit`),
  KEY `fk_contract_type_idx` (`cod_contract_type`),
  KEY `fk_alteration_contract` (`cod_alteration_user`),
  KEY `fk_creation_contract` (`cod_creation_user`),
  CONSTRAINT `FK_contract_client` FOREIGN KEY (`cod_client`) REFERENCES `client` (`cod_client`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_contract_contract_type` FOREIGN KEY (`cod_contract_type`) REFERENCES `contract_type` (`cod_contract_type`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_contract_unit` FOREIGN KEY (`cod_unit`) REFERENCES `unit` (`cod_unit`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_contract` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creation_contract` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.contract_incoming
CREATE TABLE IF NOT EXISTS `contract_incoming` (
  `cod_contract_incoming` int(11) NOT NULL AUTO_INCREMENT,
  `cod_incoming_type` int(11) NOT NULL,
  `cod_contract` int(11) NOT NULL,
  `cod_bill` int(11) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_contract_incoming`),
  KEY `fk_contract_idx` (`cod_contract`),
  KEY `fk_inc_type_idx` (`cod_incoming_type`),
  KEY `fk_bill_idx` (`cod_bill`),
  KEY `fk_alteration_contract_i` (`cod_alteration_user`),
  KEY `fk_creation_contract_i` (`cod_creation_user`),
  CONSTRAINT `FK_contract_incoming_contract` FOREIGN KEY (`cod_contract`) REFERENCES `contract` (`cod_contract`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_contract_incoming_contract_incoming` FOREIGN KEY (`cod_incoming_type`) REFERENCES `contract_incoming` (`cod_contract_incoming`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_contract_i` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_bill` FOREIGN KEY (`cod_bill`) REFERENCES `bill` (`cod_bill`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creation_contract_i` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.contract_type
CREATE TABLE IF NOT EXISTS `contract_type` (
  `cod_contract_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_contract_type`),
  KEY `fk_alteration_contract_type` (`cod_alteration_user`),
  KEY `fk_creation_contract_type` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_contract_type` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_contract_type` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.expenditure
CREATE TABLE IF NOT EXISTS `expenditure` (
  `cod_expenditure` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_date` date NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `observation` varchar(500) DEFAULT NULL,
  `value` double DEFAULT NULL,
  `cod_construction` int(11) NOT NULL,
  `cod_expenditure_type` int(11) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_expenditure`),
  KEY `FK_expenditure_order` (`cod_construction`),
  KEY `FK_expenditure_expenditure_type` (`cod_expenditure_type`),
  KEY `fk_alteration_expenditure` (`cod_alteration_user`),
  KEY `fk_creation_expenditure` (`cod_creation_user`),
  CONSTRAINT `FK_expenditure_expenditure_type` FOREIGN KEY (`cod_expenditure_type`) REFERENCES `expenditure_type` (`cod_expenditure_type`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_expenditure_order` FOREIGN KEY (`cod_construction`) REFERENCES `construction` (`cod_construction`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_expenditure` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_expenditure` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.expenditure_type
CREATE TABLE IF NOT EXISTS `expenditure_type` (
  `cod_expenditure_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_expenditure_type`),
  KEY `fk_alteration_expenditure_type` (`cod_alteration_user`),
  KEY `fk_creation_id_expenditure_type` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_expenditure_type` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_id_expenditure_type` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.incoming_type
CREATE TABLE IF NOT EXISTS `incoming_type` (
  `cod_incoming_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_incoming_type`),
  KEY `fk_alteration_i_type` (`cod_alteration_user`),
  KEY `fk_creation_i_type` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_i_type` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_i_type` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.installment
CREATE TABLE IF NOT EXISTS `installment` (
  `cod_installment` int(11) NOT NULL AUTO_INCREMENT,
  `value` double NOT NULL,
  `duedate` date NOT NULL,
  `payment_date` date DEFAULT NULL,
  `checked` char(1) DEFAULT NULL,
  `sequence` int(11) NOT NULL,
  `cod_bill` int(11) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_installment`),
  KEY `fk_bill_installment_idx` (`cod_bill`),
  KEY `fk_alteration_inst` (`cod_alteration_user`),
  KEY `fk_creation_id_inst` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_inst` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_bill_installment` FOREIGN KEY (`cod_bill`) REFERENCES `bill` (`cod_bill`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creation_id_inst` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for procedure rocha_gerenciam.list_product_construction
DELIMITER //
CREATE DEFINER=`rocha_emp`@`%` PROCEDURE `list_product_construction`(IN `codeSituation` INT, IN `searchPhrase` VARCHAR(100), IN `codeConstruction` INT)
BEGIN
SET SESSION group_concat_max_len = 1000000;
SET lc_time_names = 'pt_BR';
SET collation_connection = 'utf8_general_ci';
set searchPhrase = concat("%",lower(searchPhrase) , "%") ;

select p.name as 'Product', pr.name as 'Provider', min(pc.availability_date) as 'AvailabilityDate', 
pc.cod_product_construction as Code , s.name as 'Situation', count(*) as 'Amount',
c.name as 'Construction', p.`limit` as 'Limit', if(p.recurrent='1','Sim','Não') as 'Recurrent' , p.amount_per_unit as 'AmountPerUnit' , 
ut.name as 'UnitType', if(min(pc.availability_date) > now(),'1','0') as 'CheckDate' , pc.cod_product as 'CodProduct', pc.cod_construction as 'CodConstruction',
pc.alteration_date as 'AlterationDate'
from product_construction pc
inner join product p on p.cod_product = pc.cod_product
inner join provider pr on p.cod_provider = pr.cod_provider
inner join situation s on s.cod_situation = pc.cod_situation
left join construction c on c.cod_construction = pc.cod_construction
inner join unit_type ut on ut.cod_unit_type = p.cod_unit_type
where pc.idc_active='1' and pc.cod_situation = codeSituation and if(codeConstruction > 0 and codeSituation < 3, c.cod_construction = codeConstruction, true)

and( searchPhrase = "" or (
		   (lower(c.name) like  lower(searchPhrase) ) or 
			lower(p.name   )  like lower(searchPhrase)  or 
			lower(pr.name   )  like lower(searchPhrase)  or 
			(lower(ut.name) like  lower(searchPhrase)  )  
		/*	((lower(concat( if(weekday(pc.availability_date) <5 ,concat(dayname(pc.availability_date),'-feira'),dayname(pc.availability_date)) ,', ' ,DATE_FORMAT(pc.availability_date, '%d de %M de %Y')))) 
					like  lower(searchPhrase)   )*/
			 ))
			 
group by pc.cod_product, pc.cod_construction, if(codeSituation = '2', date(pc.alteration_date),true)
order by pc.availability_date
COLLATE latin1_general_ci;
END//
DELIMITER ;


-- Dumping structure for table rocha_gerenciam.localization
CREATE TABLE IF NOT EXISTS `localization` (
  `cod_localization` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_localization`),
  KEY `fk_alteration_loc` (`cod_alteration_user`),
  KEY `fk_creation_loc` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_loc` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_loc` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.material_type
CREATE TABLE IF NOT EXISTS `material_type` (
  `cod_material_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_material_type`),
  KEY `fk_alteration_material_type` (`cod_alteration_user`),
  KEY `fk_creation_id_material_type` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_material_type` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_id_material_type` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.order
CREATE TABLE IF NOT EXISTS `order` (
  `cod_order` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `number_order` int(11) NOT NULL,
  `total_value` double NOT NULL,
  `discount` double DEFAULT '0',
  `total_value_sale` double NOT NULL,
  `approval` char(1) DEFAULT '0',
  `estimation_date` datetime DEFAULT NULL,
  `cod_construction` int(11) NOT NULL,
  `cod_bill` int(11) DEFAULT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_order`),
  KEY `FK_order_construction` (`cod_construction`),
  KEY `FK_order_bill` (`cod_bill`),
  KEY `fk_alteration_order` (`cod_alteration_user`),
  KEY `fk_creation_id_order` (`cod_creation_user`),
  CONSTRAINT `FK_order_bill` FOREIGN KEY (`cod_bill`) REFERENCES `bill` (`cod_bill`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_order_construction` FOREIGN KEY (`cod_construction`) REFERENCES `construction` (`cod_construction`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_order` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_id_order` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for procedure rocha_gerenciam.order_approval
DELIMITER //
CREATE DEFINER=`rocha_emp`@`%` PROCEDURE `order_approval`(IN `codOrder` INT, IN `codConstruction` INT, IN `estimationDate` DATETIME, IN `codUser` INT)
BEGIN

DECLARE done INT DEFAULT FALSE;
DECLARE codProduct INT;
DECLARE amnt INT;
DECLARE counter INT DEFAULT 0;
DECLARE cur1 CURSOR FOR select cod_product,amount from order_product  op
where  op.cod_order = codOrder;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
SET lc_time_names = 'pt_BR';
SET collation_connection = 'utf8_general_ci';
OPEN cur1;

read_loop: LOOP
 FETCH cur1 INTO codProduct,amnt;
 
 IF done THEN
   LEAVE read_loop;
 END IF;
 
 select codProduct, amnt;
 loop2: LOOP
 	if counter = amnt then
 		LEAVE loop2;
 	end if;
 	set @sql = CONCAT('insert into product_construction(creation_date,cod_creation_user, cod_construction, cod_product, cod_situation, availability_date) VALUES (now(),',codUser,',',codConstruction,',',codProduct,',1,"',estimationDate,'");');
 	prepare stmt from @sql;
	execute stmt;
 	set counter = counter + 1;
 END LOOP;
	set counter = 0;
END LOOP;

CLOSE cur1;

set @sql = CONCAT('update `order` set cod_alteration_user = ',codUser, ' , approval = ''1'' , estimation_date = ''', estimationDate , ''' where cod_order =' , codOrder);
prepare stmt from @sql;
execute stmt;

END//
DELIMITER ;


-- Dumping structure for table rocha_gerenciam.order_product
CREATE TABLE IF NOT EXISTS `order_product` (
  `cod_order_product` int(11) NOT NULL AUTO_INCREMENT,
  `cod_product` int(11) NOT NULL,
  `cod_order` int(11) NOT NULL,
  `value_unit` double NOT NULL,
  `amount` double NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_order_product`),
  KEY `FK_order_product_product` (`cod_product`),
  KEY `FK_order_product_order` (`cod_order`),
  KEY `fk_alteration_order_p` (`cod_alteration_user`),
  KEY `fk_creation_id_order_p` (`cod_creation_user`),
  CONSTRAINT `FK_order_product_order` FOREIGN KEY (`cod_order`) REFERENCES `order` (`cod_order`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_order_product_product` FOREIGN KEY (`cod_product`) REFERENCES `product` (`cod_product`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_order_p` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_id_order_p` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.plan
CREATE TABLE IF NOT EXISTS `plan` (
  `cod_plan` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `deadline` datetime NOT NULL,
  `conclusion_date` datetime DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `cod_construction` int(11) NOT NULL,
  `estimation_value` double NOT NULL,
  `real_value` double DEFAULT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_plan`),
  KEY `FK_plan_construction` (`cod_construction`),
  KEY `fk_alteration_id_plan` (`cod_alteration_user`),
  KEY `fk_creation_id_plan` (`cod_creation_user`),
  CONSTRAINT `FK_plan_construction` FOREIGN KEY (`cod_construction`) REFERENCES `construction` (`cod_construction`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_id_plan` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creation_id_plan` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.product
CREATE TABLE IF NOT EXISTS `product` (
  `cod_product` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `recurrent` char(1) DEFAULT '0',
  `limit` double NOT NULL,
  `amount_per_unit` double NOT NULL,
  `value_unit` double NOT NULL,
  `specs` varchar(255) DEFAULT '',
  `cod_unit_type` int(11) NOT NULL,
  `cod_provider` int(11) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_product`),
  KEY `FK_product_unit_type` (`cod_unit_type`),
  KEY `FK_product_provider` (`cod_provider`),
  KEY `fk_alteration_product` (`cod_alteration_user`),
  KEY `fk_creation_id_product` (`cod_creation_user`),
  CONSTRAINT `FK_product_provider` FOREIGN KEY (`cod_provider`) REFERENCES `provider` (`cod_provider`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_product_unit_type` FOREIGN KEY (`cod_unit_type`) REFERENCES `unit_type` (`cod_unit_type`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_product` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_id_product` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.product_construction
CREATE TABLE IF NOT EXISTS `product_construction` (
  `cod_product_construction` int(11) NOT NULL AUTO_INCREMENT,
  `cod_product` int(11) NOT NULL,
  `cod_construction` int(11) DEFAULT NULL,
  `cod_situation` int(11) NOT NULL,
  `availability_date` date NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_product_construction`),
  KEY `FK__product` (`cod_product`),
  KEY `FK__construction` (`cod_construction`),
  KEY `FK__situation` (`cod_situation`),
  KEY `fk_alteration_order_pc` (`cod_alteration_user`),
  KEY `fk_creation_id_order_pc` (`cod_creation_user`),
  CONSTRAINT `FK__construction` FOREIGN KEY (`cod_construction`) REFERENCES `construction` (`cod_construction`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK__product` FOREIGN KEY (`cod_product`) REFERENCES `product` (`cod_product`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK__situation` FOREIGN KEY (`cod_situation`) REFERENCES `situation` (`cod_situation`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_order_pc` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creation_id_order_pc` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.profile
CREATE TABLE IF NOT EXISTS `profile` (
  `cod_profile` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  `idc_active` char(1) DEFAULT '1',
  PRIMARY KEY (`cod_profile`),
  KEY `FK_profile_user` (`cod_alteration_user`),
  KEY `FK_profile_user_2` (`cod_creation_user`),
  CONSTRAINT `FK_profile_user` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_profile_user_2` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.project
CREATE TABLE IF NOT EXISTS `project` (
  `cod_project` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  `cod_sub_service` int(11) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_project`),
  KEY `fk_alteration_project` (`cod_alteration_user`),
  KEY `fk_creation_project` (`cod_creation_user`),
  KEY `FK_project_sub_service` (`cod_sub_service`),
  CONSTRAINT `FK_project_sub_service` FOREIGN KEY (`cod_sub_service`) REFERENCES `sub_service` (`cod_sub_service`),
  CONSTRAINT `fk_alteration_project` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_project` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.provider
CREATE TABLE IF NOT EXISTS `provider` (
  `cod_provider` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `psq` char(1) NOT NULL DEFAULT '0',
  `cod_material_type` int(11) NOT NULL,
  `contact` varchar(150) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_provider`),
  KEY `FK_provider_material_type` (`cod_material_type`),
  KEY `fk_alteration_provider` (`cod_alteration_user`),
  KEY `fk_creation_id_provider` (`cod_creation_user`),
  CONSTRAINT `FK_provider_material_type` FOREIGN KEY (`cod_material_type`) REFERENCES `material_type` (`cod_material_type`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_provider` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_id_provider` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for procedure rocha_gerenciam.report_expenditure_vs_incoming
DELIMITER //
CREATE DEFINER=`rocha_emp`@`%` PROCEDURE `report_expenditure_vs_incoming`(IN `firstDate` VARCHAR(50), IN `lastDate` VARCHAR(50), IN `codeConstruction` VARCHAR(50))
BEGIN

select round(sum(total),2) as 'Expenditure', round(incoming,2) as 'Incoming', round(incoming,2) - round(sum(total),2) Result  from (
select if(sum(value) is null , 0 , sum(value))as 'total' from expenditure e
where e.idc_active = '1' and (if(lastDate != "" ,e.purchase_date<= lastDate, true)) and (if(firstDate != "" ,e.purchase_date >= firstDate, true)) 
and (if(codeConstruction != "0" ,e.cod_construction = codeConstruction, true)) 
union
select if( sum(o.total_value) is null, 0,sum(o.total_value)) as 'total'  from `order` o
where o.idc_active = '1' and o.approval = '1' and (if(lastDate != "" ,o.creation_date<= lastDate, true)) and (if(firstDate != "" ,o.creation_date >= firstDate, true)) and
if(codeConstruction != "0" ,o.cod_construction = codeConstruction, true) ) aux , 
( select if(sum(b.total_value) is null, 0,sum(b.total_value))  as 'incoming' from contract_incoming ci 
inner join bill b on b.cod_bill = ci.cod_bill
inner join contract c on c.cod_contract = ci.cod_contract
inner join unit u on u.cod_unit = c.cod_unit
inner join construction co on co.cod_construction = u.cod_construction
where b.idc_active='1' and (if(lastDate != "" ,ci.creation_date<= lastDate, true)) and (if(firstDate != "" ,ci.creation_date >= firstDate, true)) 
and (if(codeConstruction != "0" ,co.cod_construction = codeConstruction, true))  ) aux2
;
END//
DELIMITER ;


-- Dumping structure for procedure rocha_gerenciam.report_filter_order
DELIMITER //
CREATE DEFINER=`rocha_emp`@`%` PROCEDURE `report_filter_order`(IN `codOrder` INT)
BEGIN
select if(pr.contact is null, "", pr.contact) as 'Contact' , pr.phone as 'Phone', o.total_value_sale as 'TotalValueSale' , o.discount as 'Discount' , o.estimation_date as 'Estimation',  o.number_order as 'NumOrder', ut.name as UnitType, p.name as 'Product' , op.amount as Amount, op.value_unit as ValueUnit, op.value_unit * op.amount as Total, pr.name as 'Provider' from `order` o
inner join order_product op on op.cod_order = o.cod_order
inner join product p on p.cod_product = op.cod_product
inner join unit_type ut on ut.cod_unit_type = p.cod_unit_type
inner join provider pr on p.cod_provider = pr.cod_provider
where o.cod_order = codOrder;
END//
DELIMITER ;


-- Dumping structure for procedure rocha_gerenciam.report_incoming
DELIMITER //
CREATE DEFINER=`rocha_emp`@`%` PROCEDURE `report_incoming`(IN `firstDate` VARCHAR(50), IN `lastDate` VARCHAR(50), IN `codeConstruction` VARCHAR(50))
BEGIN
select c.name 'Contract', ct.name 'ContractType',  c.agreement_date 'AgreementDate', round(sum(b.total_value),2) 'Total', co.name as 'Construction'
, u.name 'Unit', u.number 'Number', if(u.block is null, '', u.block) 'Block', l.name 'Localization' , cl.name 'Client' , cl.cpf 'Document'
from contract_incoming ci 
inner join bill b on b.cod_bill = ci.cod_bill
inner join contract c on c.cod_contract = ci.cod_contract
inner join unit u on u.cod_unit = c.cod_unit
inner join construction co on co.cod_construction = u.cod_construction
inner join localization l on l.cod_localization = u.cod_localization
inner join `client` cl on cl.cod_client = c.cod_client 
inner join contract_type ct on ct.cod_contract_type = c.cod_contract_type
where
ci.idc_active = '1'  and 
(if(firstDate != "" ,c.agreement_date >= firstDate, true)) and
(if(codeConstruction != "0" ,co.cod_construction = codeConstruction, true)) and
(if(lastDate != "" ,c.agreement_date<= lastDate, true))
group by c.cod_contract;
END//
DELIMITER ;


-- Dumping structure for procedure rocha_gerenciam.report_order
DELIMITER //
CREATE DEFINER=`rocha_emp`@`%` PROCEDURE `report_order`(IN `approval` VARCHAR(50), IN `firstDate` VARCHAR(50), IN `lastDate` VARCHAR(50), IN `codeConstruction` INT)
BEGIN
SET lc_time_names = 'pt_BR';
select op.cod_order_product, b.cod_bill,  o.name as 'Order', o.number_order as 'NumberOrder', if(o.approval='1', 'Sim', 'Não') as 'Approval', 
if(o.estimation_date is null ,'',o.estimation_date) as 'EstimationDate', c.name as 'Construction'
, count((op.cod_order_product )) as 'ProductAmount', o.total_value as 'TotalValue',count((i.cod_installment)) as 'InstallmentAmount',
o.discount as 'Discount', o.total_value_sale as 'TotalValueSale'
from `order` o
inner join order_product op on op.cod_order =o.cod_order
inner join construction c on c.cod_construction = o.cod_construction
left join bill b on b.cod_bill = o.cod_bill
left join installment i on i.cod_bill = b.cod_bill
where o.idc_active = '1' and op.idc_active= '1'  and 
(if(approval >=0 ,o.approval = approval, true)) and
(if(firstDate != "" ,o.estimation_date >= firstDate, true)) and
(if(codeConstruction != "" ,c.cod_construction = codeConstruction, true)) and
(if(lastDate != "" ,o.estimation_date<= lastDate, true))
group by o.cod_order 
order by o.approval;
END//
DELIMITER ;


-- Dumping structure for procedure rocha_gerenciam.report_stock
DELIMITER //
CREATE DEFINER=`rocha_emp`@`%` PROCEDURE `report_stock`(IN `codeSituation` INT, IN `codeConstruction` INT, IN `productSearch` VARCHAR(200), IN `providerSearch` VARCHAR(200), IN `unitTypeSearch` VARCHAR(200), IN `firstDate` VARCHAR(50), IN `lastDate` VARCHAR(50))
BEGIN
SET lc_time_names = 'pt_BR';
select p.name as 'Product', pr.name as 'Provider', min(pc.availability_date) as 'AvailabilityDate', 
pc.cod_product_construction as Code , s.name as 'Situation', count(*) as 'Amount',
c.name as 'Construction', p.`limit` as 'Limit', if(p.recurrent='1','Sim','Não') as 'Recurrent' , p.amount_per_unit as 'AmountPerUnit' , 
ut.name as 'UnitType', if(min(pc.availability_date) > now(),'1','0') as 'CheckDate' , GROUP_CONCAT(distinct(pc.cod_product_construction)) as 'GroupCodeProductConstruction',
pc.alteration_date as 'AlterationDate', sum(p.value_unit) as 'ValueUnit'
from product_construction pc
inner join product p on p.cod_product = pc.cod_product
inner join provider pr on p.cod_provider = pr.cod_provider
inner join situation s on s.cod_situation = pc.cod_situation
left join construction c on c.cod_construction = pc.cod_construction
inner join unit_type ut on ut.cod_unit_type = p.cod_unit_type
where pc.idc_active='1' and if(codeSituation>0, pc.cod_situation = codeSituation, true) and 
if(codeConstruction > 0 and codeSituation < 3, c.cod_construction = codeConstruction, true) and
(if(firstDate != "" ,pc.availability_date >= firstDate, true)) and
(if(lastDate != "" ,pc.availability_date <= lastDate, true))  and
if(productSearch > 0, p.cod_product = productSearch, true) and
if(providerSearch > 0, pr.cod_provider = providerSearch, true) and
if(unitTypeSearch > 0 , ut.cod_unit_type = unitTypeSearch, true)
group by pc.cod_product, pc.cod_construction,pc.cod_situation, if(codeSituation = '2', date(pc.alteration_date),true)
order by pc.availability_date;
END//
DELIMITER ;


-- Dumping structure for table rocha_gerenciam.request
CREATE TABLE IF NOT EXISTS `request` (
  `cod_request` int(11) NOT NULL AUTO_INCREMENT,
  `tittle` varchar(50) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `need_date` datetime NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_request`),
  KEY `fk_alteration_req` (`cod_alteration_user`),
  KEY `fk_creation_id_req` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_req` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_id_req` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.request_product
CREATE TABLE IF NOT EXISTS `request_product` (
  `cod_request_product` int(11) NOT NULL AUTO_INCREMENT,
  `cod_request` int(11) NOT NULL,
  `cod_product` int(11) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_request_product`),
  KEY `FK__request_pr` (`cod_request`),
  KEY `FK__product_re` (`cod_product`),
  KEY `fk_alteration_expenditure_rp` (`cod_alteration_user`),
  KEY `fk_creation_expenditure_rp` (`cod_creation_user`),
  CONSTRAINT `FK__product_re` FOREIGN KEY (`cod_product`) REFERENCES `product` (`cod_product`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK__request_pr` FOREIGN KEY (`cod_request`) REFERENCES `request` (`cod_request`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_expenditure_rp` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_expenditure_rp` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.service
CREATE TABLE IF NOT EXISTS `service` (
  `cod_service` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_service`),
  KEY `fk_alteration_service` (`cod_alteration_user`),
  KEY `fk_creation_id_service` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_service` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creation_id_service` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.situation
CREATE TABLE IF NOT EXISTS `situation` (
  `cod_situation` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_situation`),
  KEY `fk_alteration_ss` (`cod_alteration_user`),
  KEY `fk_creation_ss` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_ss` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_ss` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.status
CREATE TABLE IF NOT EXISTS `status` (
  `cod_status` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_status`),
  KEY `fk_alteration_status` (`cod_alteration_user`),
  KEY `fk_creation_status` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_status` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_status` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.sub_service
CREATE TABLE IF NOT EXISTS `sub_service` (
  `cod_sub_service` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `cod_service` int(11) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_sub_service`),
  KEY `FK_sub_service_service` (`cod_service`),
  KEY `fk_alteration_sub_service` (`cod_alteration_user`),
  KEY `fk_creation_id_sub_service` (`cod_creation_user`),
  CONSTRAINT `FK_sub_service_service` FOREIGN KEY (`cod_service`) REFERENCES `service` (`cod_service`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_sub_service` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creation_id_sub_service` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.sub_service_plan
CREATE TABLE IF NOT EXISTS `sub_service_plan` (
  `cod_sub_service_plan` int(11) NOT NULL AUTO_INCREMENT,
  `order` varchar(50) NOT NULL,
  `total_value` double NOT NULL,
  `measure_unit` varchar(100) NOT NULL,
  `amount_repetition` int(11) NOT NULL,
  `percent` double NOT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_stop` datetime DEFAULT NULL,
  `date_refresh` datetime DEFAULT NULL,
  `date_finish` datetime DEFAULT NULL,
  `estimation_in_days` int(11) NOT NULL,
  `cod_plan` int(11) NOT NULL,
  `cod_sub_service` int(11) NOT NULL,
  `cod_sub_service_plan_depen` int(11) DEFAULT NULL,
  `cod_status` int(11) DEFAULT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_sub_service_plan`),
  KEY `plan_idx` (`cod_plan`),
  KEY `fk_sub_service_idx` (`cod_sub_service`),
  KEY `fk_depen_idx` (`cod_sub_service_plan_depen`),
  KEY `fk_alteration_ssp` (`cod_alteration_user`),
  KEY `fk_creation_ssp` (`cod_creation_user`),
  KEY `FK_sub_service_plan_status` (`cod_status`),
  CONSTRAINT `FK_sub_service_plan_status` FOREIGN KEY (`cod_status`) REFERENCES `status` (`cod_status`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_sub_service_plan_sub_service_plan` FOREIGN KEY (`cod_sub_service_plan_depen`) REFERENCES `sub_service_plan` (`cod_sub_service_plan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_ssp` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creation_ssp` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sub_service` FOREIGN KEY (`cod_sub_service`) REFERENCES `sub_service` (`cod_sub_service`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `plan` FOREIGN KEY (`cod_plan`) REFERENCES `plan` (`cod_plan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.unit
CREATE TABLE IF NOT EXISTS `unit` (
  `cod_unit` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `number` varchar(45) NOT NULL,
  `block` varchar(45) DEFAULT NULL,
  `common_size` varchar(45) DEFAULT NULL,
  `privace_size` varchar(45) DEFAULT NULL,
  `total_size` varchar(45) DEFAULT NULL,
  `floor` varchar(50) DEFAULT NULL,
  `cod_construction` int(11) NOT NULL,
  `cod_localization` int(11) NOT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_unit`),
  KEY `fk_unit_constr_idx` (`cod_construction`),
  KEY `fk_localization_idx` (`cod_localization`),
  KEY `fk_alteration_unit` (`cod_alteration_user`),
  KEY `fk_creation_unit` (`cod_creation_user`),
  CONSTRAINT `FK_unit_localization` FOREIGN KEY (`cod_localization`) REFERENCES `localization` (`cod_localization`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alteration_unit` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_unit` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_unit_constr` FOREIGN KEY (`cod_construction`) REFERENCES `construction` (`cod_construction`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table rocha_gerenciam.unit_type
CREATE TABLE IF NOT EXISTS `unit_type` (
  `cod_unit_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `idc_active` char(1) DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) NOT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod_unit_type`),
  KEY `fk_alteration_unit_type` (`cod_alteration_user`),
  KEY `fk_creation_id_unit_type` (`cod_creation_user`),
  CONSTRAINT `fk_alteration_unit_type` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`),
  CONSTRAINT `fk_creation_id_unit_type` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for procedure rocha_gerenciam.update_situation
DELIMITER //
CREATE DEFINER=`rocha_emp`@`%` PROCEDURE `update_situation`(IN `codSituation` INT, IN `codUser` INT, IN `codProduct` INT, IN `codConstruction` INT, IN `amount` INT)
BEGIN
	DECLARE result TEXT;
	SET lc_time_names = 'pt_BR';
	
	select group_concat(tab) into result from (
	select (pc2.cod_product_construction) as tab  from product_construction pc2 where pc2.cod_product= codProduct 
	and pc2.idc_active='1' and pc2.cod_construction = codConstruction and cod_situation != codSituation order by pc2.availability_date limit amount 
	) as tab;
	
	
	set @sql = concat('update product_construction
	set cod_situation = ',codSituation,' ,
	alteration_date = now() ,
	cod_alteration_user = ' , codUser , '
	where cod_product_construction in (',result,')  '  );
	
	prepare stmt from @sql;
	execute stmt;
	
	if ( codSituation = '3' ) then
		set @sql = concat('update product_construction
		set cod_construction = null
		where cod_product_construction in (',result,')  '  );
		
		prepare stmt from @sql;
		execute stmt;
	end if;
END//
DELIMITER ;


-- Dumping structure for table rocha_gerenciam.user
CREATE TABLE IF NOT EXISTS `user` (
  `cod_user` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `login` varchar(100) NOT NULL,
  `cod_profile` int(11) NOT NULL,
  `creation_date` datetime NOT NULL,
  `alteration_date` datetime DEFAULT NULL,
  `cod_creation_user` int(11) DEFAULT NULL,
  `cod_alteration_user` int(11) DEFAULT NULL,
  `idc_active` char(1) DEFAULT '1',
  PRIMARY KEY (`cod_user`),
  KEY `FK_user_profile` (`cod_profile`),
  KEY `FK_user_user` (`cod_creation_user`),
  KEY `FK_user_user_2` (`cod_alteration_user`),
  CONSTRAINT `FK_user_profile` FOREIGN KEY (`cod_profile`) REFERENCES `profile` (`cod_profile`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_user_user` FOREIGN KEY (`cod_creation_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_user_user_2` FOREIGN KEY (`cod_alteration_user`) REFERENCES `user` (`cod_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
