﻿using System;
using System.Web;
using RochaGerenciamento.Domain.Entities;
using System.Collections.Generic;
using System.Web.Security;


namespace RochaGerenciamento.Domain.ValueObject
{
    public static class SessionUser
    {
        public static User Object
        {
            get
            {
                try
                {
                    return (User)HttpContext.Current.Session["User"];
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["User"] = value;
            }
        }

        public static int CodeUser
        {
            get
            {
                User usuario = new User();
                try
                {
                    
                    usuario = (User) HttpContext.Current.Session["User"];
                    return usuario.Code;
                }
                catch (Exception)
                {
                    
                    return 0;
                    
                }
            }
        }
    }
}