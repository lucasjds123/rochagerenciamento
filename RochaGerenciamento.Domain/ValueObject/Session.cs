﻿using System.Web;
using System;

namespace RochaGerenciamento.Domain.ValueObject
{
    public static class Session
    {
        public static int CodeUser()
        {
            return 1;
        }

        public static String UrlOrder
        {
            get
            {
                try
                {
                    return (String)HttpContext.Current.Session["UrlOrder"];
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["UrlOrder"] = value;
            }
        }

        public static String UrlBill
        {
            get
            {
                try
                {
                    return (String)HttpContext.Current.Session["UrlBill"];
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["UrlBill"] = value;
            }
        }

        public static int CodeConstruction
        {
            get
            {
                try
                {
                    return (int)HttpContext.Current.Session["ConstructionGeneral"];
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set
            {
                HttpContext.Current.Session["ConstructionGeneral"] = value;
            }
        }

        public static string NameConstruction
        {
            get
            {
                try
                {
                    return (String)HttpContext.Current.Session["NameConstruction"];
                }
                catch (Exception)
                {
                    return String.Empty;
                }
            }
            set
            {
                HttpContext.Current.Session["NameConstruction"] = value;
            }
        }

        public static int AmountInstallmentsToExpire
        {
            get
            {
                try
                {
                    return (int)HttpContext.Current.Session["AmountInstallmentsToExpire"];
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set
            {
                HttpContext.Current.Session["AmountInstallmentsToExpire"] = value;
            }
        }

        public static string AmountNotVerified
        {
            get
            {
                try
                {
                    return (string)HttpContext.Current.Session["AmountNotVerified"];
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["AmountNotVerified"] = value;
            }
        }

        public static string CodeClient
        {
            get
            {
                try
                {
                    return (string)HttpContext.Current.Session["CodeClient"];
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["CodeClient"] = value;
            }
        }
        
    }

}