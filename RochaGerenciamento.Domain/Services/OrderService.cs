﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class OrderService: ServiceBase<Order>, IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
            : base(orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public IEnumerable<object> GetDynamicList(string searchPhrase, int rowCount, int current, ref int total, string nameConstruction,
                                            string numberOrder, string nameProvider)
        {
            searchPhrase = searchPhrase.ToLower();
            var lista = _orderRepository.GetDynamicList(searchPhrase, nameConstruction, numberOrder, nameProvider);
            return HandleList(ref total, lista, rowCount, current);
        }

        public IEnumerable<object> OrderList()
        {
            return _orderRepository.OrderList();
        }

        public string GetNextNumberOrder()
        {
            return _orderRepository.GetNextNumberOrder();
        }
        
    }
}
