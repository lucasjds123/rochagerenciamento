﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class ContractService :  ServiceBase<Contract>, IContractService
    {
        private readonly IContractRepository _contractRepository;

        public ContractService(IContractRepository contractRepository)
            : base(contractRepository)
        {
            _contractRepository = contractRepository;
        }

        public bool VerifyOwners(Contract entity)
        {
            foreach (var coOwner in entity.CoOwners)
            {
                if (entity.Owner.CodeClient == coOwner.CodeClient)
                    return true;
            }
            return false;

        }
        
    }
}
