﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class InstallmentService: ServiceBase<Installment>, IInstallmentService
    {
        private readonly IInstallmentRepository _installmentRepository;

        public InstallmentService(IInstallmentRepository installmentRepository)
            : base(installmentRepository)
        {
            _installmentRepository = installmentRepository;
        }

        public string InstallmentsAboutToExpire()
        {
            IEnumerable<object> list = _installmentRepository.InstallmentsAboutToExpire(1, (int)FlagBillType.Order);
            if (list.Count() == 0)
                return string.Empty;
            string message = "Verifique! Estes boletos estão vencendo AMANHÃ:#BREAKLINE";
            foreach (object installment in list.ToList())
            {
                message = message + "Número da ordem:" + installment.GetType().GetProperty("OrderNumber").GetValue(installment, null).ToString()
                        + " código de barras: " + installment.GetType().GetProperty("BarCode").GetValue(installment, null).ToString()
                        + " na obra: " + installment.GetType().GetProperty("Construction").GetValue(installment, null).ToString() + "#BREAKLINE#BREAKLINE";
            }
            return message;
        }

        
    }
}
