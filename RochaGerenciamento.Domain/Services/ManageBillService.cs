﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using RochaGerenciamento.Domain.Interfaces.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class ManageBillService: IManageBillService
    {
        private readonly IManageBillUnitOfWork _unit;

        public ManageBillService(IManageBillUnitOfWork manageBillUoW)
        {
            _unit = manageBillUoW;
        }

        public ICollection<Order> CreateOrders(ICollection<Order> orders)
        {
            ICollection<Order> ordersAux = new Collection<Order>();
            foreach (Order order in orders)
            {
                ordersAux.Add(_unit.CreateOrders(order.Code));
                
            }
            return ordersAux;
        }

        public ICollection<Installment> CreateInstallments(string paid, double totalValue , int amountInstallments, DateTime duedate)
        {
            Installment installment;
            ICollection<Installment> installments = new Collection<Installment>();
            double valueInstallment = totalValue / amountInstallments;
            for (int i = 1; i <= amountInstallments; i++)
            {
                installment = new Installment();
                installment.Value = Math.Round(valueInstallment, 2, MidpointRounding.AwayFromZero);
                installment.Sequence = i;
                installment.Checked = RegisterSituation.NonActive;
                if (paid.Equals(RegisterSituation.Active))
                {
                    installment.Checked = RegisterSituation.Active;
                    installment.PaymentDate = DateTime.Now;
                }
                installment.DueDate = duedate.AddMonths(i - 1);
                installments.Add(installment);
            }
            return installments;
        }

        public void AddBill(Bill bill, ICollection<Order> orders, ICollection<Installment> installments)
        {
            bill.Installments = installments;
            bill.Orders = orders;
            _unit.AddBill(bill);
        }

        public void AddBill(Bill bill, ContractIncoming contractIncoming, ICollection<Installment> installments)
        {
            bill.Installments = installments;
            bill.ContractIncomings = new Collection<ContractIncoming>();
            bill.ContractIncomings.Add(contractIncoming);
            _unit.AddBill(bill);
        }

        public void UpdateOrders(Bill bill)
        {
            foreach (Order order in bill.Orders)
                order.CodeBill = null;
        }

        public void ConfirmPayment(int id)
        {
            var installment = _unit.GetInstallmentById(id);
            int flagPayment = 0;
            foreach (var inst in installment.Bill.Installments)
            {
                if (inst.Checked.Equals(RegisterSituation.NonActive))
                    flagPayment++;
            }
            installment.Checked = RegisterSituation.Active;
            installment.PaymentDate = DateTime.Now;
            if (flagPayment==1){
                installment.Bill.Checked = RegisterSituation.Active;
                _unit.UpdateBill(installment.Bill);
            }
            else
                _unit.UpdateInstallment(installment);
        }

        public void ConfirmPaymentAll(int id)
        {
            var bill = _unit.GetBillById(id);
            foreach (Installment installment in bill.Installments)
            {
                if (installment.Checked.Equals(RegisterSituation.NonActive))
                {
                    installment.Checked = RegisterSituation.Active;
                    installment.PaymentDate = DateTime.Now;
                }
            }
            bill.Checked = RegisterSituation.Active;
            _unit.UpdateBill(bill);
        }

        public void AmountAboutToExpire()
        {
            _unit.AmountAboutToExpire();
        }

    }
}
