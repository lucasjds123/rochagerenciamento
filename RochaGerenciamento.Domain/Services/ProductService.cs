﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class ProductService: ServiceBase<Product>, IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
            : base(productRepository)
        {
            _productRepository = productRepository;
        }

        public bool FindBySpecs(string name, int code = 0)
        {
            return ( _productRepository.FindBySpecs(name, code) != null);
        }

        public IEnumerable<object> ProductList()
        {
            return _productRepository.ProductList();
        }

        public IEnumerable<object> DropDownList()
        {
            return _productRepository.DropDownList();
        }
    }
}
