﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class AddressService: ServiceBase<Address>, IAddressService
    {
        private readonly IAddressRepository _addressRepository;

        public AddressService(IAddressRepository addressRepository)
            : base(addressRepository)
        {
            _addressRepository = addressRepository;
        }

        public IEnumerable<object> AddressList()
        {
            return _addressRepository.AddressList();
        }

        
    }
}
