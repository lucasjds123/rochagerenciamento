﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class RequestProductService : ServiceBase<RequestProduct>, IRequestProductService
    {
        private readonly IRequestProductRepository _requestProductRepository;

        public RequestProductService(IRequestProductRepository requestProductRepository)
            : base(requestProductRepository)
        {
            _requestProductRepository = requestProductRepository;
        }
    }
}
