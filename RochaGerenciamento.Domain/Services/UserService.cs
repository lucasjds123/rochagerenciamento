﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class UserService : ServiceBase<User>, IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
            : base(userRepository)
        {
            _userRepository = userRepository;
        }

        public bool CheckUserExistence(string login)
        {
            return _userRepository.CheckUserExistence(login);
        }

        public User LookForLoginPassword(string login, string password)
        {
            return _userRepository.LookForLoginPassword(login, password);
        }

        public override void Add(User obj)
        {
            obj.Password = Criptografia.Encrypt(obj.Password);
            if (_userRepository.CheckUserExistence(obj.Login))
                throw new Exception(ErrorMessage.LoginExists);
            if (_userRepository.CheckUserWasRemoved(obj.Login))
            {
                var user = _userRepository.FindByLoginRemoved(obj.Login);
                user.Login = obj.Login;
                user.IdcActive = GeneralField.Active;
                user.Name = obj.Name;
                user.CodeProfile = obj.CodeProfile;
                user.Password = obj.Password;
                _userRepository.Update(user);
            }
            else
                _userRepository.Add(obj);
        }

        public override void Update(User obj)
        {
            obj.Password = Criptografia.Encrypt(obj.Password);
            _userRepository.Update(obj);
        }

        public string GetEmailUserByProfile(string profile)
        {
            return _userRepository.GetEmailUserByProfile(profile) + ",";
        }
    }
}
