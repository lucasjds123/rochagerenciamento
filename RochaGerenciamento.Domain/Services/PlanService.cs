﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class PlanService : ServiceBase<Plan>, IPlanService
    {
        private readonly IPlanRepository _planRepository;

        public PlanService(IPlanRepository planRepository)
            : base(planRepository)
        {
            _planRepository = planRepository;
        }
    }
}
