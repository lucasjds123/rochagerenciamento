﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class ManufacturerService: ServiceBase<Manufacturer> , IManufacturerService
    {
        private readonly IManufacturerRepository _manufacturerRepository;

        public ManufacturerService(IManufacturerRepository manufacturerRepository)
            : base(manufacturerRepository)
        {
            _manufacturerRepository = manufacturerRepository;
        }
    }
}
