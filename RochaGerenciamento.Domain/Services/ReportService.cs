﻿using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using RochaGerenciamento.Domain.StoredProcedures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class ReportService : IReportService
    {
        private readonly IReportRepository _reportRepository;

        public ReportService(IReportRepository reportRepository)
        {
            _reportRepository = reportRepository;
        }

        public IEnumerable<object> Order(int approval, int codeConstruction, string firstDate, string lastDate)
        {
            firstDate = (firstDate == string.Empty) ? string.Empty : Convert.ToDateTime(firstDate).ToString("yyyy-MM-dd");
            lastDate = (lastDate == string.Empty) ? string.Empty : Convert.ToDateTime(lastDate).ToString("yyyy-MM-dd");
            return _reportRepository.Order(approval, codeConstruction, firstDate, lastDate);
        }

        public IEnumerable<object> Stock(int codeSituation, int codeConstruction, int codeProduct, int codeProvider, int codeUnitType, string firstDate, string lastDate)
        {
            firstDate = (firstDate == string.Empty) ? string.Empty : Convert.ToDateTime(firstDate).ToString("yyyy-MM-dd");
            lastDate = (lastDate == string.Empty) ? string.Empty : Convert.ToDateTime(lastDate).ToString("yyyy-MM-dd");
            return _reportRepository.Stock(codeSituation, codeConstruction, codeProduct, codeProvider, codeUnitType, firstDate, lastDate);
        }
        public IEnumerable<object> ExpenditureVsIncoming(int codeConstruction, string firstDate, string lastDate)
        {
            firstDate = (firstDate == string.Empty) ? string.Empty : Convert.ToDateTime(firstDate).ToString("yyyy-MM-dd");
            lastDate = (lastDate == string.Empty) ? string.Empty : Convert.ToDateTime(lastDate).ToString("yyyy-MM-dd");
            return _reportRepository.ExpenditureVsIncoming(codeConstruction, firstDate, lastDate);
        }

        public IEnumerable<object> Incoming(int codeConstruction, string firstDate, string lastDate)
        {
            firstDate = (firstDate == string.Empty) ? string.Empty : Convert.ToDateTime(firstDate).ToString("yyyy-MM-dd");
            lastDate = (lastDate == string.Empty) ? string.Empty : Convert.ToDateTime(lastDate).ToString("yyyy-MM-dd");
            return _reportRepository.Incoming(codeConstruction, firstDate, lastDate);
        }

        public IEnumerable<object> FilterOrder(int codOrder)
        {
            return _reportRepository.FilterOrder(codOrder);
        }

        public IEnumerable<object> EmployeeVsProduct(int codEmployee, string firstDate, string lastDate)
        {
            firstDate = (firstDate == string.Empty) ? string.Empty : Convert.ToDateTime(firstDate).ToString("yyyy-MM-dd");
            lastDate = (lastDate == string.Empty) ? string.Empty : Convert.ToDateTime(lastDate).ToString("yyyy-MM-dd");
            return _reportRepository.EmployeeVsProduct(codEmployee, firstDate, lastDate);
        }

        public IEnumerable<object> AttendanceDate(string year)
        {
            return _reportRepository.AttendanceDate(year);
        }

        public IEnumerable<object> ContactOrigin(string year, string month)
        {
            return _reportRepository.ContactOrigin(year, month);
        }

        public IEnumerable<object> OwnerPurchases(string year, string month)
        {
            return _reportRepository.OwnerPurchases(year, month);
        }

        public IEnumerable<ListPurchasesContract> Clients()
        {
            return _reportRepository.Clients();
        }
    }
}
