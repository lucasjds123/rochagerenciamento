﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using RochaGerenciamento.Domain.Interfaces.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class ManageOrderService :  IManageOrderService
    {
        private readonly IManageOrderUnitOfWork _unit;

        public ManageOrderService(IManageOrderUnitOfWork manageOrderUoW)
        {
            _unit = manageOrderUoW;
        }

        public void ApprovalOrder(Order order)
        {
            _unit.ApprovalOrder(order);
        }

        public void VerifyOrder(Order order)
        {
            _unit.VerifyOrder(order);
        }

        public void AddOrder(Order order, bool[] permanentPrice)
        {
            _unit.AddOrder(order, permanentPrice);
        }
    }
}
