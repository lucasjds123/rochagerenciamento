﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class BuildingAreaService: ServiceBase<BuildingArea>, IBuildingAreaService
    {
        private readonly IBuildingAreaRepository _buildingAreaRepository;

        public BuildingAreaService(IBuildingAreaRepository buildingAreaRepository)
            : base(buildingAreaRepository)
        {
            _buildingAreaRepository = buildingAreaRepository;
        }

        
    }
}
