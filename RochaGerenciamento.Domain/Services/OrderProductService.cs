﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class OrderProductService: ServiceBase<OrderProduct> , IOrderProductService
    {
        private readonly IOrderProductRepository _orderProductRepository;

        public OrderProductService(IOrderProductRepository orderProductRepository)
            : base(orderProductRepository)
        {
            _orderProductRepository = orderProductRepository;
        }
    }
}
