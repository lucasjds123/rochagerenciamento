﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class ContractIncomingService : ServiceBase<ContractIncoming>, IContractIncomingService
    {
        private readonly IContractIncomingRepository _contractIncomingRepository;

        public ContractIncomingService(IContractIncomingRepository contractIncomingRepository)
            : base(contractIncomingRepository)
        {
            _contractIncomingRepository = contractIncomingRepository;
        }

        
    }
}
