﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class SubServiceService: ServiceBase<SubService> , ISubServiceService
    {
        private readonly ISubServiceRepository _subServiceRepository;

        public SubServiceService(ISubServiceRepository subServiceRepository)
            : base(subServiceRepository)
        {
            _subServiceRepository = subServiceRepository;
        }
    }
}
