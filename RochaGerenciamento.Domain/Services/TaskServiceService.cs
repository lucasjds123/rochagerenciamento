﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class TaskServiceService: ServiceBase<TaskService>, ITaskServiceService
    {
        private readonly ITaskServiceRepository _taskServiceRepository;

        public TaskServiceService(ITaskServiceRepository taskServiceRepository)
            : base(taskServiceRepository)
        {
            _taskServiceRepository = taskServiceRepository;
        }

        public void GetNextNumberTask(int codTaskService, string dateTimeStart)
        {
            _taskServiceRepository.GetNextNumberTask(codTaskService, dateTimeStart);
        }
    }
}
