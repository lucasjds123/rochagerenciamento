﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class ConstructionService : ServiceBase<Construction>, IConstructionService
    {
        private readonly IConstructionRepository _constructionRepository;

        public ConstructionService(IConstructionRepository constructionRepository)
            : base(constructionRepository)
        {
            _constructionRepository = constructionRepository;
        }
    }
}
