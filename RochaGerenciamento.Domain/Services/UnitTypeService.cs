﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class UnitTypeService: ServiceBase<UnitType>, IUnitTypeService
    {
        private readonly IUnitTypeRepository _unitTypeRepository;

        public UnitTypeService(IUnitTypeRepository unitTypeRepository)
            : base(unitTypeRepository)
        {
            _unitTypeRepository = unitTypeRepository;
        }
    }
}
