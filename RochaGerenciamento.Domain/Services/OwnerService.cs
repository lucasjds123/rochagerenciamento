﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class OwnerService: ServiceBase<Owner>, IOwnerService
    {
        private readonly IOwnerRepository _ownerRepository;

        public OwnerService(IOwnerRepository ownerRepository)
            : base(ownerRepository)
        {
            _ownerRepository = ownerRepository;
        }

        public IEnumerable<object> DropDownList()
        {
            return _ownerRepository.DropDownList();
        }

        
    }
}
