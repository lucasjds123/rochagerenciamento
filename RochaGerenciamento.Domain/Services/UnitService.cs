﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class UnitService: ServiceBase<Unit>, IUnitService
    {
        private readonly IUnitRepository _unitRepository;

        public UnitService(IUnitRepository unitRepository)
            : base(unitRepository)
        {
            _unitRepository = unitRepository;
        }

        public IEnumerable<object> DropDownList()
        {
            return _unitRepository.DropDownList();
        }
    }
}
