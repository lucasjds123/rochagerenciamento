﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class SubServicePlanService: ServiceBase<SubServicePlan>, ISubServicePlanService
    {
        private readonly ISubServicePlanRepository _subServicePlanRepository;

        public SubServicePlanService(ISubServicePlanRepository subServicePlanRepository)
            : base(subServicePlanRepository)
        {
            _subServicePlanRepository = subServicePlanRepository;
            
        }

        public IEnumerable<object> GetDynamicList(string searchPhrase, int rowCount, int current, int status, ref int total, string nameConstruction, string namePlan, string nameService, string nameSubService,
                                        string measureUnit, string amountRepetition, string percent, string estimation, string value)
        {
            searchPhrase = searchPhrase.ToLower();
            var lista = _subServicePlanRepository.GetDynamicList(status , searchPhrase, nameConstruction, namePlan, nameService, nameSubService,
                                         measureUnit, amountRepetition, percent, estimation, value);
            return HandleList(ref total, lista, rowCount, current);
        }

        public IEnumerable<object> DependencyList()
        {
            return _subServicePlanRepository.DependencyList();
        }

        public void Start(int id)
        {
            var entity = _subServicePlanRepository.GetById(id);
            entity.CodeStatus = (int) FlagStatus.Doing;
            entity.DateStart = DateTime.Now;
            entity.DateFinish = null;
            entity.DateRefresh = null;
            _subServicePlanRepository.Update(entity);
        }
        public void UpdatePercent(int cod, double amount )
        {
            var entity = _subServicePlanRepository.GetById(cod);
            entity.Percent = amount;
            if (amount == 100)
                Finish(cod);
            else
                _subServicePlanRepository.Update(entity);
        }
        public void Stop(int id)
        {
            var entity = _subServicePlanRepository.GetById(id);
            entity.CodeStatus = (int)FlagStatus.Stop;
            entity.DateStop = DateTime.Now;
            entity.DateFinish = null;
            entity.DateRefresh = null;
            _subServicePlanRepository.Update(entity);
        }
        public void Finish(int id)
        {
            var entity = _subServicePlanRepository.GetById(id);
            entity.CodeStatus = (int)FlagStatus.Done;
            entity.DateFinish = DateTime.Now;
            _subServicePlanRepository.Update(entity);
        }
        public void Refresh(int id)
        {
            var entity = _subServicePlanRepository.GetById(id);
            entity.CodeStatus = (int)FlagStatus.Doing;
            entity.DateRefresh = DateTime.Now;
            _subServicePlanRepository.Update(entity);
        }

        public bool CheckDependency(int id)
        {
            var entity = _subServicePlanRepository.GetById(id);
            if (entity.CodeSubServicePlanDepen == null || _subServicePlanRepository.CheckDependency(entity).Count() > 0)
                return true;
            return false;
        }

    }
}
