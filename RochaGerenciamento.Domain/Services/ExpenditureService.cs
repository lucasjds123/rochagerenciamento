﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class ExpenditureService: ServiceBase<Expenditure>, IExpenditureService
    {
        private readonly IExpenditureRepository _expenditureRepository;

        public ExpenditureService(IExpenditureRepository expenditureRepository)
            : base(expenditureRepository)
        {
            _expenditureRepository = expenditureRepository;
        }
    }
}
