﻿using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class ServiceBase<TEntity> : IServiceBase<TEntity> where TEntity : class
    {
        private readonly IRepositoryBase<TEntity> _repository;

        public ServiceBase(IRepositoryBase<TEntity> repository)
        {
            _repository = repository;
        }

        public virtual void Add(TEntity obj)
        {
            _repository.Add(obj);
        }

        public TEntity GetById(int id)
        {
            return _repository.GetById(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _repository.GetAll();
        }

        public virtual void Update(TEntity obj)
        {
            _repository.Update(obj);
        }

        public void Remove(TEntity obj)
        {
            obj.GetType().GetProperty(GeneralField.Active).SetValue(obj, RegisterSituation.NonActive, null);
            _repository.Update(obj);
        }


        public IEnumerable<object> GetDynamicList(string searchPhrase, int rowCount, int current, ref int total, int code = 0)
        {
            searchPhrase = searchPhrase.ToLower();
            var lista = _repository.GetDynamicList(searchPhrase, code);
            return HandleList(ref total, lista, rowCount, current);
        }

        

        public IEnumerable<object> HandleList(ref int total, IEnumerable<object> lista, int rowCount, int current)
        {
            
            total = lista.Count();
            if (rowCount == -1)
                rowCount = total;
            return lista.Skip((current - 1) * rowCount)
                    .Take(rowCount).ToList();
        }



        public bool FindByName(TEntity entity)
        {
            return !(_repository.FindByFieldName(
                                    entity.GetType().GetProperty(GeneralField.Name).Name,
                                    entity.GetType().GetProperty(GeneralField.Name).GetValue(entity, null).ToString(),
                                    Convert.ToInt32(entity.GetType().GetProperty(GeneralField.Code).GetValue(entity, null))) != null);
        }

        
    }
}
