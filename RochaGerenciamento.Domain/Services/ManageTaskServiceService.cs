﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using RochaGerenciamento.Domain.Interfaces.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Services
{
    public class ManageTaskServiceService : IManageTaskServiceService
    {
        private readonly IManageTaskServiceUnitOfWork _unit;

        public ManageTaskServiceService(IManageTaskServiceUnitOfWork manageOrderUoW)
        {
            _unit = manageOrderUoW;
        }

        public void AddTaskService(TaskService entity)
        {
            _unit.AddTaskService(entity);
        }
    }
}
