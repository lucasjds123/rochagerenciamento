﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using RochaGerenciamento.Domain.StoredProcedures;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace RochaGerenciamento.Domain.Services
{
    public class ProductConstructionService: ServiceBase<ProductConstruction>, IProductConstructionService
    {
        private readonly IProductConstructionRepository _productConstructionRepository;

        public ProductConstructionService(IProductConstructionRepository productConstructionRepository)
            : base(productConstructionRepository)
        {
            _productConstructionRepository = productConstructionRepository;
        }

        public void UpdateSituation(double[] amount, int codSituation, int[] codProduct, int codConstruction, int codEmployee, DateTime? date = null, int[] codeProductConstructions = null)
        {

            for (int i = 0; i < amount.Length; i++)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                _productConstructionRepository.Update(codSituation, codProduct[i], codConstruction, amount[i], codEmployee, date, codeProductConstructions[i]);
            }

        }

        public string ProductsAboutToExpire()
        {
            
            IEnumerable<ListProductConstruction> list = (IEnumerable<ListProductConstruction>)_productConstructionRepository.ProductsAboutToExpire();
            if (list.Count() == 0)
                return string.Empty;
            string message = "Verifique estes produtos estão expirando:#BREAKLINE";
            foreach (ListProductConstruction productConstruction in list.ToList())
            {
                message = message + productConstruction.Product + " na obra: " + productConstruction.Construction + "#BREAKLINE";
            }
            return message;
        }

        public bool AmountInStock(int[] codeProductConstructions, int[] codProduct, double[] amount, int codSituation)
        {
            for (int i = 0; i < amount.Length; i++)
            {
                if (amount[i] > _productConstructionRepository.AmountInStock(codeProductConstructions[i],  codProduct[i], codSituation, 1))
                    return false;
            }
            return true;
        }

        public double AmountInStock( int codSituation)
        {
            return _productConstructionRepository.AmountInStock(0, 0, codSituation);
        }

        public bool CheckVerified(IEnumerable<object> entities)
        {
            foreach (var entity in (IEnumerable<ProductConstruction>) entities)
            {
                if (entity.Order != null && (entity.Order.VerifiedResult == null ))
                    return true;
            }
            return false;
        }

        public bool CheckSituation(IEnumerable<object> entities)
        {
            foreach (var entity in (IEnumerable<ProductConstruction>)entities)
            {
                if (entity.CodeSituation > 1)
                    return true;
            }
            return false;
        }

        public IEnumerable<object> GetListByIds(string ids)
        {
            IList<ProductConstruction> list = new List<ProductConstruction>();
            foreach (string id in ids.Split(','))
            {
                var entity = _productConstructionRepository.GetById(Convert.ToInt32(id));
                list.Add(entity);
            }

            return list;

        }
    }
}
