﻿using RochaGerenciamento.Domain.StoredProcedures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Services
{
    public interface IReportService
    {
        IEnumerable<object> Order(int approval, int codeConstruction, string firstDate, string lastDate);
        IEnumerable<object> Stock(int codeSituation, int codeConstruction, int codeProduct, int codeProvider, int codeUnitType, string firstDate, string lastDate);
        IEnumerable<object> ExpenditureVsIncoming(int codeConstruction, string firstDate, string lastDate);
        IEnumerable<object> Incoming(int codeConstruction, string firstDate, string lastDate);
        IEnumerable<object> FilterOrder(int codOrder);
        IEnumerable<object> EmployeeVsProduct(int codEmployee, string firstDate, string lastDate);
        IEnumerable<object> AttendanceDate(string year);
        IEnumerable<object> ContactOrigin(string year, string month);
        IEnumerable<object> OwnerPurchases(string year, string month);
        IEnumerable<ListPurchasesContract> Clients();
    }
}
