﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Services
{
    public interface IManageOrderService 
    {
        void ApprovalOrder(Order order);
        void VerifyOrder(Order order);
        void AddOrder(Order order, bool[] permanentPrice);
    }
}
