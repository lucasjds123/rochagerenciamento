﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Services
{
    public interface IOrderService : IServiceBase<Order>
    {
        IEnumerable<object> GetDynamicList(string searchPhrase, int rowCount, int current, ref int total, string nameConstruction,
                                            string numberOrder, string nameProvider);
        IEnumerable<object> OrderList();
        string GetNextNumberOrder();
    }
}
