﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Services
{
    public interface IUserService : IServiceBase<User>
    {
        bool CheckUserExistence(string login);
        User LookForLoginPassword(string login, string password);
        string GetEmailUserByProfile(string profile);
    }
}
