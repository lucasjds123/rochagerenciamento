﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Services
{
    public interface ISubServicePlanService : IServiceBase<SubServicePlan>
    {
        IEnumerable<object> GetDynamicList(string searchPhrase, int rowCount, int current, int status, ref int total, string nameConstruction, string namePlan, string nameService, string nameSubService,
                                        string measureUnit, string amountRepetition, string percent, string estimation , string value);
        IEnumerable<object> DependencyList();
        void Start(int id);
        void Stop(int id);
        void Finish(int id);
        void Refresh(int id);
        bool CheckDependency(int id);

        void UpdatePercent(int cod,double amount);
    }
}
