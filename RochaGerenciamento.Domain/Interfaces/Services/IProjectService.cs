﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Services
{
    public interface IProjectService : IServiceBase<Project>
    {
    }
}
