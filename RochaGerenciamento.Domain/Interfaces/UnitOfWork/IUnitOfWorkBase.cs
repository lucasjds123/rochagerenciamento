﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.UnitOfWork
{
    public interface IUnitOfWorkBase
    {
        void Commit();
    }
}
