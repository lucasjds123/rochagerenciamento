﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.UnitOfWork
{
    public interface IManageTaskServiceUnitOfWork : IUnitOfWorkBase
    {
        ITaskServiceRepository TaskServiceRepository { get; }
        IRepositoryBase<TaskServiceEmployee> TaskServiceEmployeeRepository { get; }
        void AddTaskService(TaskService taskService);
    }
}
