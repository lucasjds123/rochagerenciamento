﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.UnitOfWork
{
    public interface IManageOrderUnitOfWork : IUnitOfWorkBase
    {
        
        IOrderRepository OrderRepository { get; }
        IProductConstructionRepository ProductConstructionRepository { get; }

        void ApprovalOrder(Order order);
        void VerifyOrder(Order order);
        Product UpdateProduct(Product product);
        void AddOrder(Order order, bool[] permanentPrice);
    }
}
