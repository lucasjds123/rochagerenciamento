﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.UnitOfWork
{
    public interface IManageBillUnitOfWork : IUnitOfWorkBase
    {
        void AddBill(Bill bill);
        Order CreateOrders(int codeOrder);
        Installment GetInstallmentById(int id);
        Bill GetBillById(int id);
        void UpdateBill(Bill bill);
        void UpdateInstallment(Installment installment);
        void AmountAboutToExpire();
    }
}
