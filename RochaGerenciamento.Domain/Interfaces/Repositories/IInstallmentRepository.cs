﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Repositories
{
    public interface IInstallmentRepository : IRepositoryBase<Installment>
    {
        IEnumerable<object> InstallmentsAboutToExpire(int daysToExpire, int flagBillType);
    }
}
