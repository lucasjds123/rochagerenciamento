﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Repositories
{
    public interface ISubServicePlanRepository : IRepositoryBase<SubServicePlan>
    {
        IEnumerable<object> GetDynamicList(int status, string searchPhrase, string nameConstruction, string namePlan, string nameService, string nameSubService,
                                        string measureUnit, string amountRepetition, string percent, string estimation, string value);
        IEnumerable<object> DependencyList();
        IEnumerable<object> CheckDependency(SubServicePlan entity);
    }
}
