﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Repositories
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        void Add(TEntity obj);
        TEntity GetById(int id);
        IEnumerable<TEntity> GetAll();
        void Update(TEntity obj);
        void Remove(TEntity obj);

        TEntity FindByFieldName(string fieldName, string value, int code = 0);
        IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0);
        
        void DynamicListTotal(string searchPhrase, ref int total, int code = 0);

    }
}
