﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Repositories
{
    public interface IOrderRepository : IRepositoryBase<Order>
    {
        IEnumerable<object> GetDynamicList(string searchPhrase, string nameConstruction, string numberOrder, string nameProvider);
        IEnumerable<object> OrderList();
        string GetNextNumberOrder();
    }
}
