﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Repositories
{
    public interface IOrderProductRepository :  IRepositoryBase<OrderProduct>
    {
        IEnumerable<object> GetListByOrder(int codeOrder);
    }
}
