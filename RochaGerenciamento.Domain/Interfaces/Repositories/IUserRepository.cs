﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Repositories
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        bool CheckUserExistence(string login);
        User LookForLoginPassword(string login, string password);
        bool CheckUserWasRemoved(string login);
        User FindByLoginRemoved(string login);
        string GetEmailUserByProfile(string profile);
    }
}
