﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Repositories
{
    public interface IUnitRepository : IRepositoryBase<Unit>
    {
        IEnumerable<object> DropDownList();
    }
}
