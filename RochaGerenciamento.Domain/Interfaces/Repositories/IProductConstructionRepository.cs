﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.Repositories
{
    public interface IProductConstructionRepository : IRepositoryBase<ProductConstruction>
    {
        IEnumerable<object> ProductsAboutToExpire();
        void Update(int codSituation, int codProduct, int codConstruction, double amount, int codEmployee, DateTime? dataBaixa = null, int productConstruction = 0);
        void Add(int codeOrder, int? codeConstruction, string estimationDate);
        double AmountInStock(int codeProductConstruction, int codProduct , int codSituation ,double decimalPoint = 1);
        void OrderVerify(int codProduct, double amount, int codOrder, double decimalPoint);
    }
}
