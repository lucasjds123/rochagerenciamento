﻿using EntityFrameworkExtras;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class ProductConstruction
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int? CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime? CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }
        [Display(Name = "CodeProduct")]
        public int CodeProduct { get; set; }
        [Display(Name = "CodeOrder")]
        public int? CodeOrder { get; set; }
        [Display(Name = "CodeConstruction")]
        public int? CodeConstruction { get; set; }
        [Display(Name = "CodeEmployee")]
        public int? CodeEmployee { get; set; }
        [Display(Name = "CodeSituation")]
        public int CodeSituation { get; set; }
        [Display(Name = "AvailabilityDate")]
        public DateTime? AvailabilityDate { get; set; }
        [Display(Name = "Quantity")]
        public double Quantity { get; set; }

        public virtual User User { get; set; }
        public virtual Product Product { get; set; }
        public virtual Construction Construction { get; set; }
        public virtual Situation Situation { get; set; }
        public virtual Order Order { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
