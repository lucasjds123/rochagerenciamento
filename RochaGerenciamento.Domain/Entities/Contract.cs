﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class Contract
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "AgreementDate")]
        public DateTime AgreementDate { get; set; }
        [Display(Name = "WitnessName")]
        public string WitnessName { get; set; }
        [Display(Name = "WitnessCPF")]
        public string WitnessCPF { get; set; }
        [Display(Name = "CodeContractType")]
        public int CodeContractType { get; set; }
        [Display(Name = "CodeOwner")]
        public int CodeOwner { get; set; }

        public virtual User User { get; set; }
        public virtual ContractType ContractType { get; set; }
        public virtual Owner Owner { get; set; }
        public virtual ICollection<ContractIncoming> ContractIncomings { get; set; }
        public virtual ICollection<CoOwner> CoOwners { get; set; }
    }
}
