﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class OrderProduct
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }
        [Display(Name = "Code Product")]
        public int CodeProduct { get; set; }
        [Display(Name = "Code Order")]
        public int CodeOrder { get; set; }
        [Display(Name = "ValueUnit")]
        public double ValueUnit { get; set; }
        [Display(Name = "Amount")]
        public double Amount { get; set; }
        [Display(Name = "DecimalPoint")]
        public double DecimalPoint { get; set; }
        [Display(Name = "AmountVerified")]
        public double? AmountVerified { get; set; }

        public virtual Product Product { get; set; }
        public virtual User User { get; set; }
        public virtual Order Order { get; set; }
    }
}
