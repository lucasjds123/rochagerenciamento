﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class Plan
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Deadline")]
        public DateTime Deadline { get; set; }

        [Display(Name = "ConclusionDate")]
        public DateTime? ConclusionDate { get; set; }

        [Display(Name = "StartDate")]
        public DateTime StartDate { get; set; }

        [Display(Name = "CodConstruction")]
        public int CodeConstruction { get; set; }

        [Display(Name = "EstimationValue")]
        public double EstimationValue { get; set; }

        [Display(Name = "RealValue")]
        public double? RealValue { get; set; }

        public virtual Construction Construction { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<SubServicePlan> SubServicePlans { get; set; }

    }
}
