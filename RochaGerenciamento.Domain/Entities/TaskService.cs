﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class TaskService
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }
        [Display(Name = "CodeTask")]
        public int CodeTask { get; set; }
        [Display(Name = "CodeConstruction")]
        public int CodeConstruction { get; set; }
        [Display(Name = "CodeTaskSituation")]
        public int CodeTaskSituation { get; set; }
        [Display(Name = "CodeBuildingArea")]
        public int? CodeBuildingArea { get; set; }
        [Display(Name = "DateTimeStart")]
        public DateTime? DateTimeStart { get; set; }
        [Display(Name = "DateTimeStart")]
        public DateTime? DateTimeFinish { get; set; }
        [Display(Name = "CodeEmployeeFinisher")]
        public int? CodeEmployeeFinisher { get; set; }
        [Display(Name = "CodeEmployeeChecker")]
        public int? CodeEmployeeChecker { get; set; }
        [Display(Name = "DateFinishAccording")]
        public DateTime? DateFinishAccording { get; set; }
        [Display(Name = "Solution")]
        public string Solution { get; set; }
        [Display(Name = "CurrentSituation")]
        public string CurrentSituation { get; set; }
        [Display(Name = "FlagAccording")]
        public string FlagAccording { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "NumberTaskService")]
        public string NumberTaskService { get; set; }
        [Display(Name = "CodeUnit")]
        public int? CodeUnit { get; set; }
        public virtual User User { get; set; }
        public virtual Unit Unit { get; set; }
        public virtual Employee EmployeeChecker { get; set; }
        public virtual Employee EmployeeFinisher { get; set; }
        public virtual Task Task { get; set; }
        public virtual TaskSituation TaskSituation { get; set; }
        public virtual Construction Construction { get; set; }
        public virtual BuildingArea BuildingArea { get; set; }
        public virtual ICollection<TaskServiceEmployee> TaskServiceEmployees { get; set; }
    }
}
