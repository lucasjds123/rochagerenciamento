﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class ContractIncoming
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }
        [Display(Name = "CodeIncomingType")]
        public int CodeIncomingType { get; set; }
        [Display(Name = "CodeContract")]
        public int CodeContract { get; set; }
        [Display(Name = "CodeBill")]
        public int CodeBill { get; set; }

        public virtual User User { get; set; }
        public virtual Bill Bill { get; set; }
        public virtual Contract Contract { get; set; }
        public virtual IncomingType IncomingType { get; set; }
    }
}
