﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class Order
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "TotalValue")]
        public double TotalValue { get; set; }
        [Display(Name = "Discount")]
        public double Discount { get; set; }
        [Display(Name = "ShippingCost")]
        public double ShippingCost { get; set; }
        [Display(Name = "LaborCost")]
        public double LaborCost { get; set; }
        [Display(Name = "TotalValueSale")]
        public double TotalValueSale { get; set; }
        [Display(Name = "Approval")]
        public string Approval { get; set; }
        [Display(Name = "EstimationDate")]
        public DateTime? EstimationDate { get; set; }
        [Display(Name = "ArrivedDate")]
        public DateTime? ArrivedDate { get; set; }
        [Display(Name = "CodeConstruction")]
        public int CodeConstructionDestination { get; set; }
        [Display(Name = "CodeBill")]
        public int? CodeBill { get; set; }
        [Display(Name = "NumberOrder")]
        public string NumberOrder { get; set; }
        [Display(Name = "VerifiedResult")]
        public string VerifiedResult { get; set; }
        [Display(Name = "Invoice")]
        public string Invoice { get; set; }

        public virtual Construction Construction { get; set; }
        public virtual User User { get; set;}
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }
        public virtual Bill Bill { get; set; }
        public virtual ICollection<ProductConstruction> ProductConstructions { get; set; }
    }
}
