﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class Product
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Specs")]
        public string Specs { get; set; }

        [Display(Name = "Recurrent")]
        public string Recurrent { get; set; }

        [Display(Name = "Limit")]
        public double Limit { get; set; }

        [Display(Name = "Amount Per Unit")]
        public double AmountPerUnit { get; set; }

        [Display(Name = "Value Unit")]
        public double ValueUnit { get; set; }

        [Display(Name = "Code Unit Type")]
        public int CodeUnitType { get; set; }

        [Display(Name = "Code Unit Type")]
        public int CodeProvider { get; set; }

        [Display(Name = "Code Product")]
        public int? CodeManufacturer { get; set; }

        public virtual User User { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }
        public virtual Provider Provider { get; set; }
        public virtual UnitType UnitType { get; set; }
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }
        public virtual ICollection<ProductConstruction> ProductConstructions { get; set; }
        public virtual ICollection<RequestProduct> RequestProducts { get; set; }

    }
}
