﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class User 
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int? CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }

        public virtual User UsuarioAux { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Display(Name = "Login")]
        public string Login { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Profile Code")]
        public int CodeProfile { get; set; }

        public virtual Profile Profile { get; set; }

        //para as foreign key relacionado a codigousuariocriacao ea alteracao
        public virtual ICollection<EmployeeStatus> EmployeeStatuss { get; set; }
        public virtual ICollection<TaskServiceEmployee> TaskServiceEmployees { get; set; }
        public virtual ICollection<TaskService> TaskServices { get; set; }
        public virtual ICollection<TaskSituation> TaskSituations { get; set; }
        public virtual ICollection<BuildingArea> BuildingAreas { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
        public virtual ICollection<Manufacturer> Manufacturers { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
        public virtual ICollection<StatusPurchase> StatusPurchases { get; set; }
        public virtual ICollection<CoOwner> CoOwners { get; set; }
        public virtual ICollection<Owner> Owners { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
        public virtual ICollection<Position> Positions { get; set; }
        public virtual ICollection<RequestProduct> RequestProducts { get; set; }
        public virtual ICollection<Situation> Situations { get; set; }
        public virtual ICollection<ContractIncoming> ContractIncomings { get; set; }
        public virtual ICollection<IncomingType> IncomingTypes { get; set; }
        public virtual ICollection<Contract> Contracts { get; set; }
        public virtual ICollection<ContractType> ContractTypes { get; set; }
        public virtual ICollection<Client> Clients { get; set; }
        public virtual ICollection<ContactOrigin> ContactOrigins { get; set; }
        public virtual ICollection<CivilStatus> CivilStatuss { get; set; }
        public virtual ICollection<Unit> Units { get; set; }
        public virtual ICollection<Localization> Localizations { get; set; }
        public virtual ICollection<Profile> Profiles { get; set; }
        public virtual ICollection<ExpenditureType> ExpenditureTypes { get; set; }
        public virtual ICollection<Expenditure> Expenditures { get; set; }
        public virtual ICollection<User> Users { get; set; }

        public virtual ICollection<Construction> Constructions { get; set; }

        public virtual ICollection<Plan> Plans { get; set; }
        public virtual ICollection<UnitType> UnitTypes { get; set; }
        public virtual ICollection<MaterialType> MaterialTypes { get; set; }

        public virtual ICollection<Provider> Providers { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<Request> Requests { get; set; }
        public virtual ICollection<Service> Services { get; set; }
        public virtual ICollection<SubService> SubServices { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
        public virtual ICollection<SubServicePlan> SubServicePlans { get; set; }
        public virtual ICollection<Status> Statuss { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }
        public virtual ICollection<ProductConstruction> ProdutConstructions { get; set; }
        public virtual ICollection<Bill> Bills { get; set; }
        public virtual ICollection<Installment> Installments { get; set; }

    }
}
