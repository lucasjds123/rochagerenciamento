﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class RequestProduct
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }
        [Display(Name = "CodProduct")]
        public int CodeProduct { get; set; }
        [Display(Name = "CodRequest")]
        public int CodeRequest { get; set; }
        [Display(Name = "Amount")]
        public double Amount { get; set; }

        public virtual Product Product { get; set; }
        public virtual Request Request { get; set; }
        public virtual User User { get; set; }

    }
}
