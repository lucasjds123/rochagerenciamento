﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class Construction
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "CodeAddress")]
        public int? CodeAddress { get; set; }

        public virtual User User { get; set; }
        public virtual Address Address { get; set; }
        public virtual ICollection<BuildingArea> BuildingAreas { get; set; }
        public virtual ICollection<Unit> Units { get; set; }
        public virtual ICollection<Plan> Plans { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<ProductConstruction> ProductConstructions { get; set; }
        public virtual ICollection<Expenditure> Expenditures { get; set; }
        public virtual ICollection<TaskService> TaskServices { get; set; }
    }
}
