﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class SubServicePlan
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Order")]
        public string Order { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }

        [Display(Name = "Total Value")]
        public double TotalValue { get; set; }

        [Display(Name = "MeasureUnit")]
        public string MeasuraUnit { get; set; }

        [Display(Name = "Percent")]
        public double Percent { get; set; }

        [Display(Name = "AmountRepetition")]
        public int AmountRepetition { get; set; }


        [Display(Name = "Code Status")]
        public int CodeStatus { get; set; }

        [Display(Name = "DateStart")]
        public DateTime? DateStart { get; set; }

        [Display(Name = "DateStop")]
        public DateTime? DateStop { get; set; }

        [Display(Name = "DateRefresh")]
        public DateTime? DateRefresh { get; set; }

        [Display(Name = "DateFinish")]
        public DateTime? DateFinish { get; set; }

        [Display(Name = "EstimationInDays")]
        public int EstimationInDays { get; set; }

        [Display(Name = "Code Plan")]
        public int CodePlan { get; set; }

        [Display(Name = "Code Sub Service")]
        public int CodeSubService { get; set; }

        [Display(Name = "CodeSubServicePlanDepen")]
        public int? CodeSubServicePlanDepen { get; set; }

        public virtual Plan Plan { get; set; }
        public virtual SubService SubService { get; set; }
        public virtual SubServicePlan SubServicePlanAux { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<SubServicePlan> SubServicePlans { get; set; }
        public virtual Status Status{ get; set; }

        
    }
}
