﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class Employee
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "BirthDate")]
        public DateTime? BirthDate { get; set; }

        [Display(Name = "HiredDate")]
        public DateTime HiredDate { get; set; }

        [Display(Name = "CodPosition")]
        public int CodePosition { get; set; }
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Display(Name = "ThirdParty")]
        public string ThirdParty { get; set; }
        [Display(Name = "CodeEmployeeStatus")]
        public int? CodeEmployeeStatus { get; set; }

        public virtual Position Position { get; set; }
        public virtual User User { get; set; }
        public virtual EmployeeStatus EmployeeStatus { get; set; }
        public virtual ICollection<ProductConstruction> ProductConstructions { get; set; }
        public virtual ICollection<TaskService> TaskServicesChecker { get; set; }
        public virtual ICollection<TaskService> TaskServicesFinisher { get; set; }
        public virtual ICollection<TaskServiceEmployee> TaskServiceEmployees { get; set; }
    }
}
