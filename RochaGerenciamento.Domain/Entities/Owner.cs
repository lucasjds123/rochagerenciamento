﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class Owner
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }

        [Display(Name = "Code Unit")]
        public int CodeUnit { get; set; }
        [Display(Name = "Code Client")]
        public int CodeClient { get; set; }
        [Display(Name = "Code Status")]
        public int CodeStatusPurchase { get; set; }

        public virtual User User { get; set; }
        public virtual StatusPurchase StatusPurchase { get; set; }
        public virtual Client Client { get; set; }
        public virtual Unit Unit { get; set; }
        public virtual ICollection<Contract> Contracts { get; set; }
        public virtual ICollection<CoOwner> CoOwners { get; set; }
    }
}
