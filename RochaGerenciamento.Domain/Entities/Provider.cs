﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class Provider
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "PSQ")]
        public string PSQ { get; set; }
        [Display(Name = "Contact")]
        public string Contact { get; set; }
        [Display(Name = "Phone")]
        public string Phone { get; set; }

        [Display(Name = "CodeAddress")]
        public int? CodeAddress { get; set; }

        [Display(Name = "Code Material Type")]
        public int CodeMaterialType { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        public virtual MaterialType MaterialType { get; set; }

        public virtual User User { get; set; }
        public virtual Address Address { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
