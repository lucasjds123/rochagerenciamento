﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class Client
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Phone")]
        public string Phone { get; set; }
        [Display(Name = "CPF")]
        public string CPF { get; set; }
        [Display(Name = "RG")]
        public string RG { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "LookingFor")]
        public string LookingFor { get; set; }
        [Display(Name = "Observation")]
        public string Observation { get; set; }
        [Display(Name = "JobTitle")]
        public string JobTitle { get; set; }
        [Display(Name = "Income")]
        public double? Income { get; set; }
        [Display(Name = "BirthDate")]
        public DateTime? BirthDate { get; set; }
        [Display(Name = "AttendanceDate")]
        public DateTime? AttendanceDate { get; set; }
        [Display(Name = "CodeContactOrigin")]
        public int? CodeContactOrigin { get; set; }
        [Display(Name = "CodeCivilStatus")]
        public int? CodeCivilStatus { get; set; }
        [Display(Name = "CodeAddress")]
        public int? CodeAddress { get; set; }

        public virtual User User { get; set; }
        public virtual Address Address { get; set; }
        public virtual CivilStatus CivilStatus { get; set; }
        public virtual ContactOrigin ContactOrigin { get; set; }

        public virtual ICollection<Owner> Owners { get; set; }
        public virtual ICollection<CoOwner> CoOwners { get; set; }
    }
}
