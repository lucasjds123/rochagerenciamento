﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class Request
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }
        [Display(Name = "Tittle")]
        public string Tittle { get; set; }
        [Display(Name = "Message")]
        public string Message { get; set; }
        [Display(Name = "Need Date")]
        public DateTime NeedDate { get; set; }
        [Display(Name = "Observation")]
        public string Observation { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<RequestProduct> RequestProducts { get; set; }

    }
}
