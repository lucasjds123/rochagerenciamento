﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class Address
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
        [Display(Name = "State")]
        public string State { get; set; }
        [Display(Name = "Number")]
        public string Number { get; set; }
        [Display(Name = "Borough")]
        public string Borough { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<Client> Clients { get; set; }
        public virtual ICollection<Construction> Constructions { get; set; }
        public virtual ICollection<Provider> Providers { get; set; }

    }
}
