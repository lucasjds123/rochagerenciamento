﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class Unit
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Floor")]
        public string Floor { get; set; }
        [Display(Name = "Number")]
        public string Number { get; set; }
        [Display(Name = "Block")]
        public string Block { get; set; }
        [Display(Name = "CommonSize")]
        public string CommonSize { get; set; }
        [Display(Name = "PrivaceSize")]
        public string PrivaceSize { get; set; }
        [Display(Name = "OtherAreasSize")]
        public string OtherAreasSize { get; set; }
        [Display(Name = "TotalSize")]
        public string TotalSize { get; set; }
        [Display(Name = "CodeConstruction")]
        public int CodeConstruction { get; set; }
        [Display(Name = "CodeLocalization")]
        public int CodeLocalization { get; set; }

        public virtual User User { get; set; }
        public virtual Construction Construction { get; set; }
        public virtual Localization Localization { get; set; }
        public virtual ICollection<Owner> Owners { get; set; }
        public virtual ICollection<TaskService> TaskServices { get; set; }

    }
}
