﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Entities
{
    public class Expenditure
    {
        [Key, Required, Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Code User Creation")]
        public int CodeCreationUser { get; set; }
        [Display(Name = "Code User Alteration")]
        public int? CodeAlterationUser { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Alteration Date")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "IDC Ative")]
        public string IdcActive { get; set; }

        [Display(Name = "Value")]
        public double Value { get; set; }
        [Display(Name = "PurchaseDate")]
        public DateTime PurchaseDate { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Observation")]
        public string Observation { get; set; }
        [Display(Name = "CodeConstruction")]
        public int CodeConstruction { get; set; }
        [Display(Name = "CodeExpenditureType")]
        public int CodeExpenditureType { get; set; }

        public virtual User User { get; set; }
        public virtual Construction Construction { get; set; }
        public virtual ExpenditureType ExpenditureType { get; set; }
    }
}
