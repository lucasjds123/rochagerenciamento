﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Enums
{
    public static class SuccessMessage
    {
        public static string Add
        {
            get
            {
                return "Cadastrado com sucesso";
            }
        }

        public static string AddAndEmailSent
        {
            get
            {
                return "Cadastrado e solicitação enviada com sucesso";
            }
        }

        public static string Alter
        {
            get
            {
                return "Alterado com sucesso";
            }
        }

        public static string Approval
        {
            get
            {
                return "A Ordem de Compra foi aprovada com sucesso e um alerta enviado para o Almoxarifado.";
            }
        }

        public static string Verified
        {
            get
            {
                return "A ordem foi analisado com sucesso. Um email foi enviado ao setor de compras informando-o sobre o estado da ordem!";
            }
        }

        public static string Delete
        {
            get
            {
                return "Excluído com sucesso";
            }
        }

        public static string Email
        {
            get
            {
                return "Email enviado com sucesso!";
            }
        }

        public static string StartConfirmed
        {
            get
            {
                return "O serviço foi iniciado com sucesso!";
            }
        }

        public static string Percent
        {
            get
            {
                return "Porcentagem atualizada com sucesso!";
            }
        }

        public static string Stop
        {
            get
            {
                return "O serviço foi parado com sucesso!";
            }
        }

        public static string Paid
        {
            get
            {
                return "Esta parcela foi paga!";
            }
        }

        public static string PaidAll
        {
            get
            {
                return "Este boleto e suas parcelas estão pagos!";
            }
        }

        public static string Confirmed
        {
            get
            {
                return "A baixa foi realizada com sucesso!";
            }
        }

        public static string Back
        {
            get
            {
                return "O produtos voltaram ao estoque com sucesso!";
            }
        }

        public static string ConstructionChosen
        {
            get
            {
                return "A obra foi escolhida com sucesso!";
            }
        }

        public static string Recycle
        {
            get
            {
                return "Os produtos foram disvinculados com sucesso!";
            }
        }

        public static string Report
        {
            get
            {
                return "Relatório gerado com sucesso!";
            }
        }

        public static string Confirm
        {
            get
            {
                return "A baixa foi feita com sucesso!";
            }
        }
        public static string TaskStarted
        {
            get
            {
                return "A tarefa foi iniciada com sucesso!";
            }
        }

        public static string TaskClosed
        {
            get
            {
                return "A tarefa foi fechada com sucesso!";
            }
        }

        public static string TaskConfirmed
        {
            get
            {
                return "A tarefa foi confirmada com sucesso!";
            }
        }

        public static string TaskUnconfirmed
        {
            get
            {
                return "A tarefa foi atualizada com sucesso!Esta tarefa esta nao conforme";
            }
        }
    }
}
