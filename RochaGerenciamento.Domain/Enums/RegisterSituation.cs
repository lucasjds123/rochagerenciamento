﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Enums
{
    public static class RegisterSituation
    {
        public static string Active
        {
            get
            {
                return "1";
            }
        }

        public static string NonActive
        {
            get
            {
                return "0";
            }
        }

        public static string Yes
        {
            get
            {
                return "Sim";
            }
        }

        public static string No
        {
            get
            {
                return "Não";
            }
        }

        public static string Green
        {
            get
            {
                return "Green";
            }
        }

        public static string Orange
        {
            get
            {
                return "Orange";
            }
        }

        public static string Red
        {
            get
            {
                return "Red";
            }
        }
    }
}
