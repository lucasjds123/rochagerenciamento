﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Enums
{
    public enum FlagBillType
    {
        Order = 1,
        Incoming = 2
    }
}
