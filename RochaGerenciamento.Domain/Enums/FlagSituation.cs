﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Enums
{
    public enum FlagSituation
    {
        Available = 1,
        NotAvailable = 2,
        NoBond = 3,
        Verified = 4,
    }
}
