﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Enums
{
    public static class ErrorMessage
    {
        public static string RegisterNotFound
        {
            get
            {
                return "Não foi encontrado nenhum registro!";
            }
        }

        public static string EndSession
        {
            get
            {
                return "Fim da sessão atual. Favor logar novamente";
            }
        }

        public static string FileExists
        {
            get
            {
                return "Este Arquivo já existe na plataforma. Verifique o arquivo que está tentando enviar!";
            }
        }
        public static string FileNotExists
        {
            get
            {
                return "Este arquivo não existe!";
            }
        }
        public static string ErrorLogin
        {
            get
            {
                return "Não foi possível fazer login no sistema! Usuário ou senha errado";
            }
        }

        public static string FieldRequired
        {
            get
            {
                return "Algum erro ocorreu! ou está faltando preencher algum campo obrigatório";
            }
        }

        public static string AdminError
        {
            get
            {
                return "Algum erro ocorreu! Por favor, contate o administrador do sistema";
            }
        }

        public static string LoginExists
        {
            get
            {
                return "O Login já existe no sistema";
            }
        }

        public static string Email
        {
            get
            {
                return "Email não enviado! Algum erro ocorreu.";
            }
        }

        public static string NotCompleted
        {
            get
            {
                return "Não foi possível completar a operação";
            }
        }
        public static string Dependency
        {
            get
            {
                return "O serviço não pode começar pois existe uma dependência!";
            }
        }

        public static string Report
        {
            get
            {
                return "Relatório NÃO foi gerado com sucesso!";
            }
        }

        public static string Confirm
        {
            get
            {
                return "A baixa nao foi feita com sucesso";
            }
        }

        public static string AmountStock
        {
            get
            {
                return "A quantidade em estoque eh menor do que a informada";
            }
        }

        public static string AlreadyConfirmed
        {
            get
            {
                return "Ja foi dado baixa neste item! Tente outro!";
            }
        }

        public static string NeedsToBeConfirmed
        {
            get
            {
                return "Estes itens ainda nao foram verificados! Verifique no formulario de nao conformidade";
            }
        }

        public static string NotApproved
        {
            get
            {
                return "Este item ainda nao foi aprovado pelo financeiro";
            }
        }

        public static string Approved
        {
            get
            {
                return "Este item ja foi aprovado pelo financeiro";
            }
        }

        public static string EmployeePassword
        {
            get
            {
                return "A Senha informada esta errada";
            }
        }

        public static string SameClient
        {
            get
            {
                return "Um propietario nao pode ser co-proprietario tambem! Verifique!";
            }
        }

        public static string NotExist
        {
            get
            {
                return "Um(ns) dos item(ns) escolhido nao existe(m) na base de dados!";
            }
        }

        public static string TaskStartedAlready
        {
            get
            {
                return "Esta tarefa ja foi iniciada!";
            }
        }

        public static string TaskClosedAlready
        {
            get
            {
                return "Esta tarefa nao pode ser fechada!";
            }
        }

        public static string TaskCannotBeClosed
        {
            get
            {
                return "Esta tarefa nao pode ser fechada!Primeiramente, inicie a tarefa!";
            }
        }

        public static string TaskNotClosed
        {
            get
            {
                return "Esta tarefa ainda nao foi fechada!Feche-a primeiro antes de confirmar";
            }
        }


        public static string TaskConfirmedAlready
        {
            get
            {
                return "Esta tarefa ja esta confirmada ou nao confirmada!";
            }
        }
        
    }
}
