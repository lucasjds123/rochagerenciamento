﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Enums
{
    public enum FlagStatus
    {
        ToDo = 1,
        Doing = 2,
        Stop = 3,
        Done = 4,

    }
}
