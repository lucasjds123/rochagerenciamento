﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Enums
{
    public enum FlagProfile
    {
        Administrador = 1,
        Financeiro = 2,
        Compras = 3,
        Almoxarifado = 4,
        Vendedor = 5,
        Encarregado = 6,

    }
}
