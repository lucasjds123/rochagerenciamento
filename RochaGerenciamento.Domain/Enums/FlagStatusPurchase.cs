﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Enums
{
    public enum FlagStatusPurchase
    {
        InProgress = 1,
        Sold = 2,
        NotSold = 3,
    }
}
