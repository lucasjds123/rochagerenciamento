﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Enums
{
    public static class GeneralField
    {
        public static string Active
        {
            get
            {
                return "IdcActive";
            }
        }

        public static string Name
        {
            get
            {
                return "Name";
            }
        }

        public static string Code
        {
            get
            {
                return "Code";
            }
        }

        public static string CreationDate
        {
            get
            {
                return "CreationDate";
            }
        }

        public static string CodeCreationUser
        {
            get
            {
                return "CodeCreationUser";
            }
        }

        public static string AlterationDate
        {
            get
            {
                return "AlterationDate";
            }
        }

        public static string CodeAlterationUser
        {
            get
            {
                return "CodeAlterationUser";
            }
        }

        public static string Dependency
        {
            get
            {
                return "Dependency";
            }
        }

        public static string ProductValue
        {
            get
            {
                return "ProductValue";
            }
        }

        public static string UnitValue
        {
            get
            {
                return "UnitValue";
            }
        }

        public static string OwnerValue
        {
            get
            {
                return "OwnerValue";
            }
        }
    }
}
