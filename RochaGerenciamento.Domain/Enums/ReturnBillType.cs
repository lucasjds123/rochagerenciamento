﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Enums
{
    public static class ReturnBillType
    {
        public static string Order
        {
            get
            {
                return "ManageBill";
            }
        }

        public static string Incoming
        {
            get
            {
                return "ContractIncoming";
            }
        }
    }

}
