﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Enums
{
    public enum FlagTaskSituation
    {
        Open = 1,
        Closed = 2,
        NotConfirmed = 3,
        Confirmed = 4,
    }
}
