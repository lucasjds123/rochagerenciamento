﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.StoredProcedures
{
    public class ListIncoming
    {
        [Display(Name = "Contract")]
        public string Contract { get; set; }
        [Display(Name = "ContractType")]
        public string ContractType { get; set; }
        [Display(Name = "AgreementDate")]
        public string AgreementDate { get; set; }
        [Display(Name = "Total")]
        public string Total { get; set; }
        [Display(Name = "Construction")]
        public string Construction { get; set; }
        [Display(Name = "Unit")]
        public string Unit { get; set; }
        [Display(Name = "Number")]
        public string Number { get; set; }
        [Display(Name = "Block")]
        public string Block { get; set; }
        [Display(Name = "Localization")]
        public string Localization { get; set; }
        [Display(Name = "Client")]
        public string Client { get; set; }
        [Display(Name = "Document")]
        public string Document { get; set; }
    }
}
