﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.StoredProcedures
{
    public class ListPurchasesContract
    {
        [Display(Name = "Client")]
        public string Client { get; set; }
        [Display(Name = "Phone")]
        public string Phone { get; set; }
        [Display(Name = "ContactOrigin")]
        public string ContactOrigin { get; set; }
        [Display(Name = "AttendanceDate")]
        public string AttendanceDate { get; set; }
        [Display(Name = "Day")]
        public string Day { get; set; }
        [Display(Name = "Month")]
        public string Month { get; set; }
        [Display(Name = "Year")]
        public string Year { get; set; }
    }
}
