﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.StoredProcedures
{
    public class ResultGraph
    {
        [Display(Name = "Label")]
        public string Label { get; set; }
        [Display(Name = "Amount")]
        public string Amount { get; set; }
    }
}
