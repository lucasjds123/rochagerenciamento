﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.StoredProcedures
{
    public class ListProductConstruction
    {
        [Display(Name = "Product")]
        public string Product { get; set; }
        [Display(Name = "Code")]
        public int Code { get; set; }
        [Display(Name = "Provider")]
        public string Provider { get; set; }
        [Display(Name = "AvailabilityDate")]
        public DateTime? AvailabilityDate { get; set; }
        [Display(Name = "Situation")]
        public string Situation { get; set; }
        [Display(Name = "Amount")]
        public string Amount { get; set; }
        [Display(Name = "ValueUnit")]
        public string ValueUnit { get; set; }
        [Display(Name = "Construction")]
        public string Construction { get; set; }
        [Display(Name = "Limit")]
        public string Limit { get; set; }
        [Display(Name = "Recurrent")]
        public string Recurrent { get; set; }
        [Display(Name = "AmountPerUnit")]
        public string AmountPerUnit { get; set; }
        [Display(Name = "UnitType")]
        public string UnitType { get; set; }
        [Display(Name = "CheckDate")]
        public string CheckDate { get; set; }
        [Display(Name = "CodProduct")]
        public string CodProduct { get; set; }
        [Display(Name = "CodConstruction")]
        public string CodConstruction { get; set; }
        [Display(Name = "AlterationDate")]
        public DateTime? AlterationDate { get; set; }
        [Display(Name = "NumberOrder")]
        public string NumberOrder { get; set; }
        [Display(Name = "Employee")]
        public string Employee { get; set; }
        [Display(Name = "CodeEmployee")]
        public string CodeEmployee { get; set; }
        [Display(Name = "RawDate")]
        public string RawDate { get; set; }

   
        
    }
}
