﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.StoredProcedures
{
    public class ListOrder
    {
        [Display(Name = "CodeOrder")]
        public string CodeOrder { get; set; }
        [Display(Name = "Order")]
        public string Order { get; set; }
        [Display(Name = "NumberOrder")]
        public string NumberOrder { get; set; }
        [Display(Name = "Approval")]
        public string Approval { get; set; }
        [Display(Name = "EstimationDate")]
        public string EstimationDate { get; set; }
        [Display(Name = "CreationDate")]
        public string CreationDate { get; set; }
        [Display(Name = "Construction")]
        public string Construction { get; set; }
        [Display(Name = "ProductAmount")]
        public string ProductAmount { get; set; }
        [Display(Name = "TotalValue")]
        public string TotalValue { get; set; }
        [Display(Name = "Discount")]
        public string Discount { get; set; }
        [Display(Name = "TotalValueSale")]
        public string TotalValueSale { get; set; }
        [Display(Name = "InstallmentAmount")]
        public string InstallmentAmount { get; set; }
        [Display(Name = "Recurrent")]
        public string Recurrent { get; set; }
        [Display(Name = "VerifiedResult")]
        public string VerifiedResult { get; set; }
        [Display(Name = "Provider")]
        public string Provider { get; set; }

    }
}
