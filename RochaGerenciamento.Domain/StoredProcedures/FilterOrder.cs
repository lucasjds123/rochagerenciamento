﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.StoredProcedures
{
    public class FilterOrder
    {
        [Display(Name = "UnitType")]
        public string UnitType { get; set; }
        [Display(Name = "Product")]
        public string Product { get; set; }
        [Display(Name = "Amount")]
        public string Amount { get; set; }
        [Display(Name = "ValueUnit")]
        public string ValueUnit { get; set; }
        [Display(Name = "Total")]
        public string Total { get; set; }
        [Display(Name = "Provider")]
        public string Provider { get; set; }
        [Display(Name = "NumOrder")]
        public string NumOrder { get; set; }
        [Display(Name = "Estimation")]
        public string Estimation { get; set; }
        [Display(Name = "Discount")]
        public string Discount { get; set; }
        [Display(Name = "TotalValueSale")]
        public string TotalValueSale { get; set; }
        [Display(Name = "Contact")]
        public string Contact { get; set; }
        [Display(Name = "Phone")]
        public string Phone { get; set; }
        [Display(Name = "Destination")]
        public string Destination { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "ShippingCost")]
        public string ShippingCost { get; set; }
        [Display(Name = "LaborCost")]
        public string LaborCost { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
    }
}
