﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.StoredProcedures
{
    public class ListTaskService
    {
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Display(Name = "Situation")]
        public string Situation { get; set; }
        [Display(Name = "Task")]
        public string Task { get; set; }
        [Display(Name = "Construction")]
        public string Construction { get; set; }
        [Display(Name = "Area")]
        public string Area { get; set; }
        [Display(Name = "DateTimeStart")]
        public DateTime? DateTimeStart { get; set; }
        [Display(Name = "DateTimeFinish")]
        public DateTime? DateTimeFinish { get; set; }
        [Display(Name = "NumberTaskService")]
        public string NumberTaskService { get; set; }
    }
}
