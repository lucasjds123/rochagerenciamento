﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.StoredProcedures
{
    public class ResultExpenditureIncoming
    {
        [Display(Name = "Expenditure")]
        public string Expenditure { get; set; }
        [Display(Name = "Incoming")]
        public string Incoming { get; set; }
        [Display(Name = "Result")]
        public string Result { get; set; }
    }
}
