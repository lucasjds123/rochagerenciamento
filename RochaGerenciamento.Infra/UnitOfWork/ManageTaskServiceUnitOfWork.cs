﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.Services;
using RochaGerenciamento.Domain.Interfaces.UnitOfWork;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Infra.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.UnitOfWork
{
    public class ManageTaskServiceUnitOfWork : UnitOfWorkBase, IManageTaskServiceUnitOfWork
    {
        private ITaskServiceRepository _taskServiceRepository;
        private IRepositoryBase<TaskServiceEmployee> _taskServiceEmployeeRepository;

        public ManageTaskServiceUnitOfWork(Context dataBaseContext)
            : base(dataBaseContext)
        {
            
        }

        public ITaskServiceRepository TaskServiceRepository
        {
            get
            {
                if (_taskServiceRepository == null)
                {
                    _taskServiceRepository = new TaskServiceRepository(Db);
                }
                return _taskServiceRepository;
            }
        }

        public IRepositoryBase<TaskServiceEmployee> TaskServiceEmployeeRepository
        {
            get
            {
                if (_taskServiceEmployeeRepository == null)
                {
                    _taskServiceEmployeeRepository = new RepositoryBase<TaskServiceEmployee>(Db);
                }
                return _taskServiceEmployeeRepository;
            }
        }

        public void AddTaskService(TaskService taskService)
        {
            foreach(TaskServiceEmployee entity in taskService.TaskServiceEmployees){
                TaskServiceEmployeeRepository.Add(entity);
            }
            taskService.TaskServiceEmployees = null;
            TaskServiceRepository.Update(taskService);
        }

    }
}
