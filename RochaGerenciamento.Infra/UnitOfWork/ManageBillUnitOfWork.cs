﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.UnitOfWork;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Infra.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.UnitOfWork
{
    public class ManageBillUnitOfWork : UnitOfWorkBase, IManageBillUnitOfWork
    {
        private IOrderRepository _orderRepository;
        private IBillRepository _billRepository;
        private IInstallmentRepository _installmentRepository;

        public ManageBillUnitOfWork(Context dataBaseContext)
            : base(dataBaseContext)
        {
            
        }

        public IOrderRepository OrderRepository
        {
            get
            {
                if (_orderRepository == null)
                {
                    _orderRepository = new OrderRepository(Db);
                }
                return _orderRepository;
            }
        }

        public IInstallmentRepository InstallmentRepository
        {
            get
            {
                if (_installmentRepository == null)
                {
                    _installmentRepository = new InstallmentRepository(Db);
                }
                return _installmentRepository;
            }
        }

        public IBillRepository BillRepository
        {
            get
            {
                if (_billRepository == null)
                {
                    _billRepository = new BillRepository(Db);
                }
                return _billRepository;
            }
        }

        

        public Order CreateOrders(int codeOrder)
        {
            return OrderRepository.GetById(codeOrder);
        }

        public void AddBill(Bill bill)
        {
            BillRepository.Add(bill);
        }

        public Installment GetInstallmentById(int id)
        {
            return InstallmentRepository.GetById(id);
        }

        public void UpdateBill(Bill bill)
        {
            BillRepository.Update(bill);
        }

        public void UpdateInstallment(Installment installment)
        {
            InstallmentRepository.Update(installment);
        }

        public Bill GetBillById(int id)
        {
            return BillRepository.GetById(id);
        }

        public void AmountAboutToExpire()
        {
            Session.AmountInstallmentsToExpire = InstallmentRepository.InstallmentsAboutToExpire(3, (int) FlagBillType.Order).Count();
        }
    }
}
