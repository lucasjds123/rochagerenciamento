﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Interfaces.UnitOfWork;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Infra.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.UnitOfWork
{
    public class ManageOrderUnitOfWork : UnitOfWorkBase, IManageOrderUnitOfWork
    {
        
        private IOrderRepository _orderRepository;
        private IProductConstructionRepository _productConstructionRepository;
        private IOrderProductRepository _orderProductRepository;
        private IProductRepository _productRepository;

        public ManageOrderUnitOfWork(Context dataBaseContext)
            : base(dataBaseContext)
        {
            
        }

        public IOrderRepository OrderRepository
        {
            get
            {
                if (_orderRepository == null)
                {
                    _orderRepository = new OrderRepository(Db);
                }
                return _orderRepository;
            }
        }

        public IProductConstructionRepository ProductConstructionRepository
        {
            get
            {
                if (_productConstructionRepository == null)
                {
                    _productConstructionRepository = new ProductConstructionRepository(Db);
                }
                return _productConstructionRepository;
            }
        }

        public IProductRepository ProductRepository
        {
            get
            {
                if (_productRepository == null)
                {
                    _productRepository = new ProductRepository(Db);
                }
                return _productRepository;
            }
        }

        

        public void ApprovalOrder(Order entity)
        {
            if (OrderRepository.GetById(entity.Code).Approval.Equals(RegisterSituation.NonActive))
            {

                var date = entity.EstimationDate.Value.Date.ToString("yyyy-MM-dd");

                ProductConstructionRepository.Add(entity.Code, null, date);
            }
        }

        public void AddOrder(Order order, bool[] permamentPrice)
        {
            for(int i = 0 ; i < order.OrderProducts.ToList().Count; i++)
            {
                //order.OrderProducts.ToArray()[i].DecimalPoint = (order.OrderProducts.ToArray()[i].Amount % 1) > 0 ? 0.1 : 1;
                order.OrderProducts.ToArray()[i].DecimalPoint = 0.1;
                if (permamentPrice[i])
                    order.OrderProducts.ToArray()[i].Product = UpdateProduct(order.OrderProducts.ToArray()[i].Product);
                else
                    order.OrderProducts.ToArray()[i].Product = null;
            }
            OrderRepository.Add(order);
        }

        public Product UpdateProduct(Product product)
        {
            var entity = ProductRepository.GetById(product.Code);
            entity.ValueUnit = product.ValueUnit;
            return entity;
        }

        public void VerifyOrder(Order entity)
        {
            var order = OrderRepository.GetById(entity.Code);
            var productConstructions = order.ProductConstructions;
            if (entity.VerifiedResult.Equals(RegisterSituation.Active))
            {
                foreach (var orderProduct in entity.OrderProducts)
                {
                    orderProduct.AmountVerified = orderProduct.Amount;
                    var productConstruction = productConstructions.Where(x => x.CodeSituation == (int)FlagSituation.Available ).FirstOrDefault();
                    productConstruction.Quantity = orderProduct.Amount;
                    ProductConstructionRepository.Update(productConstruction);
                    productConstruction = productConstructions.Where(x => x.CodeSituation == (int)FlagSituation.Verified).FirstOrDefault();
                    if (productConstruction != null)
                    {
                        productConstruction.IdcActive = RegisterSituation.NonActive;
                        ProductConstructionRepository.Update(productConstruction);
                    }
                }
                entity.ArrivedDate = entity.EstimationDate;
          
            }
            if (entity.VerifiedResult.Equals(RegisterSituation.NonActive) && productConstructions.Count > 0)
            {
                foreach (var orderProduct in entity.OrderProducts)
                {
                    double quantity = Math.Round((double)(orderProduct.Amount - orderProduct.AmountVerified.Value), 2);

                    ProductConstructionRepository.OrderVerify(orderProduct.CodeProduct, quantity, orderProduct.CodeOrder, orderProduct.DecimalPoint);
                     
                }
                
            }
            OrderRepository.Update(entity);
        }

    }
}
