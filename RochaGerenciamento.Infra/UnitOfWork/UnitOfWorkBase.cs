﻿using RochaGerenciamento.Infra.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Domain.Interfaces.UnitOfWork
{
    public class UnitOfWorkBase : IUnitOfWorkBase
    {
        protected Context Db = new Context();

        public UnitOfWorkBase(Context dataBaseContext)
        {
            Db = dataBaseContext;
        }

        public void Commit()
        {
            Db.SaveChanges();
        }
    }
}
