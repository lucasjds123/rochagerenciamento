﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class ProductConstructionConfiguration : EntityTypeConfiguration<ProductConstruction>
    {
        public ProductConstructionConfiguration()
        {
            ToTable("product_construction");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_product_construction").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsOptional();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsOptional();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();
            Property(x => x.AvailabilityDate).HasColumnName("availability_date").IsOptional();

            Property(x => x.CodeConstruction).HasColumnName("cod_construction").IsOptional();
            Property(x => x.CodeProduct).HasColumnName("cod_product").IsRequired();
            Property(x => x.CodeSituation).HasColumnName("cod_situation").IsRequired();
            Property(x => x.CodeOrder).HasColumnName("cod_order").IsOptional();
            Property(x => x.CodeEmployee).HasColumnName("cod_employee").IsOptional();
            Property(x => x.Quantity).HasColumnName("quantity").IsOptional();


        }
    }
}
