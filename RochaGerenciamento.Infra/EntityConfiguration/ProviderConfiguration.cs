﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class ProviderConfiguration : EntityTypeConfiguration<Provider>
    {
        public ProviderConfiguration()
        {
            ToTable("provider");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_provider").IsRequired();
            Property(x => x.Name).HasColumnName("name").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();
            Property(x => x.Email).HasColumnName("email").IsOptional();

            Property(x => x.Phone).HasColumnName("phone").IsRequired();
            Property(x => x.Contact).HasColumnName("contact").IsOptional();
            Property(x => x.CodeMaterialType).HasColumnName("cod_material_type").IsRequired();
            Property(x => x.PSQ).HasColumnName("psq").IsRequired();

            Property(x => x.CodeAddress).HasColumnName("cod_address").IsOptional();


            HasMany(x => x.Products)
                .WithRequired(x => x.Provider)
                .HasForeignKey(x => x.CodeProvider);
        }
    }
}
