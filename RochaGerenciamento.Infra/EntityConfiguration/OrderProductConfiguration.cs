﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class OrderProductConfiguration: EntityTypeConfiguration<OrderProduct>
    {
        public OrderProductConfiguration()
        {
            ToTable("order_product");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_order_product").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.ValueUnit).HasColumnName("value_unit").IsRequired();
            Property(x => x.CodeOrder).HasColumnName("cod_order").IsRequired();
            Property(x => x.CodeProduct).HasColumnName("cod_product").IsRequired();
            Property(x => x.Amount).HasColumnName("amount").IsRequired();
            Property(x => x.AmountVerified).HasColumnName("amount_verified").IsOptional();
            Property(x => x.DecimalPoint).HasColumnName("decimal_point").IsOptional();

            
        }
    }
}
