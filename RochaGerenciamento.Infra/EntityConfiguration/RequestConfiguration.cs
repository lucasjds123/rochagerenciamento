﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class RequestConfiguration : EntityTypeConfiguration<Request>
    {
        public RequestConfiguration()
        {
            ToTable("request");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_request").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.Tittle).HasColumnName("tittle").IsOptional();
            Property(x => x.Message).HasColumnName("message").IsOptional();
            Property(x => x.NeedDate).HasColumnName("need_date").IsRequired();
            Property(x => x.Observation).HasColumnName("observation").IsOptional();

            HasMany(x => x.RequestProducts)
              .WithRequired(x => x.Request)
              .HasForeignKey(x => x.CodeRequest);
        }
    }
}
