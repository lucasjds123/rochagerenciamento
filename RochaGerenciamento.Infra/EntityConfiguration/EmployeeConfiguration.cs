﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class EmployeeConfiguration: EntityTypeConfiguration<Employee>
    {
        public EmployeeConfiguration()
        {
            ToTable("employee");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_employee").IsRequired();
            Property(x => x.Name).HasColumnName("name").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.BirthDate).HasColumnName("birth_date").IsOptional();
            Property(x => x.HiredDate).HasColumnName("hired_date").IsRequired();
            Property(x => x.CodePosition).HasColumnName("cod_position").IsRequired();
            Property(x => x.Password).HasColumnName("password").IsRequired();
            Property(x => x.ThirdParty).HasColumnName("third_party").IsOptional();
            Property(x => x.CodeEmployeeStatus).HasColumnName("cod_employee_status").IsOptional();

            HasMany(x => x.ProductConstructions)
                .WithOptional(x => x.Employee)
                .HasForeignKey(x => x.CodeEmployee);

            HasMany(x => x.TaskServicesChecker)
                .WithOptional(x => x.EmployeeChecker)
                .HasForeignKey(x => x.CodeEmployeeChecker);

            HasMany(x => x.TaskServicesFinisher)
                .WithOptional(x => x.EmployeeFinisher)
                .HasForeignKey(x => x.CodeEmployeeFinisher);

            HasMany(x => x.TaskServiceEmployees)
                .WithRequired(x => x.Employee)
                .HasForeignKey(x => x.CodeEmployee);
        }
    }
}
