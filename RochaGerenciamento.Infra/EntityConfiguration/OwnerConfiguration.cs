﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class OwnerConfiguration : EntityTypeConfiguration<Owner>
    {
        public OwnerConfiguration()
        {
            ToTable("owner");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_owner").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.CodeUnit).HasColumnName("cod_unit").IsRequired();
            Property(x => x.CodeClient).HasColumnName("cod_client").IsRequired();
            Property(x => x.CodeStatusPurchase).HasColumnName("cod_status_purchase").IsRequired();

            HasMany(x => x.CoOwners)
                .WithRequired(x => x.Owner)
                .HasForeignKey(x => x.CodeOwner);

            HasMany(x => x.Contracts)
                .WithRequired(x => x.Owner)
                .HasForeignKey(x => x.CodeOwner);

            
        }
    }
}
