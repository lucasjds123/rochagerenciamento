﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("user");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_user").IsRequired();
            Property(x => x.Name).HasColumnName("name").IsRequired();
            Property(x => x.Password).HasColumnName("password").IsRequired();
            Property(x => x.Login).HasColumnName("login").IsRequired();
            Property(x => x.Email).HasColumnName("email").IsRequired();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();
            Property(x => x.CodeProfile).HasColumnName("cod_profile").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsOptional();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();

            HasMany(x => x.EmployeeStatuss)
              .WithOptional(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.EmployeeStatuss)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.TaskServiceEmployees)
              .WithOptional(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.TaskServiceEmployees)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.TaskServices)
              .WithOptional(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.TaskServices)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);
            HasMany(x => x.TaskSituations)
              .WithOptional(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.TaskSituations)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.BuildingAreas)
               .WithOptional(x => x.User)
               .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.BuildingAreas)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Tasks)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Tasks)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Manufacturers)
                 .WithOptional(x => x.User)
                 .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Manufacturers)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Addresses)
             .WithOptional(x => x.User)
             .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Addresses)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.StatusPurchases)
             .WithOptional(x => x.User)
             .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.StatusPurchases)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.CoOwners)
             .WithOptional(x => x.User)
             .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.CoOwners)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Owners)
             .WithOptional(x => x.User)
             .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Owners)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Employees)
              .WithOptional(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Employees)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Positions)
              .WithOptional(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Positions)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.RequestProducts)
              .WithOptional(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.RequestProducts)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Situations)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Situations)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.ContractIncomings)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.ContractIncomings)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.IncomingTypes)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.IncomingTypes)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Contracts)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Contracts)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.ContractTypes)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.ContractTypes)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Clients)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Clients)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.ContactOrigins)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.ContactOrigins)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.CivilStatuss)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.CivilStatuss)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Units)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Units)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Localizations)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Localizations)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Expenditures)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Expenditures)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.ExpenditureTypes)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.ExpenditureTypes)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Installments)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Installments)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Bills)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Bills)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.ProdutConstructions)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.ProdutConstructions)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.OrderProducts)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.OrderProducts)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Orders)
              .WithRequired(x => x.User)
              .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Orders)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Statuss)
               .WithRequired(x => x.User)
               .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Statuss)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.SubServicePlans)
                .WithRequired(x => x.User)
                .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.SubServicePlans)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Projects)
                .WithRequired(x => x.User)
                .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Projects)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.SubServices)
                .WithRequired(x => x.User)
                .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.SubServices)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Services)
                .WithRequired(x => x.User)
                .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Services)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Requests)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Requests)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Products)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Products)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Profiles)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Profiles)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Providers)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Providers)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.MaterialTypes)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.MaterialTypes)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.UnitTypes)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.UnitTypes)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Plans)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Plans)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Constructions)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Constructions)
                .WithOptional(x => x.User)
                .HasForeignKey(x => x.CodeAlterationUser);

            HasMany(x => x.Users)
               .WithOptional(x => x.UsuarioAux)
               .HasForeignKey(x => x.CodeCreationUser);

            HasMany(x => x.Users)
                .WithOptional(x => x.UsuarioAux)
                .HasForeignKey(x => x.CodeAlterationUser);

        }

    }
}
