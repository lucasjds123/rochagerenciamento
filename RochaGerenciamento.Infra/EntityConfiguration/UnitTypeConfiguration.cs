﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class UnitTypeConfiguration : EntityTypeConfiguration<UnitType>
    {
        public UnitTypeConfiguration()
        {
            ToTable("unit_type");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_unit_type").IsRequired();
            Property(x => x.Name).HasColumnName("name").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.Description).HasColumnName("description").IsOptional();

            HasMany(x => x.Products)
                .WithRequired(x => x.UnitType)
                .HasForeignKey(x => x.CodeUnitType);

        }
    }
}
