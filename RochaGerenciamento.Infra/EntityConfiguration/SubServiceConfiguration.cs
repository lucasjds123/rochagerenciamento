﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class SubServiceConfiguration: EntityTypeConfiguration<SubService>
    {
        public SubServiceConfiguration()
        {
            ToTable("sub_service");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_sub_service").IsRequired();
            Property(x => x.Name).HasColumnName("name").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.CodeService).HasColumnName("cod_service").IsRequired();

            HasMany(x => x.SubServicePlans)
                .WithRequired(x => x.SubService)
                .HasForeignKey(x => x.CodeSubService);

            HasMany(x => x.Projects)
                .WithRequired(x => x.SubService)
                .HasForeignKey(x => x.CodeSubService);

        }
    }
}
