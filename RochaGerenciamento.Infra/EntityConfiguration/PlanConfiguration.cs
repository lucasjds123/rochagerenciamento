﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class PlanConfiguration : EntityTypeConfiguration<Plan>
    {
        public PlanConfiguration()
        {
            ToTable("plan");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_plan").IsRequired();
            Property(x => x.Name).HasColumnName("name").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.CodeConstruction).HasColumnName("cod_construction").IsRequired();
            Property(x => x.Deadline).HasColumnName("deadline").IsRequired();
            Property(x => x.ConclusionDate).HasColumnName("conclusion_date").IsOptional();
            Property(x => x.StartDate).HasColumnName("start_date").IsRequired();

            Property(x => x.EstimationValue).HasColumnName("estimation_value").IsRequired();
            Property(x => x.RealValue).HasColumnName("real_value").IsOptional();

            HasMany(x => x.SubServicePlans)
                .WithRequired(x => x.Plan)
                .HasForeignKey(x => x.CodePlan);
        }
    }
}
