﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class RequestProductConfiguration : EntityTypeConfiguration<RequestProduct>
    {
        public RequestProductConfiguration()
        {
            ToTable("request_product");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_request_product").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.CodeRequest).HasColumnName("cod_request").IsRequired();
            Property(x => x.CodeProduct).HasColumnName("cod_product").IsRequired();
            Property(x => x.Amount).HasColumnName("amount").IsRequired();
        }
    }
}
