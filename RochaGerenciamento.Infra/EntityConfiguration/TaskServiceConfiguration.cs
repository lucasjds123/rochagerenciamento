﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class TaskServiceConfiguration: EntityTypeConfiguration<TaskService>
    {
        public TaskServiceConfiguration()
        { 
            ToTable("task_service");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_task_service").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.CodeTask).HasColumnName("cod_task").IsRequired();
            Property(x => x.CodeTaskSituation).HasColumnName("cod_task_situation").IsRequired();
            Property(x => x.CodeConstruction).HasColumnName("cod_construction").IsRequired();
            Property(x => x.CodeBuildingArea).HasColumnName("cod_building_area").IsOptional();
            Property(x => x.CodeUnit).HasColumnName("cod_unit").IsOptional();
            Property(x => x.DateTimeStart).HasColumnName("date_time_start").IsOptional();
            Property(x => x.DateTimeFinish).HasColumnName("date_time_finish").IsOptional();
            Property(x => x.CodeEmployeeFinisher).HasColumnName("cod_employee_finisher").IsOptional();
            Property(x => x.CodeEmployeeChecker).HasColumnName("cod_employee_checker").IsOptional();
            Property(x => x.DateFinishAccording).HasColumnName("date_finish_according").IsOptional();
            Property(x => x.FlagAccording).HasColumnName("flag_according").IsOptional();
            Property(x => x.NumberTaskService).HasColumnName("number_task_service").IsOptional();
            Property(x => x.CurrentSituation).HasColumnName("current_situation").IsOptional();
            Property(x => x.Solution).HasColumnName("solution").IsOptional();
            Property(x => x.Description).HasColumnName("description").IsOptional();


            HasMany(x => x.TaskServiceEmployees)
                .WithRequired(x => x.TaskService)
                .HasForeignKey(x => x.CodeTaskService);
        }
    }
}
