﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class SituationConfiguration : EntityTypeConfiguration<Situation>
    {
        public SituationConfiguration()
        {
            ToTable("situation");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_situation").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.Name).HasColumnName("name").IsOptional();

            HasMany(x => x.ProductConstructions)
               .WithRequired(x => x.Situation)
               .HasForeignKey(x => x.CodeSituation);

        }
    }
}
