﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class TaskServiceEmployeeConfiguration: EntityTypeConfiguration<TaskServiceEmployee>
    {
        public TaskServiceEmployeeConfiguration()
        { 
            ToTable("task_service_employee");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_task_service_employee").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.CodeEmployee).HasColumnName("cod_employee").IsRequired();
            Property(x => x.CodeTaskService).HasColumnName("cod_task_service").IsRequired();
        }
    }
}
