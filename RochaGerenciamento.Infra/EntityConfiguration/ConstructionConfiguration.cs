﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class ConstructionConfiguration : EntityTypeConfiguration<Construction>
    {
        public ConstructionConfiguration()
        {
            ToTable("construction");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_construction").IsRequired();
            Property(x => x.Name).HasColumnName("name").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.CodeAddress).HasColumnName("cod_address").IsOptional();

            HasMany(x => x.TaskServices)
             .WithRequired(x => x.Construction)
             .HasForeignKey(x => x.CodeConstruction);

            HasMany(x => x.BuildingAreas)
             .WithRequired(x => x.Construction)
             .HasForeignKey(x => x.CodeConstruction);

            HasMany(x => x.Units)
              .WithRequired(x => x.Construction)
              .HasForeignKey(x => x.CodeConstruction);

            HasMany(x => x.Expenditures)
              .WithRequired(x => x.Construction)
              .HasForeignKey(x => x.CodeConstruction);

            HasMany(x => x.Plans)
                .WithRequired(x => x.Construction)
                .HasForeignKey(x => x.CodeConstruction);

            HasMany(x => x.Orders)
                .WithRequired(x => x.Construction)
                .HasForeignKey(x => x.CodeConstructionDestination);

            HasMany(x => x.ProductConstructions)
                .WithOptional(x => x.Construction)
                .HasForeignKey(x => x.CodeConstruction);
        }
    }
}
