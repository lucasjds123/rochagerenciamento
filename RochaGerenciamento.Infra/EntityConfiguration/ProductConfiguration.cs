﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class ProductConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {
            ToTable("product");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_product").IsRequired();
            Property(x => x.Name).HasColumnName("name").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.Recurrent).HasColumnName("recurrent").IsOptional();
            Property(x => x.Limit).HasColumnName("limit").IsRequired();
            Property(x => x.AmountPerUnit).HasColumnName("amount_per_unit").IsRequired();
            Property(x => x.ValueUnit).HasColumnName("value_unit").IsRequired();
            Property(x => x.CodeUnitType).HasColumnName("cod_unit_type").IsRequired();
            Property(x => x.CodeProvider).HasColumnName("cod_provider").IsRequired();
            Property(x => x.Specs).HasColumnName("specs").IsOptional();
            Property(x => x.CodeManufacturer).HasColumnName("cod_manufacturer").IsOptional();

            HasMany(x => x.RequestProducts)
              .WithRequired(x => x.Product)
              .HasForeignKey(x => x.CodeProduct);

            HasMany(x => x.OrderProducts)
                .WithRequired(x => x.Product)
                .HasForeignKey(x => x.CodeProduct);

            HasMany(x => x.ProductConstructions)
                .WithRequired(x => x.Product)
                .HasForeignKey(x => x.CodeProduct);
        }
    }
}
