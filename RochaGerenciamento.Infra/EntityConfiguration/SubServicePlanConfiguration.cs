﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class SubServicePlanConfiguration : EntityTypeConfiguration<SubServicePlan>
    {
        public SubServicePlanConfiguration()
        {
            ToTable("sub_service_plan");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_sub_service_plan").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.TotalValue).HasColumnName("total_value").IsRequired();
            Property(x => x.MeasuraUnit).HasColumnName("measure_unit").IsRequired();
            Property(x => x.AmountRepetition).HasColumnName("amount_repetition").IsRequired();
            Property(x => x.Percent).HasColumnName("percent").IsRequired();
            Property(x => x.CodeStatus).HasColumnName("cod_status").IsRequired();
            Property(x => x.DateStart).HasColumnName("date_start").IsOptional();
            Property(x => x.DateStop).HasColumnName("date_stop").IsOptional();
            Property(x => x.DateRefresh).HasColumnName("date_refresh").IsOptional();
            Property(x => x.DateFinish).HasColumnName("date_finish").IsOptional();
            Property(x => x.EstimationInDays).HasColumnName("estimation_in_days").IsRequired();
            Property(x => x.CodePlan).HasColumnName("cod_plan").IsRequired();
            Property(x => x.CodeSubService).HasColumnName("cod_sub_service").IsRequired();
            Property(x => x.CodeSubServicePlanDepen).HasColumnName("cod_sub_service_plan_depen").IsOptional();
            Property(x => x.Order).HasColumnName("order").IsRequired();

            HasMany(x => x.SubServicePlans)
                .WithOptional(x => x.SubServicePlanAux)
                .HasForeignKey(x => x.CodeSubServicePlanDepen);

        }
    }
}
