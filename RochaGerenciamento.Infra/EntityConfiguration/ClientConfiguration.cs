﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class ClientConfiguration: EntityTypeConfiguration<Client>
    {
        public ClientConfiguration()
        {
            ToTable("client");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_client").IsRequired();
            Property(x => x.Name).HasColumnName("name").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.Phone).HasColumnName("phone").IsOptional();
            Property(x => x.CPF).HasColumnName("cpf").IsOptional();
            Property(x => x.RG).HasColumnName("rg").IsOptional();
            Property(x => x.CodeAddress).HasColumnName("cod_address").IsOptional();
          
            Property(x => x.Email).HasColumnName("email").IsOptional();
            Property(x => x.Observation).HasColumnName("observation").IsOptional();
            Property(x => x.Income).HasColumnName("income").IsOptional();
            Property(x => x.BirthDate).HasColumnName("birthdate").IsOptional();
            Property(x => x.AttendanceDate).HasColumnName("attendance_date").IsOptional();
            Property(x => x.CodeContactOrigin).HasColumnName("cod_contact_origin").IsOptional();
            Property(x => x.CodeCivilStatus).HasColumnName("cod_civil_status").IsOptional();
            Property(x => x.LookingFor).HasColumnName("looking_for").IsOptional();
            Property(x => x.JobTitle).HasColumnName("job_title").IsOptional();

            HasMany(x => x.Owners)
                .WithRequired(x => x.Client)
                .HasForeignKey(x => x.CodeClient);

            HasMany(x => x.CoOwners)
                .WithRequired(x => x.Client)
                .HasForeignKey(x => x.CodeClient);


        }
    }
}
