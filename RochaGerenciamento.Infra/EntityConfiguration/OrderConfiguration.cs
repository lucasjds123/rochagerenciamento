﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class OrderConfiguration: EntityTypeConfiguration<Order>
    {
        public OrderConfiguration()
        {
            ToTable("order");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_order").IsRequired();
            Property(x => x.Name).HasColumnName("name").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.CodeBill).HasColumnName("cod_bill").IsOptional();
            Property(x => x.CodeConstructionDestination).HasColumnName("cod_construction_destination").IsRequired();
            Property(x => x.TotalValue).HasColumnName("total_value").IsRequired();
            Property(x => x.Approval).HasColumnName("approval").IsOptional();
            Property(x => x.EstimationDate).HasColumnName("estimation_date").IsOptional();
            Property(x => x.NumberOrder).HasColumnName("number_order").IsRequired();
            Property(x => x.Discount).HasColumnName("discount").IsOptional();
            Property(x => x.TotalValueSale).HasColumnName("total_value_sale").IsRequired();
            Property(x => x.VerifiedResult).HasColumnName("verified_result").IsOptional();
            Property(x => x.ArrivedDate).HasColumnName("arrived_date").IsOptional();
            Property(x => x.ShippingCost).HasColumnName("shipping_cost").IsOptional();
            Property(x => x.LaborCost).HasColumnName("labor_cost").IsOptional();
            Property(x => x.Invoice).HasColumnName("invoice").IsOptional();

            HasMany(x => x.OrderProducts)
                .WithRequired(x => x.Order)
                .HasForeignKey(x => x.CodeOrder);

            HasMany(x => x.ProductConstructions)
               .WithOptional(x => x.Order)
               .HasForeignKey(x => x.CodeOrder);
        }
    }
}
