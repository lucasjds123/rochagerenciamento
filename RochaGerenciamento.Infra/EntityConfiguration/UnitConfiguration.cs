﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class UnitConfiguration: EntityTypeConfiguration<Unit>
    {
        public UnitConfiguration()
        {
            ToTable("unit");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_unit").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.Name).HasColumnName("name").IsRequired();
            Property(x => x.Number).HasColumnName("number").IsRequired();
            Property(x => x.Block).HasColumnName("block").IsOptional();
            Property(x => x.CommonSize).HasColumnName("common_size").IsOptional();
            Property(x => x.OtherAreasSize).HasColumnName("other_areas_size").IsOptional();
            Property(x => x.PrivaceSize).HasColumnName("privace_size").IsOptional();
            Property(x => x.TotalSize).HasColumnName("total_size").IsOptional();
            Property(x => x.CodeConstruction).HasColumnName("cod_construction").IsRequired();
            Property(x => x.CodeLocalization).HasColumnName("cod_localization").IsRequired();
            Property(x => x.Floor).HasColumnName("floor").IsOptional();

            HasMany(x => x.Owners)
              .WithRequired(x => x.Unit)
              .HasForeignKey(x => x.CodeUnit);

            HasMany(x => x.TaskServices)
              .WithOptional(x => x.Unit)
              .HasForeignKey(x => x.CodeUnit);
        }
    }
}
