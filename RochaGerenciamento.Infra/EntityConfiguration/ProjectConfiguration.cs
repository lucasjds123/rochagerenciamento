﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class ProjectConfiguration: EntityTypeConfiguration<Project>
    {
        public ProjectConfiguration()
        {
            ToTable("project");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_project").IsRequired();
            Property(x => x.Name).HasColumnName("name").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsOptional();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();
            Property(x => x.Value).HasColumnName("value").IsOptional();

            Property(x => x.CodeSubService).HasColumnName("cod_sub_service").IsRequired();
            

        }
    }
}
