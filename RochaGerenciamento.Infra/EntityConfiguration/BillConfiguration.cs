﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class BillConfiguration: EntityTypeConfiguration<Bill>
    {
        public BillConfiguration()
        {
            ToTable("bill");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_bill").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.TotalValue).HasColumnName("total_value").IsRequired();
            Property(x => x.Checked).HasColumnName("checked").IsOptional();
            Property(x => x.Barcode).HasColumnName("barcode").IsOptional();
            Property(x => x.Bank).HasColumnName("bank").IsOptional();
            Property(x => x.EmissionDate).HasColumnName("emission_date").IsRequired();
            
            HasMany(x => x.Orders)
                .WithOptional(x => x.Bill)
                .HasForeignKey(x => x.CodeBill);

            HasMany(x => x.Installments)
                .WithRequired(x => x.Bill)
                .HasForeignKey(x => x.CodeBill);

            HasMany(x => x.ContractIncomings)
              .WithRequired(x => x.Bill)
              .HasForeignKey(x => x.CodeBill);
        }
    }
}
