﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    class ExpenditureConfiguration: EntityTypeConfiguration<Expenditure>
    {
        public ExpenditureConfiguration()
        {
            ToTable("expenditure");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_expenditure").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.PurchaseDate).HasColumnName("purchase_date").IsRequired();
            Property(x => x.Description).HasColumnName("description").IsOptional();
            Property(x => x.Observation).HasColumnName("observation").IsOptional();
            Property(x => x.Value).HasColumnName("value").IsRequired();
            Property(x => x.CodeConstruction).HasColumnName("cod_construction").IsRequired();
            Property(x => x.CodeExpenditureType).HasColumnName("cod_expenditure_type").IsRequired();

        }
    }
}
