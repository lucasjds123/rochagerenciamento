﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class AddressConfiguration: EntityTypeConfiguration<Address>
    {
        public AddressConfiguration()
        { 
            ToTable("address");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_address").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.Name).HasColumnName("name").IsOptional();
            Property(x => x.City).HasColumnName("city").IsOptional();
            Property(x => x.Borough).HasColumnName("borough").IsOptional();
            Property(x => x.State).HasColumnName("state").IsOptional();
            Property(x => x.Number).HasColumnName("number").IsOptional();

            HasMany(x => x.Providers)
               .WithRequired(x => x.Address)
               .HasForeignKey(x => x.CodeAddress);

            HasMany(x => x.Clients)
               .WithRequired(x => x.Address)
               .HasForeignKey(x => x.CodeAddress);

            HasMany(x => x.Constructions)
               .WithRequired(x => x.Address)
               .HasForeignKey(x => x.CodeAddress);
        }

    }
}
