﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class InstallmentConfiguration: EntityTypeConfiguration<Installment>
    {
        public InstallmentConfiguration()
        {
            ToTable("installment");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_installment").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.Value).HasColumnName("value").IsRequired();
            Property(x => x.Checked).HasColumnName("checked").IsOptional();
            Property(x => x.DueDate).HasColumnName("duedate").IsOptional();
            Property(x => x.Sequence).HasColumnName("sequence").IsOptional();
            Property(x => x.CodeBill).HasColumnName("cod_bill").IsRequired();
            Property(x => x.PaymentDate).HasColumnName("payment_date").IsOptional();


            
        }
    }
}
