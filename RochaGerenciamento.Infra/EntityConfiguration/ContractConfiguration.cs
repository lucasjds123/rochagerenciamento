﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.EntityConfiguration
{
    public class ContractConfiguration: EntityTypeConfiguration<Contract>
    {
        public ContractConfiguration()
        {
            ToTable("contract");
            HasKey(x => x.Code);

            Property(x => x.Code).HasColumnName("cod_contract").IsRequired();
            Property(x => x.Name).HasColumnName("name").IsRequired();
            Property(x => x.CodeCreationUser).HasColumnName("cod_creation_user").IsRequired();
            Property(x => x.CodeAlterationUser).HasColumnName("cod_alteration_user").IsOptional();
            Property(x => x.CreationDate).HasColumnName("creation_date").IsRequired();
            Property(x => x.AlterationDate).HasColumnName("alteration_date").IsOptional();
            Property(x => x.IdcActive).HasColumnName("idc_active").IsOptional();

            Property(x => x.AgreementDate).HasColumnName("agreement_date").IsRequired();
            Property(x => x.WitnessName).HasColumnName("witness_name").IsOptional();
            Property(x => x.WitnessCPF).HasColumnName("witness_cpf").IsOptional();
            Property(x => x.CodeContractType).HasColumnName("cod_contract_type").IsRequired();
            Property(x => x.CodeOwner).HasColumnName("cod_owner").IsRequired();

            HasMany(x => x.ContractIncomings)
              .WithRequired(x => x.Contract)
              .HasForeignKey(x => x.CodeContract);

            HasMany(x => x.CoOwners)
              .WithRequired(x => x.Contract)
              .HasForeignKey(x => x.CodeContract);
        }
    }
}
