﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.EntityConfiguration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Contexto
{
    public class Context : DbContext
    {
        public Context()
            : base("STRING_CONNECTION")
        {
            Database.SetInitializer<Context>(null);
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<Construction> Constructions { get; set; }
        public virtual DbSet<Plan> Plans { get; set; }
        public virtual DbSet<UnitType> UnitTypes { get; set; }
        public virtual DbSet<MaterialType> MaterialTypes { get; set; }
        public virtual DbSet<Provider> Providers { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Request> Requests { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<SubService> SubServices { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<SubServicePlan> SubServicePlans { get; set; }
        public virtual DbSet<Status> Statuss { get; set; }
        public virtual DbSet<OrderProduct> OrderProducts { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<ProductConstruction> ProductConstructions { get; set; }
        public virtual DbSet<Situation> Situations { get; set; }
        public virtual DbSet<Bill> Bills { get; set; }
        public virtual DbSet<Installment> Installments { get; set; }
        public virtual DbSet<ExpenditureType> ExpenditureTypes { get; set; }
        public virtual DbSet<Expenditure> Expenditures { get; set; }
        public virtual DbSet<Localization> Localizations { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<CivilStatus> CivilStatuss { get; set; }
        public virtual DbSet<ContactOrigin> ContactOrigins { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<ContractType> ContractTypes { get; set; }
        public virtual DbSet<Contract> Contracts { get; set; }
        public virtual DbSet<IncomingType> IncomingTypes { get; set; }
        public virtual DbSet<ContractIncoming> ContractIncomings { get; set; }
        public virtual DbSet<RequestProduct> RequestProducts { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Position> Positions { get; set; }
        public virtual DbSet<Owner> Owners { get; set; }
        public virtual DbSet<CoOwner> CoOwners { get; set; }
        public virtual DbSet<StatusPurchase> StatusPurchases { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Manufacturer> Manufacturers { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<BuildingArea> BuildingAreas { get; set; }
        public virtual DbSet<TaskSituation> TaskSituations { get; set; }
        public virtual DbSet<TaskService> TaskServices { get; set; }
        public virtual DbSet<TaskServiceEmployee> TaskServiceEmployees { get; set; }
        public virtual DbSet<EmployeeStatus> EmployeeStatuss { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new ProfileConfiguration());
            modelBuilder.Configurations.Add(new ConstructionConfiguration());
            modelBuilder.Configurations.Add(new PlanConfiguration());
            modelBuilder.Configurations.Add(new UnitTypeConfiguration());
            modelBuilder.Configurations.Add(new MaterialTypeConfiguration());
            modelBuilder.Configurations.Add(new ProviderConfiguration());
            modelBuilder.Configurations.Add(new ProductConfiguration());
            modelBuilder.Configurations.Add(new RequestConfiguration());
            modelBuilder.Configurations.Add(new ServiceConfiguration());
            modelBuilder.Configurations.Add(new SubServiceConfiguration());
            modelBuilder.Configurations.Add(new ProjectConfiguration());
            modelBuilder.Configurations.Add(new SubServicePlanConfiguration());
            modelBuilder.Configurations.Add(new StatusConfiguration());
            modelBuilder.Configurations.Add(new OrderProductConfiguration());
            modelBuilder.Configurations.Add(new OrderConfiguration());
            modelBuilder.Configurations.Add(new ProductConstructionConfiguration());
            modelBuilder.Configurations.Add(new SituationConfiguration());
            modelBuilder.Configurations.Add(new BillConfiguration());
            modelBuilder.Configurations.Add(new InstallmentConfiguration());
            modelBuilder.Configurations.Add(new ExpenditureTypeConfiguration());
            modelBuilder.Configurations.Add(new ExpenditureConfiguration());
            modelBuilder.Configurations.Add(new LocalizationConfiguration());
            modelBuilder.Configurations.Add(new UnitConfiguration());
            modelBuilder.Configurations.Add(new CivilStatusConfiguration());
            modelBuilder.Configurations.Add(new ContactOriginConfiguration());
            modelBuilder.Configurations.Add(new ClientConfiguration());
            modelBuilder.Configurations.Add(new ContractTypeConfiguration());
            modelBuilder.Configurations.Add(new ContractConfiguration());
            modelBuilder.Configurations.Add(new IncomingTypeConfiguration());
            modelBuilder.Configurations.Add(new ContractIncomingConfiguration());
            modelBuilder.Configurations.Add(new RequestProductConfiguration());
            modelBuilder.Configurations.Add(new PositionConfiguration());
            modelBuilder.Configurations.Add(new EmployeeConfiguration());
            modelBuilder.Configurations.Add(new OwnerConfiguration());
            modelBuilder.Configurations.Add(new CoOwnerConfiguration());
            modelBuilder.Configurations.Add(new StatusPurchaseConfiguration());
            modelBuilder.Configurations.Add(new AddressConfiguration());
            modelBuilder.Configurations.Add(new ManufacturerConfiguration());
            modelBuilder.Configurations.Add(new TaskConfiguration());
            modelBuilder.Configurations.Add(new BuildingAreaConfiguration());
            modelBuilder.Configurations.Add(new TaskSituationConfiguration());
            modelBuilder.Configurations.Add(new TaskServiceConfiguration());
            modelBuilder.Configurations.Add(new TaskServiceEmployeeConfiguration());
            modelBuilder.Configurations.Add(new EmployeeStatusConfiguration());

            //modelBuilder.Properties<string>()
            //    .Configure(p => p.HasColumnType("varchar"));
            //modelBuilder.Properties<string>()
            //    .Configure(p => p.HasMaxLength(100));

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty(GeneralField.CreationDate) != null))
            {


                if (entry.State == EntityState.Added)
                {
                    entry.Property(GeneralField.CreationDate).CurrentValue = DateTime.Now;
                    entry.Property(GeneralField.CodeCreationUser).CurrentValue = SessionUser.CodeUser;
                    entry.Property(GeneralField.Active).CurrentValue = RegisterSituation.Active;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property(GeneralField.AlterationDate).CurrentValue = DateTime.Now;
                    entry.Property(GeneralField.CodeAlterationUser).CurrentValue = SessionUser.CodeUser;
                }

                
            }
            try
            {
                return base.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }

        }
    }
}
