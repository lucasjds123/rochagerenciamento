﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Repositories
{
    public class ContractIncomingRepository: RepositoryBase<ContractIncoming>, IContractIncomingRepository
    {
        public ContractIncomingRepository(Context context)
            : base(context)
        {
        }

        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {

            return Db.Set<ContractIncoming>().ToList()
                    .Where(x => 
                         x.IdcActive.ToString().Equals(RegisterSituation.Active) 
                        && (Session.CodeConstruction != 0 ? x.Contract.Owner.Unit.Construction.Code == Session.CodeConstruction : true))
                    .Select(m => new
                    {
                        Code = m.Code,
                        Construction = m.Contract.Owner.Unit.Construction.Name,
                        Contract = m.Contract.Name,
                        IncomeType = m.IncomingType.Name,
                        TotalValue = m.Bill.TotalValue,
                        CodeBill = m.CodeBill,
                        Checked = m.Bill.Checked.Equals(RegisterSituation.Active) ? RegisterSituation.Yes : RegisterSituation.No,
                        AmountInstallments = m.Bill.Installments.Where(b => b.IdcActive.Equals(RegisterSituation.Active)).Count(),
                        AmountInstallmentsPaid = m.Bill.Installments.Where(a => a.Checked.Equals(RegisterSituation.Active) && a.IdcActive.Equals(RegisterSituation.Active)).Count(),
                    }).ToList();
        }

        
    }
}
