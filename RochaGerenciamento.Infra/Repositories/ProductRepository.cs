﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.StoredProcedures;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Repositories
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(Context context)
            : base(context)
        {
        }

        public Product FindBySpecs(string name, int code =0){
            var test = (Db.Set<Product>().ToList().Where(x => (code > 0 ? x.Code != code : true) && x.Specs != null && x.Specs.ToLower() == name.ToLower())).FirstOrDefault();
            return test;
        }

        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            return Db.Set<Product>().ToList()
                    .Where(x => (
                                ArrangeString.RemoveDiacritics(x.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Provider.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.UnitType.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                (x.Manufacturer != null ? ArrangeString.RemoveDiacritics(x.Manufacturer.Name.ToString().ToLower()).Contains(searchPhrase) : false)
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active))
                    .Select(m => new
                    {
                        UnitType = m.UnitType.Name,
                        Provider = m.Provider.Name,
                        Name = m.Name,
                        Recurrent = m.Recurrent == RegisterSituation.Active ? RegisterSituation.Yes : RegisterSituation.No,
                        Limit = m.Limit,
                        AmountPerUnit = m.AmountPerUnit,
                        ValueUnit = m.ValueUnit,
                        Code = m.Code,
                        Specs = m.Specs,
                        Manufacturer = m.Manufacturer != null ? m.Manufacturer.Name : string.Empty

                    }).ToList();
        }

        public IEnumerable<object> ProductList()
        {
            return Db.Set<Product>().ToList().Where(x => x.IdcActive.Equals(RegisterSituation.Active)).Select(x => new { Code = x.Code, ProductValue = x.Name + " | " + x.Provider.Name + " | " + (( x.Manufacturer == null) ? "Sem Fabricante" : x.Manufacturer.Name) + " | R$:" + x.ValueUnit }).ToList();
        }

        public IEnumerable<object> DropDownList()
        {
            return Db.Database.SqlQuery<DropDown>("select name as Name, cod_product as Code  from product where idc_active = 1 group by name").Select(x => new { x.Name, x.Code }).ToList();
        }
    }
}
