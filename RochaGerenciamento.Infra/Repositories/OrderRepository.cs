﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.StoredProcedures;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace RochaGerenciamento.Infra.Repositories
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(Context context)
            : base(context)
        {
            //other stuff here
        }

        public override void Update(Order obj)
        {
            var attached = Db.Set<Order>().Find(obj.GetType().GetProperty(GeneralField.Code).GetValue(obj, null));
            if (attached != null) { Db.Entry(attached).State = EntityState.Detached; }
            Db.Entry(obj).State = EntityState.Modified;
            foreach (var orderProduct in obj.OrderProducts)
                Db.Entry(orderProduct).State = EntityState.Modified;
            
            Db.SaveChanges();
        }

        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            return Db.Set<Order>().ToList()
                    .Where(x => (ArrangeString.RemoveDiacritics(x.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Construction.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                x.CreationDate.ToString().ToLower().Contains(searchPhrase) ||
                                x.TotalValue.ToString().ToLower().Contains(searchPhrase) ||
                                x.EstimationDate.ToString().ToLower().Contains(searchPhrase) ||
                                x.NumberOrder.ToString().ToLower().Contains(searchPhrase) 
                        )
                        && (code > 0 ? x.CodeBill == code : true)
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) && (Session.CodeConstruction != 0 ? Session.CodeConstruction == x.Construction.Code : true))
                    .Select(m => new
                    {
                        Name = m.Name,
                        TotalValue = string.Format("{0:N2}", m.TotalValue, 2),
                        Construction = m.Construction.Name,
                        EstimationDate = m.EstimationDate != null ? m.EstimationDate.Value.GetDateTimeFormats('D')[0] : string.Empty,
                        CreationDate = m.CreationDate.GetDateTimeFormats('D')[0],
                        NumberService = m.NumberOrder,
                        TotalValueSale = m.TotalValueSale,
                        Discount = m.Discount,
                        Approval = m.Approval.Equals(RegisterSituation.Active) ? RegisterSituation.Yes : RegisterSituation.No,
                        Code = m.Code,
                        Verified = m.VerifiedResult,
                        Recurrent = m.OrderProducts.Where(c => c.Product.Recurrent.Equals(RegisterSituation.NonActive)).Count() > 0 ? RegisterSituation.No : RegisterSituation.Yes

                    }).ToList();
        }

        public IEnumerable<object> GetDynamicList(string searchPhrase, string nameConstruction, string numberOrder, string nameProvider)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());

            return Db.Database.SqlQuery<ListOrder>("CALL list_order_main('" + Session.CodeConstruction + "','" + numberOrder + "','" + nameConstruction + "','" + nameProvider + "' )").ToList()
                                       .Select(m => new
                                       {
                                           Name = m.Order,
                                           TotalValue = string.Format("{0:N2}", m.TotalValue, 2),
                                           Construction = m.Construction,
                                           EstimationDate = m.EstimationDate,
                                           CreationDate = m.CreationDate,
                                           NumberService = m.NumberOrder,
                                           TotalValueSale = m.TotalValueSale,
                                           Discount = m.Discount,
                                           Approval = m.Approval,
                                           Code = m.CodeOrder,
                                           Verified = m.VerifiedResult,
                                           Recurrent = m.Recurrent,
                                           Provider = m.Provider

                                       }).ToList();

        }

        public IEnumerable<object> OrderList()
        {
            return Db.Set<Order>().ToList().Where(x => x.IdcActive.Equals(RegisterSituation.Active) && (Session.CodeConstruction != 0 ? Session.CodeConstruction == x.Construction.Code : true) && x.CodeBill == null).Select(x => new { Code = x.Code, Name = x.Construction.Name + " | " + x.Name + " | " + x.NumberOrder + " | R$:" + x.TotalValue });
        }

        public string GetNextNumberOrder()
        {
            return Db.Database.SqlQuery<ListProductConstruction>("CALL get_next_number_order( )").ToList().FirstOrDefault().NumberOrder;
        }
        

        
    }
}
