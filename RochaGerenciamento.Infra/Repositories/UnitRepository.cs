﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace RochaGerenciamento.Infra.Repositories
{
    public class UnitRepository: RepositoryBase<Unit>, IUnitRepository
    {
        public UnitRepository(Context context)
            : base(context)
        {
            //other stuff here
        }
        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            return Db.Set<Unit>().ToList()
                .Where(x => ((x.Block != null ? ArrangeString.RemoveDiacritics(x.Block.ToString().ToLower()).Contains(searchPhrase) : false) ||
                                (x.CommonSize != null ? ArrangeString.RemoveDiacritics(x.CommonSize.ToString().ToLower()).Contains(searchPhrase) : false) ||
                                (x.PrivaceSize != null ? x.PrivaceSize.ToString().ToLower().Contains(searchPhrase) : false) ||
                                (x.TotalSize != null ? x.TotalSize.ToString().ToLower().Contains(searchPhrase) : false) ||
                                ArrangeString.RemoveDiacritics(x.Construction.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                x.Number.ToString().ToLower().Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Localization.Name.ToLower()).Contains(searchPhrase)
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) && (Session.CodeConstruction != 0 ? Session.CodeConstruction == x.Construction.Code : true)) 
                    .Select(m => new
                    {
                        Block = m.Block,
                        CommonSize = m.CommonSize,
                        Construction = m.Construction.Name,
                        PrivaceSize = m.PrivaceSize,
                        Localization = m.Localization.Name,
                        TotalSize = m.TotalSize,
                        Number = m.Number,
                        Name = m.Name,
                        Code = m.Code,
                        Floor = m.Floor
                    }).ToList();
        }

        public IEnumerable<object> DropDownList()
        {
            return Db.Set<Unit>().ToList()
                .Where(x => x.IdcActive.Equals(RegisterSituation.Active) && x.Owners.Where(y => y.IdcActive.Equals(RegisterSituation.Active) && y.CodeUnit == x.Code && (y.CodeStatusPurchase == 1 || y.CodeStatusPurchase == 2)).ToList().Count <= 0 )
                .Select(x => new { Code = x.Code, UnitValue = x.Name + " | " + x.Localization.Name + "|" + x.Number + " | " + x.Construction.Name }).ToList();
        }
    }
}
