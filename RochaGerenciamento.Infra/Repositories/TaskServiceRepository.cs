﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.StoredProcedures;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace RochaGerenciamento.Infra.Repositories
{
    public class TaskServiceRepository: RepositoryBase<TaskService>, ITaskServiceRepository
    {
        public TaskServiceRepository(Context context)
            : base(context)
        {
            //other stuff here
        }

        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            var list = Db.Database.SqlQuery<ListTaskService>("CALL list_task_service("+ code +")").ToList();
            return list.Select(m => new
                    {
                        Code = m.Code,
                        Construction = m.Construction,
                        Area = m.Area,
                        Situation = m.Situation,
                        Task = m.Task,
                        NumberTaskService = m.NumberTaskService,
                        DateTimeStart = m.DateTimeStart != null ? m.DateTimeStart.Value.GetDateTimeFormats('D')[0].ToString() : string.Empty,
                        DateTimeFinish = m.DateTimeFinish != null ? m.DateTimeFinish.Value.GetDateTimeFormats('D')[0].ToString() : string.Empty
                    });
        }

        public void GetNextNumberTask(int codTaskService, string dateTimeStart)
        {
            Db.Database.SqlQuery<ListTaskService>("CALL get_next_number_task('"+codTaskService+"','" +dateTimeStart+"')").ToList();
        }

       
    }
}
