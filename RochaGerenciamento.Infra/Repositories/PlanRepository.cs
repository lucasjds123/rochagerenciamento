﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace RochaGerenciamento.Infra.Repositories
{
    public class PlanRepository : RepositoryBase<Plan>, IPlanRepository
    {
        public PlanRepository(Context context)
            : base(context)
        {
            //other stuff here
        }
        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            return Db.Set<Plan>().ToList()
                    .Where(x => (
                                ArrangeString.RemoveDiacritics(x.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                x.Deadline.GetDateTimeFormats('D')[0].ToString().ToLower().Contains(searchPhrase) ||
                                (x.ConclusionDate != null ? x.ConclusionDate.GetValueOrDefault().GetDateTimeFormats('D')[0].ToString().ToLower().Contains(searchPhrase) : false ) ||
                                x.StartDate.GetDateTimeFormats('D')[0].ToString().ToLower().Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Construction.Name.ToString().ToLower()).Contains(searchPhrase)
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) && (Session.CodeConstruction != 0 ? Session.CodeConstruction == x.Construction.Code : true))
                    .Select(m => new
                    {
                        Deadline =  m.Deadline.GetDateTimeFormats('D')[0].ToString() ,
                        ConclusionDate = m.ConclusionDate != null ? m.ConclusionDate.GetValueOrDefault().GetDateTimeFormats('D')[0].ToString() : String.Empty,
                        StartDate = m.StartDate.GetDateTimeFormats('D')[0].ToString(),
                        Name = m.Name,
                        Code = m.Code,
                        RealValue = m.RealValue != null ? m.RealValue.ToString() : string.Empty,
                        EstimationValue = m.EstimationValue

                    }).ToList();
        }
    }
}
