﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Repositories
{
    public class RequestProductRepository : RepositoryBase<RequestProduct>, IRequestProductRepository
    {
        public RequestProductRepository(Context context)
            : base(context)
        {
        }

        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
   
            return Db.Set<RequestProduct>().ToList()
                    .Where(x => (
                                ArrangeString.RemoveDiacritics(x.Product.Name.ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Product.Provider.Name.ToLower()).Contains(searchPhrase)

                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) && (code > 0 ? code == x.CodeRequest : code > 0))
                    .Select(m => new
                    {
                        Product = m.Product.Name,
                        Provider = m.Product.Provider.Name,
                        Amount = m.Amount,


                    }).ToList();
        }
    }
}
