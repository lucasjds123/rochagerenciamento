﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace RochaGerenciamento.Infra.Repositories
{
    public class OwnerRepository: RepositoryBase<Owner>, IOwnerRepository
    {
        public OwnerRepository(Context context)
            : base(context)
        {
            //other stuff here
        }

        public override IEnumerable<object> GetDynamicList(string searchPhrase, int status)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            return Db.Set<Owner>().ToList()
                    .Where(x => (
                                ArrangeString.RemoveDiacritics(x.Client.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Unit.Name.ToString().ToLower()).Contains(searchPhrase)
                        ) && x.CodeStatusPurchase == status
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) && (Session.CodeConstruction != 0 ? Session.CodeConstruction == x.Unit.Construction.Code : true))
                    .Select(m => new
                    {
                        Code = m.Code,
                        Client = m.Client.Name,
                        Unit = m.Unit.Name,
                        Status = m.StatusPurchase.Name,
                        Construction = m.Unit.Construction.Name,

                    }).ToList();
        }

        public IEnumerable<object> DropDownList()
        {
            return Db.Set<Owner>().ToList().Where(x => x.IdcActive.Equals(RegisterSituation.Active) && x.StatusPurchase.Code == (int) (FlagStatusPurchase.Sold)).Select(x => new { Code = x.Code, OwnerValue = x.Client.Name + " | " + x.Unit.Name + "|" + x.Unit.Number + " | " + x.Unit.Construction.Name }).ToList();
        }

    }
}
