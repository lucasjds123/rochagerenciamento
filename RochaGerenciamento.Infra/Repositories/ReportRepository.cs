﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.StoredProcedures;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace RochaGerenciamento.Infra.Repositories
{
    public class ReportRepository :  IReportRepository
    {
        private Context Db = new Context();

        public ReportRepository(Context context)
        {
            Db = context;
        }

        public IEnumerable<object> Order(int approval, int codeConstruction, string firstDate, string lastDate)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            var list = Db.Database.SqlQuery<ListOrder>("CALL report_order('" + approval + "', '" + firstDate + "','" + lastDate + "','" + codeConstruction + "')").ToList();
            return list.Select(m => new
            {
                Construction = m.Construction,
                Order = m.Order,
                NumberOrder = m.NumberOrder,
                Approval = m.Approval,
                EstimationDate = m.EstimationDate,
                ProductAmount = m.ProductAmount,
                TotalValue = m.TotalValue,
                InstallmentAmount = m.InstallmentAmount,
                Discount = m.Discount,
                TotalValueSale = m.TotalValueSale
            });
        }

        public IEnumerable<object> Stock(int codeSituation, int codeConstruction, int codeProduct, int codeProvider, int codeUnitType, string firstDate, string lastDate)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            var list = Db.Database.SqlQuery<ListProductConstruction>("CALL report_stock('" + codeSituation + "', '" + codeConstruction + "','" + codeProduct + "','" + codeProvider + "','" + codeUnitType + "','" + firstDate + "','" + lastDate + "')").ToList();
            return list.Select(m => new
            {
                Construction = m.Construction,
                Product = m.Product,
                Amount = m.Amount,
                ValueUnit = m.ValueUnit,
                Provider = m.Provider,
                AvailabilityDate = m.AvailabilityDate != null ? m.AvailabilityDate.Value.GetDateTimeFormats('D')[0].ToString() : "",
                Situation = m.Situation,
                Limit = m.Limit,
                UnitType = m.UnitType,
                Recurrent = m.Recurrent,
                AmountPerUnit = m.AmountPerUnit,
                AlterationDate = m.AlterationDate != null ? m.AlterationDate.Value.GetDateTimeFormats('D')[0].ToString() : ""
            });
        }

        public IEnumerable<object> ExpenditureVsIncoming(int codeConstruction, string firstDate, string lastDate)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            var list = Db.Database.SqlQuery<ResultExpenditureIncoming>("CALL report_expenditure_vs_incoming('" + firstDate + "','" + lastDate + "','" + codeConstruction + "')").ToList();
            return list.Select(m => new
            {
                Expenditure = m.Expenditure,
                Incoming = m.Incoming,
                Result = m.Result
            });
        }

        public IEnumerable<object> Incoming(int codeConstruction, string firstDate, string lastDate)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            var list = Db.Database.SqlQuery<ListIncoming>("CALL report_incoming('" + firstDate + "','" + lastDate + "','" + codeConstruction + "')").ToList();
            return list.Select(m => new
            {
                Contract = m.Contract,
                ContractType = m.ContractType,
                AgreementDate = m.AgreementDate,
                Total = m.Total,
                Construction = m.Construction,
                Number = m.Number,
                Block = m.Block,
                Unit = m.Unit,
                Localization = m.Localization,
                Client = m.Client,
                Document = m.Document
            });
        }

        public IEnumerable<object> FilterOrder(int codOrder)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            var list = Db.Database.SqlQuery<FilterOrder>("CALL report_filter_order('" + codOrder + "')").ToList();
            return list.Select(m => new
            {
                Product = m.Product,
                UnitType = m.UnitType,
                Amount = m.Amount,
                ValueUnit = m.ValueUnit,
                Total = m.Total,
                Provider = m.Provider,
                NumOrder = m.NumOrder,
                Estimation = m.Estimation == null ? string.Empty : m.Estimation,
                Discount = m.Discount,
                TotalValueSale = m.TotalValueSale,
                Contact = m.Contact,
                Phone = m.Phone,
                Destination = m.Destination,
                Email = m.Email,
                LaborCost = m.LaborCost,
                ShippingCost = m.ShippingCost,
                Address = m.Address
            });
        }

        public IEnumerable<object> EmployeeVsProduct(int codEmployee, string firstDate, string lastDate)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            var list = Db.Database.SqlQuery<ListProductConstruction>("CALL report_employee_vs_product('" + codEmployee + "','" + firstDate + "','" + lastDate + "')").ToList();
            return list.Select(m => new
            {
                Product = m.Product,
                Amount = m.Amount,
                Employee = m.Employee,
                Construction = m.Construction,
                AlterationDate = m.AlterationDate.Value.GetDateTimeFormats('D')[0].ToString(),
                Day = m.AlterationDate.Value.Day.ToString(),
                Month = m.AlterationDate.Value.ToString("MMMM"),
                Year = m.AlterationDate.Value.Year.ToString()
            });
        }

        public IEnumerable<object> AttendanceDate(string year)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            var list = Db.Database.SqlQuery<ResultGraph>("CALL report_attendance_date_month(" + year + ")").ToList();
            return list.Select(m => new
            {
                Month = m.Label,
                Amount = m.Amount,
            });
        }

        public IEnumerable<object> ContactOrigin(string year, string month)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            var list = Db.Database.SqlQuery<ResultGraph>("CALL report_contact_origin(" + year + "," + month + ")").ToList();
            return list.Select(m => new
            {
                ContactOrigin = m.Label,
                Amount = m.Amount,
            });
        }

        public IEnumerable<object> OwnerPurchases(string year, string month)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            var list = Db.Database.SqlQuery<ResultGraph>("CALL report_owner_purchases('" + year + "','" + month + "')").ToList();
            return list.Select(m => new
            {
                StatusPurchase = m.Label,
                Amount = m.Amount,
            });
        }

        public IEnumerable<ListPurchasesContract> Clients()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            var list = Db.Database.SqlQuery<ListPurchasesContract>("CALL report_clients()").ToList();
            var list2 = list.Select(m => new ListPurchasesContract
            {
                Client = m.Client,
                ContactOrigin = m.ContactOrigin,
                Phone = m.Phone,
                AttendanceDate = m.AttendanceDate,
                Day = m.Day,
                Month = m.Month,
                Year = m.Year
            });
            return list2;
        }
    }
}
