﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Repositories
{
    public class BuildingAreaRepository: RepositoryBase<BuildingArea>, IBuildingAreaRepository
    {
        public BuildingAreaRepository(Context context)
            : base(context)
        {
        }

        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {

            return Db.Set<BuildingArea>().ToList()
                    .Where(x => (ArrangeString.RemoveDiacritics(x.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Construction.Name.ToString().ToLower()).Contains(searchPhrase) 
                            ) &&
                         x.IdcActive.ToString().Equals(RegisterSituation.Active) 
                       )
                    .Select(m => new
                    {
                        Code = m.Code,
                        Name = m.Name,
                        Construction = m.Construction.Name,
                    }).ToList();
        }

        
    }
}
