﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Infra.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Repositories
{
    public class AddressRepository : RepositoryBase<Address>, IAddressRepository
    {
        public AddressRepository(Context context)
            : base(context)
        {
            //other stuff here
        }

        public IEnumerable<object> AddressList()
        {
            return Db.Set<Address>().ToList().Where(x => x.IdcActive.Equals(RegisterSituation.Active)).Select(x => new { Code = x.Code, Name = x.Name + ", " + x.Number + " " + x.Borough + " " + x.City + " " + x.State });
        }
    }
}
