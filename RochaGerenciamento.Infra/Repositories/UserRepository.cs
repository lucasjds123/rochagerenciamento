﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;

namespace RochaGerenciamento.Infra.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(Context context)
            : base(context)
        {
            //other stuff here
        }
        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            return Db.Set<User>().ToList()
                    .Where(x => (ArrangeString.RemoveDiacritics(x.Login.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Email.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Profile.Name.ToString().ToLower()).Contains(searchPhrase)
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active))
                    .Select(m => new
                    {
                        Login = m.Login,
                        Name = m.Name,
                        Profile = m.Profile.Name,
                        Email = m.Email,
                        Code = m.Code

                    }).ToList();
        }

        public bool CheckUserExistence(string login)
        {
            return (Db.Set<User>().Where(x => x.Login == login && x.IdcActive.Equals(RegisterSituation.Active)).FirstOrDefault() != null);
        }

        public User LookForLoginPassword(string login, string password)
        {
            return Db.Set<User>().Where(x => x.Login == login && x.Password == password && x.IdcActive.Equals(RegisterSituation.Active)).FirstOrDefault();
        }

        public bool CheckUserWasRemoved(string login)
        {
            return (Db.Set<User>().Where(x => x.Login == login && x.IdcActive.Equals(RegisterSituation.NonActive)).FirstOrDefault() != null);
        }

        public User FindByLoginRemoved(string login)
        {
            return Db.Set<User>().Where(x => x.Login == login && x.IdcActive.Equals(RegisterSituation.NonActive)).FirstOrDefault();
        }

        public string GetEmailUserByProfile(string profile)
        {
            return Db.Set<User>().Where(x => x.Profile.Name.Equals(profile) && x.IdcActive.Equals(RegisterSituation.Active)).Select(x => x.Email).FirstOrDefault();
        }
    }
}
