﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Infra.Contexto;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace RochaGerenciamento.Infra.Repositories
{
    public class InstallmentRepository: RepositoryBase<Installment>, IInstallmentRepository
    {
        public InstallmentRepository(Context context)
            : base(context)
        {
        }

        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            return Db.Set<Installment>().ToList()
                    .Where(x => (x.Value.ToString().ToLower().Contains(searchPhrase) ||
                                x.DueDate.ToString().ToLower().Contains(searchPhrase) 
                                
                        )
                        && (code > 0 ? x.CodeBill == code : true)
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active))
                    .Select(m => new
                    {
                        Situation = (DateTime.Now.Date.AddDays(3) > m.DueDate && m.DueDate >= DateTime.Now.Date && m.PaymentDate == null) ? RegisterSituation.Orange :( (m.DueDate >= DateTime.Now.Date || m.PaymentDate <= m.DueDate) ? RegisterSituation.Green : RegisterSituation.Red),
                        DueDate = m.DueDate.GetDateTimeFormats('D')[0],
                        Checked = m.Checked.Equals(RegisterSituation.Active) ? RegisterSituation.Yes : RegisterSituation.No,
                        Value = m.Value,
                        Sequence = m.Sequence,
                        Code = m.Code,
                        PaymentDate = m.PaymentDate != null ? m.PaymentDate.Value.GetDateTimeFormats('D')[0] : ""


                    }).ToList();
        }

        public IEnumerable<object> InstallmentsAboutToExpire(int daysToExpire, int flagBillType)
        {
            return Db.Set<Installment>().ToList()
                .Where(x => x.IdcActive.Equals(RegisterSituation.Active) && (flagBillType == 1 ? x.Bill.Orders.Count() > 0 : x.Bill.ContractIncomings.Count() > 0)
                        && x.Bill.IdcActive.Equals(RegisterSituation.Active) && DateTime.Now.Date.AddDays(daysToExpire)>=(x.DueDate) && x.Checked.Equals(RegisterSituation.NonActive))
                    .Select(m => new
                    {
                        BarCode = m.Bill.Barcode,
                        OrderNumber = flagBillType == (int) FlagBillType.Order ? m.Bill.Orders.FirstOrDefault().NumberOrder.ToString() : "",
                        Construction = flagBillType == (int)FlagBillType.Order ? m.Bill.Orders.FirstOrDefault().Construction.Name : ""


                    }).ToList();
        }
    }
}
