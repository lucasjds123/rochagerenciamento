﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Repositories
{
    public class ProjectRepository : RepositoryBase<Project>, IProjectRepository
    {
        public ProjectRepository(Context context)
            : base(context)
        {
            //other stuff here
        }
        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            return Db.Set<Project>().ToList()
                    .Where(x => (
                                ArrangeString.RemoveDiacritics(x.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                x.Value.ToString().ToLower().Contains(searchPhrase)||
                                ArrangeString.RemoveDiacritics(x.SubService.Name.ToString().ToLower()).Contains(searchPhrase)
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active))
                    .Select(m => new
                    {
                        Name = m.Name,
                        Value = m.Value,
                        SubService = m.SubService.Name,
                        Code = m.Code

                    }).ToList();
        }
    }
}
