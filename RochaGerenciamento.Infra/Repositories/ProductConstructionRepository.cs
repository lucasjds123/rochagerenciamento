﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Infra.Contexto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Configuration;
using MySql.Data.MySqlClient;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.StoredProcedures;
using RochaGerenciamento.Domain.ValueObject;

namespace RochaGerenciamento.Infra.Repositories
{
    public class ProductConstructionRepository : RepositoryBase<ProductConstruction>, IProductConstructionRepository
    {
        public ProductConstructionRepository(Context context)
            : base(context)
        {
        }
        public override IEnumerable<object> GetDynamicList(string searchPhrase, int status)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            var list = Db.Database.SqlQuery<ListProductConstruction>("CALL list_product_construction(" + status + ", '" + searchPhrase + "'," + Session.CodeConstruction + ")").ToList();
            return list.Select(m => new
            {
                Product = m.Product,
                Amount = m.Amount,
                
                Provider = m.Provider,
                AvailabilityDate = m.AvailabilityDate != null ? m.AvailabilityDate.Value.GetDateTimeFormats('D')[0].ToString() : "",
                Situation = m.Situation,
                Code = m.Code,
                Construction = m.Construction,
                Limit = m.Limit,
                UnitType = m.UnitType, 
                Recurrent = m.Recurrent ,
                AmountPerUnit = m.AmountPerUnit,
                CheckDate = m.CheckDate,
                CodeConstruction = m.CodConstruction,
                CodeProduct = m.CodProduct,
                NumberOrder = m.NumberOrder,
                AlterationDate = m.AlterationDate != null ? m.AlterationDate.Value.GetDateTimeFormats('D')[0].ToString() : "",
                RawDate = m.AlterationDate != null ? m.AlterationDate.Value.ToString("yyyy-MM-dd") : "",
                Employee = m.Employee,
                CodeEmployee = m.CodeEmployee
            });
           
        }

        public IEnumerable<object> ProductsAboutToExpire()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            var list = Db.Database.SqlQuery<ListProductConstruction>("CALL list_product_construction(1, '',0)").ToList();
            list = list.Where(x => x.Limit == x.Amount).ToList();
            return list;
            
        }

        public void Update(int codSituation, int codProduct, int codConstruction, double amount, int codEmployee , DateTime? dataBaixa = null, int codeProductConstruction = 0)
        {
            string dt = dataBaixa != null ? dataBaixa.Value.ToString("yyyy-MM-dd") : string.Empty;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Db.Database.SqlQuery<ListProductConstruction>("CALL update_situation('" + codSituation + "' , '" + SessionUser.CodeUser + "','" + codProduct + "','" + codConstruction + "','" + amount + "','" + codEmployee + "','" + dt + "','" + codeProductConstruction + "')").ToList();
        }

        public void Add(int codeOrder, int? codeConstruction, string estimationDate )
        {
            Db.Database.SqlQuery<ListProductConstruction>("CALL order_approval('" + codeOrder + "', 'null' , '" + estimationDate + "', " + SessionUser.CodeUser + " )").ToList();
        }

        public double AmountInStock(int codeProductConstruction, int codProduct , int codSituation, double decimalPoint = 1)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            return Convert.ToDouble(Db.Database.SqlQuery<ListProductConstruction>("CALL amount_in_stock('" + codeProductConstruction + "' , '" + codProduct + "','"+codSituation+"','" + decimalPoint +  "' )").FirstOrDefault().Amount);
                    
        }

        public void OrderVerify(int codProduct, double amount, int codOrder, double decimalPoint)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Db.Database.SqlQuery<ListProductConstruction>("CALL order_verify('" + amount + "','" + codProduct + "','" + codOrder + "', '" + SessionUser.CodeUser + "','" + decimalPoint + "' )").ToList();

        }

        
    }
}
