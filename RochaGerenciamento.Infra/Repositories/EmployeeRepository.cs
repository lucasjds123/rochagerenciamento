﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Repositories
{
    public class EmployeeRepository : RepositoryBase<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(Context context)
            : base(context)
        {
            //other stuff here
        }

        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            return Db.Set<Employee>().ToList()
                    .Where(x => (
                                ArrangeString.RemoveDiacritics(x.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Position.Name.ToString().ToLower()).Contains(searchPhrase) 
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active))
                    .Select(m => new
                    {
                        Position = m.Position.Name,
                        Name = m.Name,
                        HiredDate = m.HiredDate.GetDateTimeFormats('D')[0],
                        Code = m.Code,
                        BirthDate = m.BirthDate.Value.GetDateTimeFormats('D')[0],
                        Status = m.EmployeeStatus.Name

                    }).ToList();
        }
    }
}
