﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Repositories
{
    public class ContractRepository : RepositoryBase<Contract>, IContractRepository
    {
        public ContractRepository(Context context)
            : base(context)
        {
        }
        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            return Db.Set<Contract>().ToList()
                    .Where(x => (
                                ArrangeString.RemoveDiacritics(x.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Owner.Client.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.ContractType.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Owner.Unit.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Owner.Unit.Construction.Name.ToString().ToLower()).Contains(searchPhrase)
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) )
                    .Select(m => new
                    {
                        Client = m.Owner.Client.Name,
                        Name = m.Name,
                        ContractType = m.ContractType.Name,
                        Unit = m.Owner.Unit.Name,
                        Construction = m.Owner.Unit.Construction.Name,
                        Code = m.Code

                    }).ToList();
        }
    }
}
