﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace RochaGerenciamento.Infra.Repositories
{
    public class SubServicePlanRepository : RepositoryBase<SubServicePlan>, ISubServicePlanRepository
    {
        public SubServicePlanRepository(Context context)
            : base(context)
        {
        }
        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            return Db.Set<SubServicePlan>().ToList()
                    .Where(x => (x.TotalValue.ToString().ToLower().Contains(searchPhrase) ||
                                x.MeasuraUnit.ToString().ToLower().Contains(searchPhrase) ||
                                x.AmountRepetition.ToString().ToLower().Contains(searchPhrase) ||
                                x.Percent.ToString().ToLower().Contains(searchPhrase) ||
                               
                                (x.DateStart != null ? x.DateStart.ToString().ToLower().Contains(searchPhrase) : false) ||
                                (x.DateStop != null ? x.DateStop.ToString().ToLower().Contains(searchPhrase) : false ) ||
                                (x.DateRefresh != null ? x.DateRefresh.ToString().ToLower().Contains(searchPhrase) : false ) ||
                                (x.DateFinish !=  null ? x.DateFinish.ToString().ToLower().Contains(searchPhrase) : false) ||
                                ArrangeString.RemoveDiacritics(x.Plan.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.SubService.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.SubService.Service.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Plan.Construction.Name.ToString().ToLower()).Contains(searchPhrase)
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) && (Session.CodeConstruction != 0 ? Session.CodeConstruction == x.Plan.Construction.Code : true))
                    .OrderBy( m => m.Order)
                    .Select(m => new
                    {
                        TotalValue = m.TotalValue,
                        MeasureUnit = m.MeasuraUnit,
                        AmountRepetition = m.AmountRepetition,
                        Percent = m.Percent,
                        DateStart = m.DateStart != null ? m.DateStart.Value.GetDateTimeFormats('D')[0].ToString() : string.Empty,
                        DateStop = m.DateStop != null ? m.DateStop.Value.GetDateTimeFormats('D')[0].ToString() : string.Empty,
                        DateRefresh = m.DateRefresh != null ? m.DateRefresh.Value.GetDateTimeFormats('D')[0].ToString() : string.Empty,
                        DateFinish = m.DateFinish != null ? m.DateFinish.Value.GetDateTimeFormats('D')[0].ToString() : string.Empty,
                        EstimationInDays = m.EstimationInDays,
                        Plan = m.Plan.Name,
                        SubService = m.SubService.Name,
                        Dependency = m.SubServicePlanAux != null ? Db.Set<SubServicePlan>().Where(j => j.Code == m.CodeSubServicePlanDepen).Select(j => j.Order).FirstOrDefault() : string.Empty,
                        Service = m.SubService.Service.Name,
                        Construction = m.Plan.Construction.Name,
                        Status = m.Status.Name,
                        Code = m.Code,
                        Order =  m.Order

                    }).ToList();
        }

        public IEnumerable<object> GetDynamicList(int status , string searchPhrase, string nameConstruction, string namePlan, string nameService, string nameSubService,
                                        string measureUnit, string amountRepetition, string percent, string estimation, string value)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            return Db.Set<SubServicePlan>().ToList()
                    .Where(x => (
                                (measureUnit != "" ? x.MeasuraUnit.ToString().ToLower().Contains(measureUnit.ToLower()) : false) ||
                                (amountRepetition != "" ? x.AmountRepetition.ToString().ToLower().Contains(amountRepetition.ToLower()) : false) ||
                                (percent != "" ? x.Percent.ToString().ToLower().Contains(percent.ToLower()) : false) ||
                                (namePlan != "" ? x.Plan.Name.ToString().ToLower().Contains(namePlan.ToLower()) : false) ||
                                (nameSubService != "" ? x.SubService.Name.ToString().ToLower().Contains(nameSubService.ToLower()) : false) ||
                                (nameService != "" ? x.SubService.Service.Name.ToString().ToLower().Contains(nameService.ToLower()) : false ) ||
                                (nameConstruction != "" ? x.Plan.Construction.Name.ToString().ToLower().Contains(nameConstruction.ToLower()) : false ) ||
                                (estimation != "" ? x.EstimationInDays.ToString().ToLower().Contains(estimation.ToLower()) : false) ||
                                (value != "" ? x.TotalValue.ToString().ToLower().Contains(value.ToLower()) : false)
                                || 
                                (measureUnit == "" && amountRepetition == "" && percent == "" && namePlan == "" &&
                                 nameSubService == "" && nameService == "" && nameConstruction == "" && estimation == "" && value == "" ? true : false)
                                
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) && x.CodeStatus == status && (Session.CodeConstruction != 0 ? Session.CodeConstruction == x.Plan.Construction.Code : true))
                    .OrderBy(m => m.Order)
                    .Select(m => new
                    {
                        TotalValue = m.TotalValue.ToString(),
                        MeasureUnit = m.MeasuraUnit,
                        AmountRepetition = m.AmountRepetition,
                        Percent = m.Percent.ToString(),
                        DateStart = m.DateStart != null ? m.DateStart.Value.GetDateTimeFormats('D')[0].ToString() : string.Empty,
                        DateStop = m.DateStop != null ? m.DateStop.Value.GetDateTimeFormats('D')[0].ToString() : string.Empty,
                        DateRefresh = m.DateRefresh != null ? m.DateRefresh.Value.GetDateTimeFormats('D')[0].ToString() : string.Empty,
                        DateFinish = m.DateFinish != null ? m.DateFinish.Value.GetDateTimeFormats('D')[0].ToString() : string.Empty,
                        EstimationInDays = m.EstimationInDays,
                        Plan = m.Plan.Name,
                        SubService = m.SubService.Name,
                        Dependency = m.SubServicePlanAux != null ? Db.Set<SubServicePlan>().Where(j => j.Code == m.CodeSubServicePlanDepen).Select(j => j.Order).FirstOrDefault() : string.Empty,
                        Service = m.SubService.Service.Name,
                        Construction = m.Plan.Construction.Name,
                        Status = m.Status.Name,
                        Code = m.Code,
                        Order =  m.Order,
                        Color = m.CodeStatus ==(int) FlagStatus.Doing || m.CodeStatus ==(int) FlagStatus.Done ? ((m.DateStart.Value.AddDays( m.EstimationInDays)) > DateTime.Now  ? RegisterSituation.Green : RegisterSituation.Red) : string.Empty,
                        FrequencyInConstr = string.Format("{0:N2}", (m.TotalValue / m.Plan.EstimationValue) * 100),
                        FrequencyInService = string.Format("{0:N2}", (m.TotalValue / Db.Set<SubServicePlan>().Where(b => b.Plan.Code == m.Plan.Code && m.SubService.Service.Code == b.SubService.Service.Code).Select(b => b.TotalValue).Sum()) * 100)
                    }).ToList();
        }

        public IEnumerable<object> DependencyList()
        {
            return Db.Set<SubServicePlan>().ToList().Where(x => x.IdcActive.Equals(RegisterSituation.Active)).OrderBy(x => x.Order)
                    .Select(x => new { Code = x.Code, Dependency = x.Order + " | " + x.SubService.Name + " | " + x.Plan.Name }).ToList();
        }

        public IEnumerable<object> CheckDependency(SubServicePlan entity)
        {
            return Db.Set<SubServicePlan>().ToList()
                    .Where(x => x.Code == entity.CodeSubServicePlanDepen && x.CodeStatus == (int) FlagStatus.Done);
        }

    }
}
