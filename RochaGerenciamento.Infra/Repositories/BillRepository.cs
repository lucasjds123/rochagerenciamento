﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace RochaGerenciamento.Infra.Repositories
{
    public class BillRepository : RepositoryBase<Bill>, IBillRepository
    {
        public BillRepository(Context context)
            : base(context)
        {
        }

        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            return Db.Set<Bill>().ToList()
                    .Where(x => (ArrangeString.RemoveDiacritics(x.TotalValue.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Barcode.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Bank.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.EmissionDate.ToString().ToLower()).Contains(searchPhrase) 
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) 
                        && code.ToString().Equals(x.Checked) 
                        && x.Orders.Count() > 0
                        && (Session.CodeConstruction != 0 ? x.Orders.Where(r => r.Construction.Code == Session.CodeConstruction).ToList().Count() > 0 : true))
                    .Select(m => new
                    {
                        Construction = m.Orders.FirstOrDefault().Construction.Name,
                        TotalValue = m.TotalValue,
                        Barcode = m.Barcode,
                        Bank = m.Bank,
                        EmissionDate = m.EmissionDate.GetDateTimeFormats('D')[0].ToString(),
                        Code = m.Code,
                        Checked = m.Checked.Equals(RegisterSituation.Active) ? RegisterSituation.Yes : RegisterSituation.No,
                        AmountInstallments = m.Installments.Where(b => b.IdcActive.Equals(RegisterSituation.Active)).Count(),
                        AmountInstallmentsPaid = m.Installments.Where(a => a.Checked.Equals(RegisterSituation.Active) && a.IdcActive.Equals(RegisterSituation.Active)).Count(),
                        NextInstallment = m.Installments.Where(a => a.Checked.Equals(RegisterSituation.NonActive) && a.IdcActive.Equals(RegisterSituation.Active)).FirstOrDefault() != null  ?  
                                m.Installments.OrderBy(a => a.Sequence).Where(a => a.Checked.Equals(RegisterSituation.NonActive) && a.IdcActive.Equals(RegisterSituation.Active)).FirstOrDefault().DueDate.GetDateTimeFormats('D')[0].ToString() : ""
                    }).ToList();
        }

        
    }
}
