﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace RochaGerenciamento.Infra.Repositories
{
    public class ManufacturerRepository: RepositoryBase<Manufacturer>, IManufacturerRepository
    {
        public ManufacturerRepository(Context context)
            : base(context)
        {
            //other stuff here
        }

        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            return Db.Set<Manufacturer>().ToList()
                    .Where(x => (ArrangeString.RemoveDiacritics(x.Name.ToString().ToLower()).Contains(searchPhrase)
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) )
                    .Select(m => new
                    {
                        Name = m.Name,
                        Code = m.Code
                    }).ToList();
        }
    }
}
