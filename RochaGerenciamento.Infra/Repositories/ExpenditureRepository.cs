﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace RochaGerenciamento.Infra.Repositories
{
    public class ExpenditureRepository: RepositoryBase<Expenditure>, IExpenditureRepository
    {
        public ExpenditureRepository(Context context)
            : base(context)
        {
            //other stuff here
        }
        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            return Db.Set<Expenditure>().ToList()
                .Where(x => ((x.Description != null ? ArrangeString.RemoveDiacritics(x.Description.ToString().ToLower()).Contains(searchPhrase) : false) ||
                                (x.Observation != null ? ArrangeString.RemoveDiacritics(x.Observation.ToString().ToLower()).Contains(searchPhrase) : false) ||
                                ArrangeString.RemoveDiacritics(x.Construction.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                x.Value.ToString().ToLower().Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.ExpenditureType.Name.ToString()).ToLower().Contains(searchPhrase) ||
                                x.PurchaseDate.GetDateTimeFormats('D')[0].ToLower().Contains(searchPhrase)
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) && (Session.CodeConstruction != 0 ? Session.CodeConstruction == x.Construction.Code : true)) 
                    .OrderBy(x => x.ExpenditureType.Name)
                    .Select(m => new
                    {
                        Description = m.Description,
                        Observation = m.Observation,
                        Construction = m.Construction.Name,
                        Value = m.Value,
                        ExpenditureType = m.ExpenditureType.Name,
                        PurchaseDate = m.PurchaseDate.GetDateTimeFormats('D')[0],
                        Code = m.Code

                    }).ToList();
        }
    }
}
