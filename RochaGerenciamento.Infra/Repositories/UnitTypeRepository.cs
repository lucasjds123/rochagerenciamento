﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Repositories
{
    public class UnitTypeRepository : RepositoryBase<UnitType> , IUnitTypeRepository
    {
        public UnitTypeRepository(Context context)
            : base(context)
        {
            //other stuff here
        }
        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            return Db.Set<UnitType>().ToList()
                    .Where(x => (
                                ArrangeString.RemoveDiacritics(x.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Description.ToString().ToLower()).Contains(searchPhrase)
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active))
                    .Select(m => new
                    {
                        Description = m.Description,
                        Name = m.Name,
                        Code = m.Code

                    }).ToList();
        }
    }
}
