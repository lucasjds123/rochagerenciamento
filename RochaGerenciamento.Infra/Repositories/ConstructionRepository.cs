﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Repositories
{
    public class ConstructionRepository : RepositoryBase<Construction>, IConstructionRepository
    {
        public ConstructionRepository(Context context)
            : base(context)
        {
            //other stuff here
        }
        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            return Db.Set<Construction>().ToList()
                .Where(x => (ArrangeString.RemoveDiacritics( (x.Address != null ? x.Address.Name.ToString().ToLower() : string.Empty) ).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Name.ToString()).ToLower().Contains(searchPhrase)  ||
                                ArrangeString.RemoveDiacritics((x.Address != null ? x.Address.Borough.ToString() : string.Empty)).ToLower().Contains(searchPhrase)
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) && (Session.CodeConstruction != 0 ? Session.CodeConstruction == x.Code : true)) 
                    .Select(m => new
                    {
                        Address = m.Address != null ? m.Address.Name : string.Empty,
                        City = m.Address != null ? m.Address.City : string.Empty,
                        Number =  m.Address != null ? m.Address.Number  : string.Empty,
                        Name = m.Name,
                        Borough = m.Address != null ? m.Address.Borough : string.Empty,
                        Code = m.Code

                    }).ToList();
        }

        public override void Update(Construction obj)
        {

            var attached = Db.Set<Construction>().Find(obj.GetType().GetProperty(GeneralField.Code).GetValue(obj, null));
            Db.Entry(obj.Address).State = EntityState.Added;
            if (attached != null) { Db.Entry(attached).State = EntityState.Detached; }
            Db.Entry(obj).State = EntityState.Modified;
            
            Db.SaveChanges();

        }
    }
}
