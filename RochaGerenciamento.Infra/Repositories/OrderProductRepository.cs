﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Repositories
{
    public class OrderProductRepository : RepositoryBase<OrderProduct>, IOrderProductRepository
    {
        public OrderProductRepository(Context context)
            : base(context)
        {
        }

        public IEnumerable<object> GetListByOrder(int codeOrder)
        {
            return Db.Set<OrderProduct>().ToList().Where(x => x.CodeOrder.Equals(codeOrder)).ToList();
        }

        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            int counter = 1;
            return Db.Set<OrderProduct>().ToList()
                    .Where(x => (
                                ArrangeString.RemoveDiacritics(x.Product.Name.ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Product.Provider.Name.ToLower()).Contains(searchPhrase) 
                                
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) && (code > 0 ? code == x.CodeOrder : code > 0) ) 
                    .Select(m => new
                    {
                        Order= counter++,
                        Product = m.Product.Name,
                        TotalValue = (m.ValueUnit * m.Amount).ToString("n2"),
                        Provider = m.Product.Provider.Name,
                        ValueUnit = m.ValueUnit,
                        Amount = m.Amount,
                        Code = m.Code,
                        Recurrent = m.Product.Recurrent.Equals(RegisterSituation.Active) ? RegisterSituation.Yes : RegisterSituation.No,
                        Limit = m.Product.Limit,
                        AmountVerified = m.AmountVerified,

                    }).ToList();
        }
    }
}
