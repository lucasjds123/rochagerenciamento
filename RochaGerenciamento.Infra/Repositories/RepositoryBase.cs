﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Utils;


namespace RochaGerenciamento.Infra.Repositories
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected Context Db = new Context();

        public RepositoryBase(Context context)
        {
            Db = context;
        }

        public void Add(TEntity obj)
        {
            Db.Set<TEntity>().Add(obj);
            Db.SaveChanges();
        }

        public TEntity GetById(int id)
        {
            return Db.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Db.Set<TEntity>().ToList().Where(x => x.GetType().GetProperty(GeneralField.Active).GetValue(x, null).ToString().Equals(RegisterSituation.Active)).ToList();
        }

        public virtual void Update(TEntity obj)
        {
            //if (Db.Entry(obj).State == EntityState.Detached)
            //{
            var attached = Db.Set<TEntity>().Find(obj.GetType().GetProperty(GeneralField.Code).GetValue(obj, null));
            if (attached != null) { Db.Entry(attached).State = EntityState.Detached; }
            Db.Entry(obj).State = EntityState.Modified;
            //}
            Db.SaveChanges();

        }

        public void Remove(TEntity obj)
        {
            Db.Set<TEntity>().Remove(obj);
            Db.SaveChanges();
        }


        public virtual IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            return Db.Set<TEntity>().ToList()
                    .Where(x => ArrangeString.RemoveDiacritics(x.GetType().GetProperty(GeneralField.Name).GetValue(x, null).ToString().ToLower()).Contains(searchPhrase) &&
                         x.GetType().GetProperty(GeneralField.Active).GetValue(x, null).ToString().Equals(RegisterSituation.Active))

                    .Select(m => new
                    {
                        Name = m.GetType().GetProperty(GeneralField.Name).GetValue(m, null).ToString(),
                        Code = m.GetType().GetProperty(GeneralField.Code).GetValue(m, null).ToString()
                    }).OrderBy(x => x.Name).ToList();
        }

        public virtual void DynamicListTotal(string searchPhrase, ref int total, int code = 0)
        {
            total = Db.Set<TEntity>().ToList().Where(x => x.GetType().GetProperty(GeneralField.Name).GetValue(x, null).ToString().ToLower().Contains(searchPhrase) &&
                         x.GetType().GetProperty(GeneralField.Active).GetValue(x, null).ToString().Equals(RegisterSituation.Active)).Count();
        }

        public virtual TEntity FindByFieldName(string fieldName, string valor = "", int code = 0)
        {
            return (Db.Set<TEntity>().ToList()
                    .Where(x => x.GetType().GetProperty(fieldName).GetValue(x, null).ToString().ToLower().Equals(valor.ToLower()) &&
                            (code > 0 ? x.GetType().GetProperty(GeneralField.Code).GetValue(x, null).ToString() != code.ToString() : code == 0) &&
                            x.GetType().GetProperty(GeneralField.Active).GetValue(x, null).ToString().Equals(RegisterSituation.Active))).FirstOrDefault();
        }

        
    }
}
