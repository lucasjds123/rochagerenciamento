﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace RochaGerenciamento.Infra.Repositories
{
    public class RequestRepository : RepositoryBase<Request>, IRequestRepository
    {
        public RequestRepository(Context context)
            : base(context)
        {
            //other stuff here
        }
        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            
            return Db.Set<Request>().ToList()
                    .Where(x => (
                                ArrangeString.RemoveDiacritics(x.Tittle.ToString().ToLower()).Contains(searchPhrase) ||
                                ArrangeString.RemoveDiacritics(x.Message.ToString().ToLower()).Contains(searchPhrase) 
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active))
                    .OrderByDescending(x => x.CreationDate)
                    .Select(m => new
                    {
                        Tittle = m.Tittle,
                        Message = m.Message,
                        NeedDate = m.NeedDate.GetDateTimeFormats('D')[0],
                        Code = m.Code,
                        Observation = m.Observation,

                    }).ToList();
        }
    }
}
