﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Domain.ValueObject;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Repositories
{
    public class ClientRepository : RepositoryBase<Client>, IClientRepository
    {
        public ClientRepository(Context context)
            : base(context)
        {
        }
        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            return Db.Set<Client>().ToList()
                    .Where(x => (
                                ArrangeString.RemoveDiacritics(x.Name.ToString().ToLower()).Contains(searchPhrase)  ||
                                (x.Phone != null ? ArrangeString.RemoveDiacritics(x.Phone.ToString().ToLower()).Contains(searchPhrase): false) ||
                                (x.Email != null ? ArrangeString.RemoveDiacritics(x.Email.ToString().ToLower()).Contains(searchPhrase): false)  ||
                                (x.CPF != null ? ArrangeString.RemoveDiacritics(x.CPF.ToString().ToLower()).Contains(searchPhrase) : false) 
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active) ) 
                    .Select(m => new
                    {
                        CPF = m.CPF,
                        Phone = m.Phone,
                        Name = m.Name,
                        Email = m.Email,
                        Code = m.Code

                    }).ToList();
        }

        public IEnumerable<object> DropDownList()
        {
            return Db.Set<Client>().ToList().Where(x => x.IdcActive.Equals(RegisterSituation.Active)  && !string.IsNullOrEmpty(x.AttendanceDate.ToString()) && !string.IsNullOrEmpty(x.CodeContactOrigin.ToString()) ).Select(x => new { Code = x.Code, Name = x.Name }).ToList();
        }

        public override void Update(Client obj)
        {

            var attached = Db.Set<Client>().Find(obj.GetType().GetProperty(GeneralField.Code).GetValue(obj, null));
            Db.Entry(obj.Address).State = EntityState.Added;
            if (attached != null) { Db.Entry(attached).State = EntityState.Detached; }
            Db.Entry(obj).State = EntityState.Modified;
            Db.Entry(obj.Address).State = EntityState.Modified;
            Db.SaveChanges();

        }
    }
}
