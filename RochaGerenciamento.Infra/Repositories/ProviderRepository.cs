﻿using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Enums;
using RochaGerenciamento.Domain.Interfaces.Repositories;
using RochaGerenciamento.Infra.Contexto;
using RochaGerenciamento.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Infra.Repositories
{
    public class ProviderRepository : RepositoryBase<Provider>, IProviderRepository
    {
        public ProviderRepository(Context context)
            : base(context)
        {
            //other stuff here
        }
        public override IEnumerable<object> GetDynamicList(string searchPhrase, int code = 0)
        {
            searchPhrase = ArrangeString.RemoveDiacritics(searchPhrase.ToLower());
            return Db.Set<Provider>().ToList()
                    .Where(x => (
                                ArrangeString.RemoveDiacritics(x.Name.ToString().ToLower()).Contains(searchPhrase) ||
                                (x.Contact != null ? ArrangeString.RemoveDiacritics(x.Contact.ToString().ToLower()).Contains(searchPhrase) : true) 
                        )
                        && x.IdcActive.ToString().Equals(RegisterSituation.Active))
                    .Select(m => new
                    {
                        Phone = m.Phone,
                        Name = m.Name,
                        Email = m.Email,
                        Code = m.Code,
                        MaterialType = m.MaterialType.Name ,
                        Contact = m.Contact,
                        PSQ = m.PSQ.Equals(RegisterSituation.Active) ? RegisterSituation.Yes : RegisterSituation.No,

                    }).ToList();
        }

        public override void Update(Provider obj)
        {
         
            var attached = Db.Set<Provider>().Find(obj.GetType().GetProperty(GeneralField.Code).GetValue(obj, null));
            Db.Entry(obj.Address).State = EntityState.Added;
            if (attached != null) { Db.Entry(attached).State = EntityState.Detached; }
            Db.Entry(obj).State = EntityState.Modified;
            Db.SaveChanges();

        }
    }
}
