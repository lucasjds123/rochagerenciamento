﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ProductAppService: AppServiceBase<Product>, IProductAppService
    {
        private readonly IProductService _productService;

        public ProductAppService(IProductService productService)
            : base(productService)
        {
            _productService = productService;
        }

        public bool FindBySpecs(string name, int code = 0)
        {
            return _productService.FindBySpecs(name, code);
        }

        public IEnumerable<object> ProductList()
        {
            return _productService.ProductList();
        }

        public IEnumerable<object> DropDownList()
        {
            return _productService.DropDownList();
        }
    }
}
