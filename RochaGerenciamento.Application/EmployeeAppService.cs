﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class EmployeeAppService: AppServiceBase<Employee>, IEmployeeAppService
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeAppService(IEmployeeService employeeService)
            : base(employeeService)
        {
            _employeeService = employeeService;
        }

        public bool CheckPassword(int codeEmployee, string password)
        {
            return _employeeService.CheckPassword(codeEmployee, password);
        }
    }
}
