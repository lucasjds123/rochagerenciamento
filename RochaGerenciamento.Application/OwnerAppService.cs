﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class OwnerAppService: AppServiceBase<Owner>, IOwnerAppService
    {
        private readonly IOwnerService _ownerService;

        public OwnerAppService(IOwnerService ownerService)
            : base(ownerService)
        {
            _ownerService = ownerService;
        }

        public IEnumerable<object> DropDownList()
        {
            return _ownerService.DropDownList();
        }
    }
}
