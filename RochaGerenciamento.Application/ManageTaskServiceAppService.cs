﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ManageTaskServiceAppService : IManageTaskServiceAppService
    {
        private IManageTaskServiceService _manageTaskService;

        public ManageTaskServiceAppService(IManageTaskServiceService manageTaskService)
        {
            _manageTaskService = manageTaskService;
        }

        public void AddTaskService(TaskService entity)
        {
            _manageTaskService.AddTaskService(entity);
        }
    }
}
