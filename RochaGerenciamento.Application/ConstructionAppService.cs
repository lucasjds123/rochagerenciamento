﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ConstructionAppService : AppServiceBase<Construction>, IConstructionAppService
    {
        private readonly IConstructionService _constructionService;

        public ConstructionAppService(IConstructionService constructionService)
            : base(constructionService)
        {
            _constructionService = constructionService;
        }
    }
}
