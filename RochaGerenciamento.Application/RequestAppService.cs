﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class RequestAppService: AppServiceBase<Request>, IRequestAppService
    {
        private readonly IRequestService _requestService;

        public RequestAppService(IRequestService requestService)
            : base(requestService)
        {
            _requestService = requestService;
        }
    }
}
