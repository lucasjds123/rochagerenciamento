﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ExpenditureAppService: AppServiceBase<Expenditure>, IExpenditureAppService
    {
        private readonly IExpenditureService _expenditureService;

        public ExpenditureAppService(IExpenditureService expenditureService)
            : base(expenditureService)
        {
            _expenditureService = expenditureService;
        }
    }
}
