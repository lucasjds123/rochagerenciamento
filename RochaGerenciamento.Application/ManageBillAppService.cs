﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ManageBillAppService:  IManageBillAppService
    {
        private readonly IManageBillService _manageBillService;

        public ManageBillAppService(IManageBillService manageBillService)
        {
            _manageBillService = manageBillService;
        }

        public ICollection<Installment> CreateInstallments(string paid, double totalValue, int amountInstallments, DateTime duedate)
        {
            return _manageBillService.CreateInstallments(paid, totalValue, amountInstallments, duedate);
        }
        
        public void AddBill(Bill bill, ICollection<Order> orders, ICollection<Installment> installments)
        {
            _manageBillService.AddBill(bill,orders,installments );
        }

        public void AddBill(Bill bill, ContractIncoming contractIncoming, ICollection<Installment> installments)
        {
            _manageBillService.AddBill(bill, contractIncoming, installments);
        }

        public ICollection<Order> CreateOrders(ICollection<Order> orders)
        {
            return _manageBillService.CreateOrders(orders);
        }

        public void UpdateOrders(Bill bill)
        {
            _manageBillService.UpdateOrders(bill);
        }

        public void ConfirmPayment(int id)
        {
            _manageBillService.ConfirmPayment(id);
        }

        public void ConfirmPaymentAll(int id)
        {
            _manageBillService.ConfirmPaymentAll(id);
        }

        public void AmountAboutToExpire()
        {
            _manageBillService.AmountAboutToExpire();
        }
    }
}
