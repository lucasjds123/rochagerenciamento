﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class TaskServiceAppService: AppServiceBase<TaskService>, ITaskServiceAppService
    {
        private readonly ITaskServiceService _taskService;

        public TaskServiceAppService(ITaskServiceService taskService)
            : base(taskService)
        {
            _taskService = taskService;
        }

        public void GetNextNumberTask(int codTaskService, string dateTimeStart)
        {
            _taskService.GetNextNumberTask(codTaskService, dateTimeStart);
        }
    }
}
