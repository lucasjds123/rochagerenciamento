﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class PlanAppService : AppServiceBase<Plan>, IPlanAppService
    {
        private readonly IPlanService _planService;

        public PlanAppService(IPlanService planService)
            : base(planService)
        {
            _planService = planService;
        }
    }
}
