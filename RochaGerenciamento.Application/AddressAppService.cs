﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class AddressAppService : AppServiceBase<Address>, IAddressAppService
    {
        private readonly IAddressService _addressService;

        public AddressAppService(IAddressService addressService)
            : base(addressService)
        {
            _addressService = addressService;
        }

        public IEnumerable<object> AddressList()
        {
            return _addressService.AddressList();
        }
    }
}
