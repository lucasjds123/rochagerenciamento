﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ProviderAppService : AppServiceBase<Provider>, IProviderAppService
    {
        private readonly IProviderService _providerService;

        public ProviderAppService(IProviderService providerService)
            : base(providerService)
        {
            _providerService = providerService;
        }
    }
}
