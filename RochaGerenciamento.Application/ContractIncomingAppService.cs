﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ContractIncomingAppService: AppServiceBase<ContractIncoming>, IContractIncomingAppService
    {
        private readonly IContractIncomingService _contractIncomingService;

        public ContractIncomingAppService(IContractIncomingService contractIncomingService)
            : base(contractIncomingService)
        {
            _contractIncomingService = contractIncomingService;
        }
    }
}
