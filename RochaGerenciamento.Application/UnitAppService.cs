﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class UnitAppService: AppServiceBase<Unit> , IUnitAppService
    {
        private readonly IUnitService _unitService;

        public UnitAppService(IUnitService unitService)
            : base(unitService)
        {
            _unitService = unitService;
        }

        public IEnumerable<object> DropDownList()
        {
            return _unitService.DropDownList();
        }
    }
}
