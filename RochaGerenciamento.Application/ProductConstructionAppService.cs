﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ProductConstructionAppService: AppServiceBase<ProductConstruction>, IProductConstructionAppService
    {
        private readonly IProductConstructionService _productConstructionService;

        public ProductConstructionAppService(IProductConstructionService productConstructionService)
            : base(productConstructionService)
        {
            _productConstructionService = productConstructionService;
        }

        public void UpdateSituation(double[] amount, int codSituation, int[] codProduct, int codConstruction, int codEmployee = 0,  DateTime? date = null ,  int[] codeProductConstructions = null)
        {
            _productConstructionService.UpdateSituation(amount, codSituation, codProduct, codConstruction, codEmployee, date, codeProductConstructions);
        }
        public string ProductsAboutToExpire()
        {
            return _productConstructionService.ProductsAboutToExpire();
        }

        public bool AmountInStock(int[] codeProductConstructions, int[] codProduct, double[] amount, int codSituation)
        {
            return _productConstructionService.AmountInStock(codeProductConstructions, codProduct, amount, codSituation);
        }

        public double AmountInStock( int codSituation)
        {
            return _productConstructionService.AmountInStock(codSituation);
        }

        public bool CheckVerified(IEnumerable<object> entity)
        {
            return _productConstructionService.CheckVerified(entity);
        }
        public bool CheckSituation(IEnumerable<object> entity)
        {
            return _productConstructionService.CheckSituation(entity);
        }

        public IEnumerable<object> GetListByIds(string ids)
        {
            return _productConstructionService.GetListByIds(ids);
        }
    }
}
