﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class SubServicePlanAppService : AppServiceBase<SubServicePlan>, ISubServicePlanAppService
    {
        private readonly ISubServicePlanService _subServicePlanService;

        public SubServicePlanAppService(ISubServicePlanService subServicePlanService)
            : base(subServicePlanService)
        {
            _subServicePlanService = subServicePlanService;
        }

        public IEnumerable<object> GetDynamicList(string searchPhrase, int rowCount, int current, int status, ref int total, string nameConstruction, string namePlan, string nameService, string nameSubService,
                                        string measureUnit, string amountRepetition,  string percent, string estimation, string value)
        {
            return _subServicePlanService.GetDynamicList( searchPhrase, rowCount,current, status, ref total,  nameConstruction,  namePlan,  nameService,  nameSubService,
                                         measureUnit,  amountRepetition,  percent,  estimation, value);
        }

        public IEnumerable<object> DependencyList()
        {
            return _subServicePlanService.DependencyList();
        }

        public void Start(int id)
        {
            _subServicePlanService.Start(id); 
        }

        public void UpdatePercent(int cod, double amount)
        {
            _subServicePlanService.UpdatePercent(cod,amount); 
        }

        public void Stop(int id)
        {
            _subServicePlanService.Stop(id); 
        }
        public void Finish(int id)
        {
            _subServicePlanService.Finish(id);
        }
        public void Refresh(int id)
        {
            _subServicePlanService.Refresh(id);
        }

        public bool CheckDependency(int id)
        {
            return _subServicePlanService.CheckDependency(id);
        }
    }
}
