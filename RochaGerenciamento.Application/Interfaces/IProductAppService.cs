﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application.Interfaces
{
    public interface IProductAppService : IAppServiceBase<Product>
    {
        IEnumerable<object> ProductList();
        bool FindBySpecs(string name, int code = 0);
        IEnumerable<object> DropDownList();

    }
}
