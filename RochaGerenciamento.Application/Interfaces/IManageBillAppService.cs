﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application.Interfaces
{
    public interface IManageBillAppService 
    {
        ICollection<Installment> CreateInstallments(string paid, double totalValue, int amountInstallments, DateTime duedate);
        void AddBill(Bill bill, ICollection<Order> orders, ICollection<Installment> installments);
        void AddBill(Bill bill, ContractIncoming contractIncoming, ICollection<Installment> installments);
        ICollection<Order> CreateOrders(ICollection<Order> orders);
        void UpdateOrders(Bill bill);
        void ConfirmPayment(int id);
        void ConfirmPaymentAll(int id);
        void AmountAboutToExpire();
    }
}
