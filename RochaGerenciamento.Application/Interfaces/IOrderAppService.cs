﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application.Interfaces
{
    public interface IOrderAppService : IAppServiceBase<Order>
    {
        IEnumerable<object> GetDynamicList(string searchPhrase, int rowCount, int status, ref int total, string nameConstruction,
             string numberOrder, string nameProvider);
        IEnumerable<object> OrderList();
        string GetNextNumberOrder();
    }
}
