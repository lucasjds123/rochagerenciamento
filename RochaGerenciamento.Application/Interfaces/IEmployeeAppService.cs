﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application.Interfaces
{
    public interface IEmployeeAppService : IAppServiceBase<Employee>
    {
        bool CheckPassword(int codeEmployee, string password);
    }
}
