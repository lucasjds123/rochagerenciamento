﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application.Interfaces
{
    public interface IProductConstructionAppService : IAppServiceBase<ProductConstruction>
    {
        void UpdateSituation(double[] amount, int codSituation, int[] codProduct, int codConstruction, int codEmployee = 0, DateTime? dataBaixa = null, int[] codeProductConstructions = null);
        string ProductsAboutToExpire();
        bool AmountInStock(int[] codeProductConstructions , int[] codProduct, double[] amount, int codSituation);
        double AmountInStock(int codSituation);
        bool CheckVerified(IEnumerable<object> entity);
        bool CheckSituation(IEnumerable<object> entity);
        IEnumerable<object> GetListByIds(string ids);
    }
}
