﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application.Interfaces
{
    public interface IAppServiceBase<TEntity> where TEntity : class
    {
        void Add(TEntity obj);
        TEntity GetById(int id);
        IEnumerable<TEntity> GetAll();
        void Update(TEntity obj);
        void Remove(TEntity obj);

        IEnumerable<object> GetDynamicList(string searchPhrase, int rowCount, int current, ref int total, int codigo);
        

        bool FindByName(TEntity entity);

    }
}
