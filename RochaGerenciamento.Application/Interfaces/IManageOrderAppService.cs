﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application.Interfaces
{
    public interface IManageOrderAppService
    {
        void ApprovalOrder(Order order);
        void VerifyOrder(Order order);
        void AddOrder(Order order, bool[] permanentPrice);
    }
}
