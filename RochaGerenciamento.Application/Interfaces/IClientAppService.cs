﻿using RochaGerenciamento.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application.Interfaces
{
    public interface IClientAppService : IAppServiceBase<Client>
    {
        IEnumerable<object> DropDownList();
    }
}
