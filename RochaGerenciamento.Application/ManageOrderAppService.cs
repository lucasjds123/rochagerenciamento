﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ManageOrderAppService : IManageOrderAppService
    {
        private IManageOrderService _manageOrderService;

        public ManageOrderAppService(IManageOrderService manageOrderService)
        {
            _manageOrderService = manageOrderService;
        }

        public void ApprovalOrder(Order order)
        {
            _manageOrderService.ApprovalOrder(order);
        }

        public void VerifyOrder(Order order)
        {
            _manageOrderService.VerifyOrder(order);
        }

        public void AddOrder(Order order, bool[] permanentPrice)
        {
            _manageOrderService.AddOrder(order, permanentPrice);
        }
        
    }
}
