﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class OrderProductAppService: AppServiceBase<OrderProduct>, IOrderProductAppService
    {
        private readonly IOrderProductService _orderProductService;

        public OrderProductAppService(IOrderProductService orderProductService)
            : base(orderProductService)
        {
            _orderProductService = orderProductService;
        }
    }
}
