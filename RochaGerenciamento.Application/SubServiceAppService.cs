﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class SubServiceAppService: AppServiceBase<SubService>, ISubServiceAppService
    {
        private readonly ISubServiceService _subServiceService;

        public SubServiceAppService(ISubServiceService subServiceService)
            : base(subServiceService)
        {
            _subServiceService = subServiceService;
        }
    }
}
