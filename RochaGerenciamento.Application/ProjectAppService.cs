﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ProjectAppService: AppServiceBase<Project>, IProjectAppService
    {
        private readonly IProjectService _projectService;

        public ProjectAppService(IProjectService projectService)
            : base(projectService)
        {
            _projectService = projectService;
        }
    }
}
