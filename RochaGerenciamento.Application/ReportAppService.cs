﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Interfaces.Services;
using RochaGerenciamento.Domain.StoredProcedures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ReportAppService : IReportAppService
    {
        private readonly IReportService _reportService;

        public ReportAppService(IReportService reportService)
        {
            _reportService = reportService;
        }

        public IEnumerable<object> Order(int approval, int codeConstruction, string firstDate, string lastDate)
        {
            return _reportService.Order(approval, codeConstruction, firstDate, lastDate);
        }

        public IEnumerable<object> Stock(int codeSituation, int codeConstruction, int codeProduct, int codeProvider, int codeUnitType, string firstDate, string lastDate)
        {
            return _reportService.Stock(codeSituation, codeConstruction, codeProduct, codeProvider, codeUnitType, firstDate, lastDate);
        }

        public IEnumerable<object> ExpenditureVsIncoming(int codeConstruction, string firstDate, string lastDate)
        {
            return _reportService.ExpenditureVsIncoming(codeConstruction, firstDate, lastDate);
        }

        public IEnumerable<object> Incoming(int codeConstruction, string firstDate, string lastDate)
        {
            return _reportService.Incoming(codeConstruction, firstDate, lastDate);
        }
        public IEnumerable<object> FilterOrder(int codOrder)
        {
            return _reportService.FilterOrder(codOrder);
        }

        public IEnumerable<object> EmployeeVsProduct(int codEmployee, string firstDate, string lastDate)
        {
            return _reportService.EmployeeVsProduct(codEmployee, firstDate, lastDate);
        }

        public IEnumerable<object> AttendanceDate(string year)
        {
            return _reportService.AttendanceDate(year);
        }

        public IEnumerable<object> ContactOrigin(string year, string month)
        {
            return _reportService.ContactOrigin(year, month);
        }

        public IEnumerable<object> OwnerPurchases(string year, string month)
        {
            return _reportService.OwnerPurchases(year, month);
        }

        public IEnumerable<ListPurchasesContract> Clients()
        {
            return _reportService.Clients();
        }
    }
}
