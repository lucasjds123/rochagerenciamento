﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class BillAppService : AppServiceBase<Bill>, IBillAppService
    {
        private readonly IBillService _billService;

        public BillAppService(IBillService billService)
            : base(billService)
        {
            _billService = billService;
        }
    }
}
