﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class InstallmentAppService: AppServiceBase<Installment>, IInstallmentAppService
    {
        private readonly IInstallmentService _installmentService;

        public InstallmentAppService(IInstallmentService installmentService)
            : base(installmentService)
        {
            _installmentService = installmentService;
        }

        public string InstallmentsAboutToExpire()
        {
            return _installmentService.InstallmentsAboutToExpire();
        }

    }
}
