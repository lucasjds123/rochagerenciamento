﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ManufacturerAppService: AppServiceBase<Manufacturer>, IManufacturerAppService
    {
        private readonly IManufacturerService _manufacturerService;

        public ManufacturerAppService(IManufacturerService manufacturerService)
            : base(manufacturerService)
        {
            _manufacturerService = manufacturerService;
        }
    }
}
