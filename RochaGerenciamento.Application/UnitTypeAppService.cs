﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class UnitTypeAppService : AppServiceBase<UnitType> , IUnitTypeAppService
    {
        private readonly IUnitTypeService _unitTypeService;

        public UnitTypeAppService(IUnitTypeService unitTypeService)
            : base(unitTypeService)
        {
            _unitTypeService = unitTypeService;
        }
    }
}
