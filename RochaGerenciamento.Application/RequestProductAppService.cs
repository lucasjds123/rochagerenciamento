﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class RequestProductAppService: AppServiceBase<RequestProduct>, IRequestProductAppService
    {
        private readonly IRequestProductService _requestProductService;

        public RequestProductAppService(IRequestProductService requestProductService)
            : base(requestProductService)
        {
            _requestProductService = requestProductService;
        }
    }
}
