﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class AppServiceBase<TEntity> : IAppServiceBase<TEntity> where TEntity : class
    {
        private readonly IServiceBase<TEntity> _serviceBase;

        public AppServiceBase(IServiceBase<TEntity> serviceBase)
        {
            _serviceBase = serviceBase;
        }

        public void Add(TEntity obj)
        {
            _serviceBase.Add(obj);
        }

        public TEntity GetById(int id)
        {
            return _serviceBase.GetById(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _serviceBase.GetAll();
        }

        public void Update(TEntity obj)
        {
            _serviceBase.Update(obj);
        }

        public void Remove(TEntity obj)
        {
            _serviceBase.Remove(obj);
        }


        public IEnumerable<object> GetDynamicList(string searchPhrase, int rowCount, int current, ref int total, int code = 0)
        {
            return _serviceBase.GetDynamicList(searchPhrase, rowCount, current, ref total, code);
        }
        

        public bool FindByName(TEntity entity)
        {
            return _serviceBase.FindByName(entity);
        }


    }
}
