﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ContractAppService : AppServiceBase<Contract>, IContractAppService
    {
        private readonly IContractService _contractService;

        public ContractAppService(IContractService contractService)
            : base(contractService)
        {
            _contractService = contractService;
        }

        public bool VerifyOwners(Contract entity)
        {
            return _contractService.VerifyOwners(entity);

        }
    }
}
