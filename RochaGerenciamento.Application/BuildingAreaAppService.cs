﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class BuildingAreaAppService : AppServiceBase<BuildingArea>, IBuildingAreaAppService
    {
        private readonly IBuildingAreaService _buildingAreaService;

        public BuildingAreaAppService(IBuildingAreaService buildingAreaService)
            : base(buildingAreaService)
        {
            _buildingAreaService = buildingAreaService;
        }
    }
}
