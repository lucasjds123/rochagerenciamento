﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class UserAppService : AppServiceBase<User>, IUserAppService
    {
        private readonly IUserService _userService;

        public UserAppService(IUserService userService)
            : base(userService)
        {
            _userService = userService;
        }

        public bool CheckUserExistence(string login)
        {
            return _userService.CheckUserExistence(login);
        }

        public User LookForLoginPassword(string login, string password)
        {
            return _userService.LookForLoginPassword(login, password);
        }
        public string GetEmailUserByProfile(string profile)
        {
            return _userService.GetEmailUserByProfile(profile) ;
        }
    }
}
