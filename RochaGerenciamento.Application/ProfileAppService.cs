﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class ProfileAppService : AppServiceBase<Profile>, IProfileAppService
    {
        private readonly IProfileService _profileService;

        public ProfileAppService(IProfileService profileService)
            : base(profileService)
        {
            _profileService = profileService;
        }
    }
}
