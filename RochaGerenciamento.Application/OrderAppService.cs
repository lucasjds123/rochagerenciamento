﻿using RochaGerenciamento.Application.Interfaces;
using RochaGerenciamento.Domain.Entities;
using RochaGerenciamento.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RochaGerenciamento.Application
{
    public class OrderAppService: AppServiceBase<Order>, IOrderAppService
    {
        private readonly IOrderService _orderService;

        public OrderAppService(IOrderService orderService)
            : base(orderService)
        {
            _orderService = orderService;
        }

        public IEnumerable<object> GetDynamicList(string searchPhrase, int rowCount, int current, ref int total, string nameConstruction,
                                            string numberOrder, string nameProvider)
        {
            return _orderService.GetDynamicList(searchPhrase, rowCount, current, ref total, nameConstruction, numberOrder,  nameProvider);
        }

        public IEnumerable<object> OrderList()
        {
            return _orderService.OrderList();
        }

        public string GetNextNumberOrder()
        {
            return _orderService.GetNextNumberOrder();
        }

       
    }
}
