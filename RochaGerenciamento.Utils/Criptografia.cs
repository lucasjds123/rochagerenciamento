﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace RochaGerenciamento.Utils
{
    public class Criptografia
    {
        public static string Encrypt(string password)
        {
            byte[] encrypted = HashPassword(password);
            //return as base64 string
            return Convert.ToBase64String(encrypted);
        }

        private static byte[] HashPassword(string password)
        {
            var provider = new SHA1CryptoServiceProvider();
            var encoding = new UnicodeEncoding();
            return provider.ComputeHash(encoding.GetBytes(password));
        }

    }
}
