﻿using CsvHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace RochaGerenciamento.Utils
{
    public static class File
    {
        public static void CreateFolder(string diretorio)
        {
            if (!System.IO.Directory.Exists(GetPath(diretorio)))
                System.IO.Directory.CreateDirectory(GetPath(diretorio));
        }

        public static void Save(HttpPostedFileBase uploadFile)
        {
            if (uploadFile != null && uploadFile.ContentLength > 0)
                uploadFile.SaveAs(GetPath( Path.GetFileName(uploadFile.FileName)));
        }

        public static void Save(IEnumerable records)
        {
            using (var sw = new StreamWriter(Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/"), "output.csv")))
            {
                var writer = new CsvWriter(sw);



                //Write the entire contents of the CSV file into another
                writer.WriteRecords(records);

                //Now we will write the data into the same output file but will do it 
                //Using two methods.  The first is writing the entire record.  The second
                //method writes individual fields.  Note you must call NextRecord method after 
                //using Writefield to terminate the record.

                //Note that WriteRecords will write a header record for you automatically.  If you 
                //are not using the WriteRecords method and you want to a header, you must call the 
                //Writeheader method like the following:
                //
                //writer.WriteHeader<DataRecord>();
                //
                //Do not use WriteHeader as WriteRecords will have done that already.

                //foreach (ListProductConstruction record in list)
                //{
                //    //Write entire current record
                //    writer.WriteRecord(record);

                //    //write record field by field
                //    writer.WriteField(record.Construction);
                //    writer.WriteField(record.Product);
                //    writer.WriteField(record.Provider);
                //    writer.WriteField(record.AvailabilityDate);
                //    writer.WriteField(record.Situation);
                //    writer.WriteField(record.Amount);
                //    writer.WriteField(record.Recurrent);
                //    writer.WriteField(record.AmountPerUnit);
                //    writer.WriteField(record.UnitType);
                //    //ensure you write end of record when you are using WriteField method
                //    writer.NextRecord();
                //}

            }
        }

        public static string GetPath(string nome = "")
        {
            return Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Images/Specs/" ), nome);
        }

        public static void Delete( string nome)
        {
            var filePath = GetPath( nome);
            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);
        }

        public static void Open(string filePath, string nameFile)
        {
            FileInfo file = new FileInfo(filePath);
            System.Web.HttpContext.Current.Response.Clear();
            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + nameFile);
            System.Web.HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            System.Web.HttpContext.Current.Response.ContentType = "application/octet-stream";
            System.Web.HttpContext.Current.Response.Flush();
            System.Web.HttpContext.Current.Response.WriteFile(file.FullName);
            System.Web.HttpContext.Current.Response.End();
        }

        public static void Download()
        {
            // Create an instance of WebClient
            WebClient client = new WebClient();
            var local = @"c:\users\" + System.Environment.UserName.ToString() + @"\desktop\output.csv";
            client.DownloadFile(Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/"), "output.csv"), local);
        }
    }
}
