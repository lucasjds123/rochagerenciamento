﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;

namespace RochaGerenciamento.Utils
{
    public static class Email
    {
        public static string SubjectExpirationDate
        {
            get
            {
                return "ALERTA ! Há boletos vencendo AMANHÃ!!";
            }
        }
        public static string SubjectLimit
        {
            get
            {
                return "ALERTA ! Há produtos se esgotando no estoque!!";
            }
        }

        public static string SubjectAlmoxarifado
        {
            get
            {
                return "Solicitação de compra enviada pelo Almoxarifado - ";
            }
        }

        public static string SubjectCompras
        {
            get
            {
                return "Solicitação de compra cadastrada pelo Setor de Compras - Número Ordem: ";
            }
        }

        public static string SubjectFinanceiro
        {
            get
            {
                return "Solicitação de compra aprovada pelo Setor de Financeiro - Número Ordem: ";
            }
        }

        public static string SubjectVerified
        {
            get
            {
                return "Resultado Analise de Conformidade - Número Ordem: ";
            }
        }

        public static string SubjectOrder
        {
            get
            {
                return "Ordem de Compra ROCHA EMPREENDIMENTOS";
            }
        }

        public static string BodyOrder
        {
            get
            {
                return "Ordem de compra enviada automaticamente pelo sistema";
            }
        }

        public static void Send(String text, String subject, String from, String to, HttpPostedFileBase fileAttachment = null)
        {

            var inlineLogo = new Attachment(System.Web.Hosting.HostingEnvironment.MapPath("~/Images/marca.png"));
            inlineLogo.ContentId = "marca";
            inlineLogo.ContentDisposition.Inline = true;
            inlineLogo.ContentDisposition.DispositionType = DispositionTypeNames.Inline;

            to = to.Substring(0, to.Length-1);
            MailMessage mm = new MailMessage("sistema@rochaempreendimentos.net", to);

            mm.Subject = subject;
            mm.IsBodyHtml = true;
            mm.Attachments.Add(inlineLogo);
            if (fileAttachment != null)
            {
                var file = new Attachment(fileAttachment.InputStream, Path.GetFileName(fileAttachment.FileName));
                mm.Attachments.Add(file);
            }
                
            mm.Body = Body(text, inlineLogo.ContentId);
            mm.CC.Add(from.Substring(0, from.Length - 1));
            mm.CC.Add("josecarlos@rochaempreendimentos.net");

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.rochaempreendimentos.net";
            smtp.EnableSsl = false;
            NetworkCredential NetworkCred = new NetworkCredential("sistema@rochaempreendimentos.net", "i1r2o3n4");
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = NetworkCred;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Port = 587;
            smtp.Send(mm);

        }

        private static String Body(String texto, String contentID)
        {
            StringBuilder mailBody = new StringBuilder();
            mailBody.AppendFormat(texto.Replace("#BREAKLINE", "<br>").Replace("#NEGRITO", "<b>").Replace("#FNEGRITO", "</b>"));
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("<p>Atenciosamente;</p><br>");
            mailBody.AppendFormat("<h1>Não responda esta mensagem! Esta mensagem é enviada pelo sistema</h1><br><img src=\"cid:" + contentID + "\">");

            return mailBody.ToString();
        }
    }
}
