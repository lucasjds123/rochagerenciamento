﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace RochaEmpreendimento.Utils
{
    public static class DropDownList
    {
        public static SelectList ListYesNo(string valueEdit = null)
        {
            return new SelectList(new[] { new { Value = "0", Text = "Não" },
                                        new { Value = "1", Text = "Sim" },
                                        },
                                        "Value",
                                        "Text", valueEdit);
        }

        public static SelectList ListNo()
        {
            return new SelectList(new[] { new { Value = "0", Text = "Não" },
                                        },
                                        "Value",
                                        "Text");
        }

        public static SelectList ListTypeReport()
        {
            return new SelectList(new[] { new { Value = "quantity", Text = "Por Quantitade" },
                                        new { Value = "value", Text = "Por Valor" },
                                        }, 
                                        "Value",
                                        "Text","quantity");
        }

        public static SelectList Months()
        {
            return new SelectList(new[] { new { Value = "1", Text = "Janeiro" },
                                            new { Value = "2", Text = "Fevereiro" },
                                            new { Value = "3", Text = "Marco" },
                                            new { Value = "4", Text = "Abril" },
                                            new { Value = "5", Text = "Maio" },
                                            new { Value = "6", Text = "Junho" },
                                            new { Value = "7", Text = "Julho" },
                                            new { Value = "8", Text = "Agosto" },
                                            new { Value = "9", Text = "Setembro" },
                                            new { Value = "10", Text = "Outubro" },
                                            new { Value = "11", Text = "Novembro" },
                                            new { Value = "12", Text = "Dezembro" },
                                        },
                                        "Value",
                                        "Text");
        }

    }
}
